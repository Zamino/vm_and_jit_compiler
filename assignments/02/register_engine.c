#include <cstdlib>
#include <cstdint>
#include <cstdio>
#include <cassert>

int STACK_SIZE = 128;

typedef enum {
  //arithmetic <operation> <destination> <input register> <input register or constant>
  ADD_R,
  SUB_R,
  DIV_R,
  MUL_R,
  MOD_R,
  ADD_I,
  SUB_I,
  DIV_I,
  MUL_I,
  MOD_I,
  // data <destination> <input register or constant>
  MOV_R,
  MOV_I,
  // stack <operation> <register>
  PUSH,
  POP,
  // flow
  B, // <address>
  BR, // <register>
  BL, // <address>
  B_EQ, // <address>
  B_NE, // <address>
  // function
  RET,
  // print / debug
  PrintStack,
  PrintReg,
} instruction_t;

typedef enum {
  /*00*/ R0,
  /*01*/ R1,
  /*02*/ R2,
  /*03*/ R3,
  /*04*/ R4,
  /*05*/ R5,
  /*06*/ R6,
  /*07*/ R7,
  /*08*/ R8,
  /*09*/ R9,
  /*010*/ R10,
  /*011*/ R11,
  /*012*/ R12,
  /*013*/ R13,
  /*014*/ R14,
  /*015*/ R15,
  /*016*/ PC, // PROGRAM_COUNTER
  /*017*/ LR, // LINK_REGISTER
  /*018*/ SP, // STACK_PTR
  /*019*/ FP, // FRAME_PTR
  /*020*/ CMP, // COMPARE_REGISTER
} r_t;

inline uint64_t inc_pc(uint64_t &pc) {
  uint64_t ret = *(uint64_t*) pc;
  pc += 8;
  return ret;
}

uint64_t interpret(uint64_t* program) {
  uint64_t r[21]; // register
  r[PC] = (uint64_t) program;
  uint64_t stack[STACK_SIZE];
  r[SP] = (uint64_t) (stack + STACK_SIZE); // point after end
  r[LR] = 0;

  uint64_t address;
  uint64_t dest_reg, in_1_reg, in_2_reg;
  for (;;) {
    switch ((r_t) inc_pc(r[PC])) {
      case ADD_R:
        dest_reg = inc_pc(r[PC]); in_1_reg = inc_pc(r[PC]); in_2_reg = inc_pc(r[PC]);
        r[dest_reg] = (int64_t) r[in_1_reg] + (int64_t) r[in_2_reg];
        break;
      case SUB_R:
        dest_reg = inc_pc(r[PC]); in_1_reg = inc_pc(r[PC]); in_2_reg = inc_pc(r[PC]);
        r[dest_reg] = (int64_t) r[in_1_reg] - (int64_t) r[in_2_reg];
        break;
      case DIV_R:
        dest_reg = inc_pc(r[PC]); in_1_reg = inc_pc(r[PC]); in_2_reg = inc_pc(r[PC]);
        r[dest_reg] = (int64_t) r[in_1_reg] / (int64_t) r[in_2_reg];
        break;
      case MUL_R:
        dest_reg = inc_pc(r[PC]); in_1_reg = inc_pc(r[PC]); in_2_reg = inc_pc(r[PC]);
        r[dest_reg] = (int64_t) r[in_1_reg] * (int64_t) r[in_2_reg];
        break;
      case MOD_R:
        dest_reg = inc_pc(r[PC]); in_1_reg = inc_pc(r[PC]); in_2_reg = inc_pc(r[PC]);
        r[dest_reg] = (int64_t) r[in_1_reg] % (int64_t) r[in_2_reg];
        break;
      case ADD_I:
        dest_reg = inc_pc(r[PC]); in_1_reg = inc_pc(r[PC]);
        r[dest_reg] = (int64_t) r[in_1_reg] + (int64_t) inc_pc(r[PC]);
        break;
      case SUB_I:
        dest_reg = inc_pc(r[PC]); in_1_reg = inc_pc(r[PC]);
        r[dest_reg] = (int64_t) r[in_1_reg] - (int64_t) inc_pc(r[PC]);
        break;
      case DIV_I:
        dest_reg = inc_pc(r[PC]); in_1_reg = inc_pc(r[PC]);
        r[dest_reg] = (int64_t) r[in_1_reg] / (int64_t) inc_pc(r[PC]);
        break;
      case MUL_I:
        dest_reg = inc_pc(r[PC]); in_1_reg = inc_pc(r[PC]);
        r[dest_reg] = (int64_t) r[in_1_reg] * (int64_t) inc_pc(r[PC]);
        break;
      case MOD_I:
        dest_reg = inc_pc(r[PC]); in_1_reg = inc_pc(r[PC]);
        r[dest_reg] = (int64_t) r[in_1_reg] % (int64_t) inc_pc(r[PC]);
        break;
      case PUSH:
        r[SP]-=8;
        *(uint64_t*)r[SP] = r[inc_pc(r[PC])];
        break;
      case POP:
        r[inc_pc(r[PC])] = *(uint64_t*)r[SP];
        r[SP]+=8;
        break;
      case MOV_R:
        dest_reg = inc_pc(r[PC]); in_1_reg = inc_pc(r[PC]);
        r[dest_reg] = r[in_1_reg];
        break;
      case MOV_I:
        dest_reg = inc_pc(r[PC]);
        r[dest_reg] = inc_pc(r[PC]);
        break;
      // flow
      case B: // <adress>
        r[PC] = inc_pc(r[PC]);
        break;
      case BR: // <register>
        dest_reg = inc_pc(r[PC]);
        r[PC] = r[dest_reg];
        break;
      case BL: // <adress>
        address = inc_pc(r[PC]);
        r[LR] = r[PC];
        r[PC] = address;
        break;
      case B_EQ: // <adress>
        address = inc_pc(r[PC]);
        if (r[CMP]) {
          r[PC] = address;
        }
        break;
      case B_NE: // <adress>
        address = inc_pc(r[PC]);
        if (!r[CMP]) {
          r[PC] = address;
        }
        break;
      case RET:
        if (r[LR]) {
          r[PC] = r[LR];
          r[LR] = 0; // reset
        } else {
          return r[0];
        }
        break;
      // print / debug
      case PrintStack:
        printf("STACK TOP: %ld\n", *(uint64_t*)r[SP]);
        break;
      case PrintReg:
        printf("Register: %ld\n", r[inc_pc(r[PC])]);
        break;
    }
  }
}

void test_arithmetic_operations() {
  uint64_t program_5_plus_7[] = {
    MOV_I, R0, 5,
    MOV_I, R1, 7,
    ADD_R, R0, R0, R1,
    RET
  };
  assert(("5+7=12", 12 == interpret(program_5_plus_7)));

  uint64_t program_5_sub_7[] = {
    MOV_I, R0, 5,
    MOV_I, R1, 7,
    SUB_R, R0, R0, R1,
    RET
  };
  assert(("5-7=-2", -2 == interpret(program_5_sub_7)));

  uint64_t program_5_mul_7[] = {
    MOV_I, R0, 5,
    MOV_I, R1, 7,
    MUL_R, R0, R0, R1,
    RET
  };
  assert(("5*7=35", 35 == interpret(program_5_mul_7)));

  uint64_t program_15_div_5[] = {
    MOV_I, R0, 15,
    DIV_I, R0, R0, 5,
    RET
  };
  assert(("15/5=3", 3 == interpret(program_15_div_5)));

  uint64_t program_15_mod_4[] = {
    MOV_I, R0, 15,
    MOD_I, R0, R0, 4,
    RET
  };
  assert(("15%4=3", 3 == interpret(program_15_mod_4)));
}

void test_branch() {
  uint64_t program_inc_func[] = {
    MOV_I, R0, 3,
    BL, 0, //space for address
    RET,
    0, 0, 0, // skip this
    // inc function
    ADD_I, R0, R0, 1,
    RET
  };
  program_inc_func[4] = (uint64_t) &program_inc_func[9]; // set address for branch

  assert(("3++==4", 4 == interpret(program_inc_func)));

  uint64_t program_fib[] = {
    MOV_I, R0, 17,
    BL, 0, // call fib
    RET,
    // // fib
    MOV_R, R2, R0, // n-th fib number
    MOV_I, R0, 0,
    MOV_I, R1, 1,
    PUSH, LR,
    BL, 0, // call fib_2
    POP, LR,
    RET,
    // // fib_2
    MOV_R, CMP, R2, // test if r2 is 0
    B_NE, 0, // branch to RET
    ADD_R, R3, R0, R1, // f_n+2 = f_n + f_n+1
    MOV_R, R0, R1,     // f_n   = f_n+1
    MOV_R, R1, R3,     // f_n+1 = f_n+2
    SUB_I, R2, R2, 1,  // n--
    B, 0, // call fib_2 in tail recursion
    RET,
  };
  program_fib[4]  = (uint64_t) &program_fib[6]; // call fib
  program_fib[18] = (uint64_t) &program_fib[22]; // call fib_2
  program_fib[26] = (uint64_t) &program_fib[43]; // branch to RET
  program_fib[42] = (uint64_t) &program_fib[22]; // call fib_2 recursion

  assert(("fib(17)==1597", 1597 == interpret(program_fib)));
}


int main () {

  test_arithmetic_operations();
  test_branch();
}

