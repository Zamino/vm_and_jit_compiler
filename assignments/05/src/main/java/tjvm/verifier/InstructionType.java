package tjvm.verifier;

import java.util.Arrays;
import java.util.EnumSet;

/**
 * Enum that represents the different instructions, that can be used in TinyJava. An instruction
 * is described in "The Java Virtual Machine Specification" as follows:
 * <blockquote>A Java Virtual Machine instruction consists of an opcode specifying the
 * operation to be performed, followed by zero or more operands embodying values to
 * be operated upon. (&sect;6)</blockquote>
 * This enum has basic information about what the opcode of that instruction is and
 * how many bytes are filled with operands after the instruction (without being modified by the
 * {@link #WIDE} instruction. <p>
 * There can be no two instructions with the same opcode.
 */
public enum InstructionType {
    /** No operation. */
    NOP(0,0),
    //	ACONST_NULL,	//no null reference in TinyJava

    /** Push constant -1 onto stack. */
    ICONST_M1(2,0),
    /** Push constant 0 onto stack. */
    ICONST_0(3, 0),
    /** Push constant 1 onto stack. */
    ICONST_1(4, 0),
    /** Push constant 2 onto stack. */
    ICONST_2(5, 0),
    /** Push constant 3 onto stack. */
    ICONST_3(6, 0),
    /** Push constant 4 onto stack. */
    ICONST_4(7, 0),
    /** Push constant 5 onto stack. */
    ICONST_5(8, 0),

    //	LCONST_i,	//i(0,1, no long type in TinyJava
    //	FCONST_i,	//i(0,1,2, no float type in TinyJava
    //	DCONST_i,	//i(0,1, no double type in TinyJava

    /** Push following byte onto stack, treated as integer. */
    BIPUSH(16, 1),
    /** Push following 2 bytes onto stack, treated as integer. */
    SIPUSH(17, 2),
    /** Push constant from constant pool onto stack, index specified by following byte. */
    LDC(18, 1),
    /** Push constant from constant pool onto stack, index specified by following two bytes (big endian). */
    LDC_W(19, 2),

    //	LDC2_W,		//only for long or double values

    /** Push integer from local variable table onto stack, index specified by following byte. */
    ILOAD(21, 1),
    //	LLOAD, FLOAD, DLOAD,	//not needed

    /** Push reference from loc var tab onto stack, index in following byte. */
    ALOAD(25, 1),
    /** Push integer from loc var tab at index 0 onto stack. */
    ILOAD_0(26, 0),
    /** Push integer from loc var tab at index 1 onto stack. */
    ILOAD_1(27, 0),
    /** Push integer from loc var tab at index 2 onto stack. */
    ILOAD_2(28, 0),
    /** Push integer from loc var tab at index 3 onto stack. */
    ILOAD_3(29, 0),

    //	LLOAD_0,...,3,	//not needed
    //	FLOAD_0,...,3,
    //	DLOAD_0,...,3,

    /** Push reference from loc var tab at index 0 onto stack. */
    ALOAD_0(42, 0),
    /** Push reference from loc var tab at index 1 onto stack. */
    ALOAD_1(43, 0),
    /** Push reference from loc var tab at index 2 onto stack. */
    ALOAD_2(44, 0),
    /** Push reference from loc var tab at index 3 onto stack. */
    ALOAD_3(45, 0),

    /** Push integer from array onto stack with arrayref and index on stack. */
    IALOAD(46, 0),
    //	LALOAD, FALOAD, DALOAD,
    //	AALOAD			//no object arrays in TinyJava
    //	BALOAD, CALOAD, SALOAD,	//not needed for TinyJava

    /** Store integer from stack into local variable at index specified by next byte. */
    ISTORE(54, 1),
    //	LSTORE, FSTORE, DSTORE,	//not needed

    /** Store reference from stack into loc var at index in next byte. */
    ASTORE(58, 1),
    /** Store integer from stack into loc var at index 0. */
    ISTORE_0(59, 0),
    /** Store integer from stack into loc var at index 1. */
    ISTORE_1(60, 0),
    /** Store integer from stack into loc var at index 2. */
    ISTORE_2(61, 0),
    /** Store integer from stack into loc var at index 3. */
    ISTORE_3(62, 0),

    //	LSTORE_0,...3, FSTORE_0,...3, DSTORE_0,...3,

    /** Store reference from stack into loc var at index 0. */
    ASTORE_0(75, 0),
    /** Store reference from stack into loc var at index 1. */
    ASTORE_1(76, 0),
    /** Store reference from stack into loc var at index 2. */
    ASTORE_2(77, 0),
    /** Store reference from stack into loc var at index 3. */
    ASTORE_3(78, 0),
    /** Store integer into array with arrayref, index and value on stack. */
    IASTORE(79, 0),

    //	LASTORE, FASTORE, DASTORE, AASTORE, BASTORE, CASTORE, SASTORE,

    /** Delete topmost stack value (e.g. if method returns void and stack is not empty yet). */
    POP(87, 0),
    /** Delete topmost two stack values. */
    POP2(88, 0),
    /** Duplicate topmost stack value. */
    DUP(89, 0),
    /** Copy topmost stack value two places underneath. */
    DUP_X1(90, 0),
    /** In TinyJava: copy topmost stack value three places underneath. */
    DUP_X2(91, 0),
    /** Duplicate the top two operand stack values.*/
    DUP2(92,0),
    /** Duplicate the top two operand stack values and insert three values down.*/
    DUP2_X1(93,0),
    /** Duplicate the top two operand stack values and insert four values down*/
    DUP2_X2(94,0),

    /** Swap two topmost stack values. */
    SWAP(95, 0),

    /** Add two integers. */
    IADD(96, 0),
    //	LADD, FADD, DADD,

    /** Subtract one integer from another. */
    ISUB(100, 0),		//
    //	LSUB, FSUB, DSUB,

    /** Multiply two integers. */
    IMUL(104, 0),
    //	LMUL, FMUL, DMUL,

    /** Divide one integer by another. */
    IDIV(108, 0),
    //	LDIV, FDIV, DDIV,

    /** Integer remainder. */
    IREM(112, 0),
    //	LREM, FREM, DREM,

    /** Negate integer. */
    INEG(116, 0),
    //	LNEG, FNEG, DNEG,

    /** Integer left shift of value1 by positions defined in low 5 bits of value2. */
    ISHL(120, 0),
    //	LSHL,

    /** Integer right shift of value1 by positions defined in low 5 bits of value2 (arithmetic, keeping the sign bit and distinguishing b/w mantissa and exponent). */
    ISHR(122, 0),
    //	LSHR,

    /** Integer right shift of value1 by positions defined in low 5 bits of value2 (logical, regardless of sign bit/mantissa/exponent). */
    IUSHR(124, 0),
    //	LUSHR,

    /** Bitwise AND on two integers. */
    IAND(126, 0),
    //	LAND,

    /** Bitwise OR on two integers. */
    IOR(128, 0),
    //	LOR,

    /** Bitwise XOR on two integers. */
    IXOR(130, 0),
    //	LXOR,

    /** Increment integer in loc var table at index in next byte by constant in byte after index byte. */
    IINC(132, 2),

    //	I2L, I2F, I2D,	//conversions of integers
    //	L2I, L2F, L2D,	//conversions of long values
    //	F2I, F2L, F2D,	//converisons of float values
    //	D2I, D2L, D2F,	//conversions of double values
    //	I2B, I2C, I2S,	//more conversions of integers
    //	LCMP,		//comparing of two long values
    //	FCMPL, FCMPG,	//comparing of two float values
    //	DCMPL, DCMPG,	//comparing of two double values

    /** If topmost stack entry is 0, jump to instruction indicated by next two bytes. */
    IFEQ(153, 2),
    /** If topmost stack entry is not 0, jump to instruction indicated by next two bytes. */
    IFNE(154, 2),
    /** If topmost stack entry is less than 0, jump to instruction indicated by next two bytes. */
    IFLT(155, 2),
    /** If topmost stack entry is greater or equal to 0, jump to instruction indicated by next two bytes. */
    IFGE(156, 2),
    /** If topmost stack entry is greater than 0, jump to instruction indicated by next two bytes. */
    IFGT(157, 2),
    /** If topmost stack entry is less or equal to 0, jump to instruction indicated by next two bytes. */
    IFLE(158, 2),

    /** If topmost two stack integer values are equal, jump to instruction indicated by next two bytes. */
    IF_ICMPEQ(159, 2),
    /** If topmost two stack integer values are not equal, jump to instruction indicated by next two bytes. */
    IF_ICMPNE(160, 2),
    /** If integer in second position from top on stack is less than integer in topmost stack position, jump to instruction indicated by next two bytes. */
    IF_ICMPLT(161, 2),
    /** If second integer is greater or equal to topmost integer, jump to instruction indicated by next two bytes. */
    IF_ICMPGE(162, 2),
    /** If second integer is greater than the topmost integer, jump to instruction indicated by next two bytes. */
    IF_ICMPGT(163, 2),
    /** If second integer less or equal to the topmost integer, jump to instruction indicated by next two bytes. */
    IF_ICMPLE(164, 2),

    /** If topmost references are equal, jump to instruction indicated by next two bytes. */
    IF_ACMPEQ(165, 2),
    /** If topmost references are not equal, jump to instruction indicated by next two bytes. */
    IF_ACMPNE(166, 2),

    /** Go to instruction indicated by next two bytes*/
    GOTO(167, 2),

    //	JSR, RET,			mainly used for finally clauses
    //	TABLESWITCH, LOOKUPSWITCH,	used for switch statement, not available in TinyJava

    /** Return top of stack, an {@code int} value from method to the invoker. */
    IRETURN(172, 0),
    //	LRETURN, FRETURN, DRETURN, ARETURN	//only integer or void allowed as return types
    /** Return {@code void} from method. */
    RETURN(177, 0),

    /** Push static field value onto stack, constant pool index specified by next two bytes. */
    GETSTATIC(178, 2),
    /** Store topmost stack value into static field at constant pool index specified by next two bytes. */
    PUTSTATIC(179, 2),
    /** Push field value in cp indicated by next two bytes onto stack. */
    GETFIELD(180, 2),
    /** Store topmost stack value into cp field indicated by next two bytes. */
    PUTFIELD(181, 2),

    /** Invoke virtual method at cp index specified by next two bytes. */
    INVOKEVIRTUAL(182, 2),
    /**
     * Invoke instance method at cp index specified by next two bytes.
     * There are only instance methods in Tiny Java, because no abstract methods can be declared.
     * But neither is there a possibility to declare final classes, so consequently the compiler
     * compiles all the methods of a TinyJava source code file into virtual methods, except for
     * constructors, which are always final methods and therefore compiled into INVOKESPECIAL.
     */
    INVOKESPECIAL(183, 2),
    /** Invoke static method at cp index specified by next two bytes. */
    INVOKESTATIC(184, 2),

    //	INVOKEINTERFACE,	//no interfaces
    //	INVOKEDYNAMIC,		//no dynamic methods

    /** Create new object of a class at constant pool index specified by next two bytes. */
    NEW(187, 2),
    /** Create new array of primitive type specified by next byte. */
    NEWARRAY(188, 1),

    //	ANEWARRAY,		//no arrays of objects in TinyJava

    /** Push array length onto stack. */
    ARRAYLENGTH(190, 0),

    //	ATHROW,			//no exceptions in TinyJava
    //	CHECKCAST,		//no casts in TinyJava

    //	INSTANCEOF,			//no class inheritance in TinyJava
    //	MONITORENTER, MONITOREXIT,	//no threads in TinyJava

    /** Double the operand length of the instruction right after this instruction. */
    WIDE(196, -1),

    //	MULTIANEWARRAY,		//only one-dimensional arrays in TinyJava

    /** If null reference on top of stack, jump to instruction indicated by next two bytes. */
    IFNULL(198, 2),
    /** If no null reference on top of stack, jump to instruction indicated by next two bytes. */
    IFNONNULL(199, 2),

    /** Go to instruction specified by next four bytes. */
    GOTO_W(200, 4);

    //	JSR_W,		//no subroutines (other than methods) in TinyJava

    //	instructions 202 to 255 are reserved for debuggers or future instructions

    /** Byte-value of the opcode (should be unsigned).*/
    public final byte opcode;
    /** Number of {@code bytes} after the instruction.*/
    public final int byteCount;
    /** Set with all {@link InstructionType}s that can be directly after the instruction {@link #WIDE}. */
    private static EnumSet<InstructionType> wideModifiable;

    static {
        wideModifiable = EnumSet.of(
                IFEQ, IFNE, IFGT, IFGE, IFLT, IFLE,
                IF_ICMPEQ, IF_ICMPNE, IF_ICMPGT, IF_ICMPGE, IF_ICMPGE, IF_ICMPLT, IF_ICMPLE,
                IF_ACMPEQ, IF_ACMPNE,
                IFNONNULL, IFNULL,
                ILOAD, ISTORE, ALOAD, ASTORE,
                IINC);
    }

    /**
     * Define a Type of VerifierInstruction by opcode and number of {@code bytes} after it.
     *
     * @param opcode the value of the unsigned byte representing the instruction
     * @param byteCount the number of {@code bytes} after the instruction
     */
    InstructionType(int opcode, int byteCount) {
        this.opcode = (byte) opcode;
        this.byteCount = byteCount;
    }

    /**
     * Check, if an instruction can be modified by the {@link #WIDE} instruction.
     *
     * @return {@code true}, if instruction can be modified by {@link #WIDE}
     */
    public boolean isWideModifiable() {
        return wideModifiable.contains(this);
    }

    /**
     * Get the {@link InstructionType} for a {@code byte} {@link #opcode}.
     *
     * @param opcode the {@link #opcode} to look for
     * @return the wrapped instruction
     * @throws InternalError if there is no corresponding opcode
     */
    public static InstructionType getType(byte opcode) {
        return Arrays.stream(values())
            .filter(inst -> inst.opcode == opcode)
            .findAny()
            .orElseThrow(() -> new InternalError("Byte opcode " + opcode + " has no Instruction type."));
    }

}
