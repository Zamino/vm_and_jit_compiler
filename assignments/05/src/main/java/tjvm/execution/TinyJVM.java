package tjvm.execution;

import tjvm.data.Heap;
import tjvm.data.MethodArea;
import tjvm.loader.ClassLoader;
import tjvm.loader.DerivedClass;
import tjvm.verifier.BytecodeVerifier;

/**
 * The TinyJava Virtual Machine is class with the functionality adapted from "The Java Virtual Machine
 * Specification (Java SE 10 Edition)" to the lightweight Java sublanguage "Tiny Java" that has a lot
 * less language elements than the original.<br>
 * Additionally this virtual machine implements some data structures according to of the lecture
 * "Mobiler Code" of Prof. Wolfram Amme at the FSU Jena to be possibly of help while teaching the
 * basics of compilers, virtual machines and interpreters. <br>
 * For the reason of education and the transparent implementation of the specification, some design
 * choices might look from a developer point of view a little clumsy, but were made in order to have
 * specific tasks done within a certain class, at a specific position or in a specific way that
 * is either from the specification or the lecture.
 */
public class TinyJVM {

    /**
     * The identifier of the first method to run, which is on purpose different to Java.
     */
    private static final String MAIN_METHOD = ".tinyMain:()V";
    /**
     * The identifier of the class initialization method.
     */
    private static final String CLASS_INIT_METHOD = ".<clinit>:()V";
    /**
     * The identifier of the object initialization method without arguments.
     */
    private static final String OBJECT_INIT_METHOD = ".<init>:()V";

    /**
     * The heap to store arrays and objects defined in the JVMS 10 &sect;2.5.3.
     */
    private Heap heap;

    /**
     * The method area to store per-class information defined in the JVMS 10 &sect;2.5.4.
     */
    private MethodArea methodArea;

    /**
     * The bootstrap class loader to load classes into the method area  defined in the JVMS 10 &sect;5.3.1.
     */
    private ClassLoader classLoader;

    /**
     * The bytecode verifier which is used while linking of a class defined in the JVMS 10 &sect;4.10.2.2.
     */
    private BytecodeVerifier verifier;

    /**
     * The interpreter which runs the java bytecode without any modifications to the code.
     */
    private Interpreter interpreter;

    /**
     * Thread to execute the main method from the loaded and linked main class in. It drives all further execution.
     */
    private Thread mainThread;


    /**
     * Method to start the Tiny Java Virtual Machine.
     * <blockquote>
     * "The Java Virtual Machine starts up by creating an initial class or interface using
     * the bootstrap class loader (&sect;5.3.1) [...]. The Java Virtual Machine then links the initial
     * class or interface, initializes it, and invokes the {@code public static} method {@code void main(String[])}.
     * The invocation of this method drives all further execution. Execution of the Java Virtual
     * Machine instructions constituting the {@code main} method may cause linking (and consequently creation) of
     * additional classes and interfaces, as well as invocation of additional methods." (&sect;5.2 JVMS)
     * </blockquote>
     * Compared to the "real" JVM, this virtual machine does loading and creation in two separate steps,
     * while the initialization is not necessary due to missing language parts of TinyJava compared to the JVM.
     * Furthermore the main method is called {@code static void tinyMain()} to be distinctive from
     * the "full" Java main method.
     *
     * @param pathName the path of the initial {@code class} file
     * @param fileName the name of the initial {@code class} file
     * @throws ClassNotFoundException if no purported representation for the className is found
     */
    public void start(String pathName, String fileName) throws ClassNotFoundException {
        //1. Load main class with class loader
        classLoader = new ClassLoader(pathName);
        DerivedClass mainClass = classLoader.load(fileName);

        //2. Create main class in method area
        methodArea = new MethodArea();
        methodArea.create(mainClass);
        System.out.println("Class " + fileName + " loaded.");


        //3. Link main class, which includes Verification, Preparation and Resolution (not in this case, because dynamic).
        verifier = new BytecodeVerifier();
        System.out.println("Kein Verifier");
	//verifier.verify(mainClass);
        methodArea.prepare(mainClass);
        System.out.println("Class " + fileName + " linked.");

        //4a. Initialize heap and interpreter
        heap = new Heap();
        interpreter = new Interpreter(heap, methodArea, this);

        //4b. Initialize main class
        initialize(fileName);

        //5a. Get offset for code and class descriptor offset for main method
        int methodOffset;
        try {
            methodOffset = methodArea.getMethodOffsetFor(fileName + MAIN_METHOD);
        } catch (NullPointerException ex) {
            throw new NoSuchMethodError("Method " + MAIN_METHOD + " not found during startup at location " + pathName + fileName);
        }
        int codeOffset = methodArea.getClassDescTableEntry(methodOffset);

        int cdOffset = methodArea.getClassOffsetFor(fileName);

        //5b. Invoke method tinyMain().
        mainThread = new Thread();
        invokeMethod(codeOffset, mainThread, new int[0], cdOffset, fileName + MAIN_METHOD);
    }

    /**
     * Method to run the Tiny JVM, this would basically be the main loop for execution, which
     * is only described for the interpreter in The Java Virtual Machine Specification, whereas
     * in future implementations there is going to be the {@link Interpreter} and a JIT-Compiler
     * that are both used to execute the programs.<br>
     * Before running the TinyJVM, it has to be started with the method {@link #start(String, String)}.
     * <blockquote>
     * "The Java Virtual Machine exits when some thread invokes the exit method of
     * class {@code Runtime} or class {@code System}, or the halt method of class {@code Runtime},
     * and the exit or halt operation is permitted by the security manager." (&sect;5.7)
     * </blockquote>
     * As there is only one thread and no JIT-Compiler just yet, this method can't do
     * any scheduling or switching between interpreter and engine for compiled code.
     */
    public void run() {
        while (mainThread.framesLeft()) {
            if (true) { // decide between interpreter and engine for compiled code here
                interpreter.executeStep(mainThread);
            } else {
                // execute compiled code
            }
        }
    }

    /**
     * Invoke a method. At the moment this just calls {@link Interpreter#prepareMethod(int, Thread, int[], int, String)}
     * but should in future builds be used, to compile code, if that hasn't happened or invoke methods in the
     * engine that runs the compiled code.
     *
     * @param codeOffset       the offset of the code in the {@link MethodArea}
     *                         - positive for Bytecode, negative for compiled code
     * @param thread           the thread, in which the method is invoked
     * @param args             the array of arguments for the method
     * @param cdOffset         the reference to the {@code Class Descriptor of the class}
     * @param methodIdentifier the identifier of the method for debugging
     */
    void invokeMethod(int codeOffset, Thread thread, int[] args, int cdOffset, String methodIdentifier) {
        if (codeOffset > 0) {
            interpreter.prepareMethod(codeOffset, thread, args, cdOffset, methodIdentifier);
            // compile code here, if necessary
        } else {
            // prepare method with compiled code
        }
    }

    /**
     * Load a class specified by its class name. This process is divided into two steps:
     * <ol>
     * <li>Load the class with the class loader with {@link ClassLoader#load(String)}</li>
     * <li>Create the class in the method area with {@link MethodArea#create(DerivedClass)}</li>
     * </ol>
     * <blockquote>
     * "Creation of a class or interface C denoted by the name N consists of the construction
     * in the method area of the Java Virtual Machine (§2.5.4) of an implementationspecific
     * internal representation of C. Class or interface creation is triggered by
     * another class or interface D, which references C through its run-time constant pool. [...]
     * If C is not an array class, it is created by loading a binary representation of C (§4 (The
     * class File Format)) using a class loader. [...]
     * If the Java Virtual Machine ever attempts to load a class C during verification
     * (§5.4.1) or resolution (§5.4.3) (but not initialization (§5.5)), and the class loader
     * that is used to initiate loading of C throws an instance of ClassNotFoundException,
     * then the Java Virtual Machine must throw an instance of NoClassDefFoundError
     * whose cause is the instance of ClassNotFoundException."
     * </blockquote>
     *
     * @param className the name of the class to load and create
     * @throws ClassNotFoundException if no purported representation for the className is found
     */
    void load(String className) throws ClassNotFoundException {
        DerivedClass dc = classLoader.load(className);
        methodArea.create(dc);
        System.out.println("Class " + className + " loaded.");
    }

    /**
     * Link a class specified by its class name. This process is divided into two steps:
     * <ol>
     * <li>Verify the class with the verifier with {@link BytecodeVerifier#verify(DerivedClass)}</li>
     * <li>Prepare the class in the method area with {@link MethodArea#prepare(DerivedClass)}</li>
     * </ol>
     * <blockquote>
     * "Linking a class […] involves verifying and preparing that class […]. Resolution of symbolic
     * references in the class or interface is an optional part of linking. <br>
     * Verification (&sect;4.10) ensures that the binary representation of a class […] is structurally
     * correct (&sect;4.9). Verification may cause additional classes […] to be loaded (&sect;5.3) but need not
     * cause them to be verified or prepared.
     * Preparation involves creating the static fields for a class […] and initializing such fields
     * to their default values (&sect;2.3, &sect;2.4). This does not require the execution of any
     * Java Virtual Machine code;explicit initializers for static fields are executed as part of
     * initialization (&sect;5.5), not preparation." (&sect;5.4 JVMS)
     * </blockquote>
     * If the class is not loaded yet, it is loaded prior to linking.
     *
     * @param className the name of the class to link
     * @throws ClassNotFoundException if no purported representation for the className is found
     */
    void link(String className) throws ClassNotFoundException {
        if (!methodArea.classLoaded(className))
            load(className);
        DerivedClass dc = classLoader.getClass(className);
        verifier.verify(dc);
        methodArea.prepare(dc);
        System.out.println("Class " + className + " linked.");
    }

    /**
     * Initialize a class specified by its class name.
     * <blockquote>
     * "Initialization of a class or interface consists of executing its class or interface
     * initialization method. <br>
     * A class or interface C may be initialized only as a result of: <br>
     * The execution of any one of the Java Virtual Machine instructions {@code new}, {@code getstatic},
     * {@code putstatic} or {@code invokestatic} that references C.<br>
     * <ul>
     * <li>Upon execution of a new instruction, the class to be initialized is the class
     * referenced by the instruction. </li>
     * <li>Upon execution of a {@code getstatic}, {@code putstatic}, or {@code invokestatic} instruction, the class or
     * interface to be initialized is the class or interface that declares the resolved field
     * or method. [...] </li>
     * <li>It's designation as the initial class or interface at Java Virtual Machine startup.</li>
     * </ul>
     * [...] There is also the possibility that initialization of a class or interface may be requested recursively as part
     * of the initialization of that class or interface. The implementation of the Java
     * Virtual Machine is responsible for taking care of synchronization and recursive
     * initialization by using the following procedure. It assumes that the Class object
     * has already been verified and prepared, and that the Class object contains state
     * that indicates one of [three] situations:
     * <ul>
     * <li>This Class object is verified and prepared but not initialized.</li>
     * <li>This Class object is being initialized by some particular thread.</li>
     * <li>This Class object is fully initialized and ready for use. [...]</li>
     * </ul>
     * The procedure for initializing C is then as follows:
     * <ol>
     * <li>Synchronize on the initialization lock, LC, for C. This involves waiting until the
     * current thread can acquire LC. [...]</li>
     * <li>If the Class object for C indicates that initialization is in progress for C by the
     * current thread, then this must be a recursive request for initialization. Release
     * LC and complete normally.</li>
     * <li>If the Class object for C indicates that C has already been initialized, then no
     * further action is required. Release LC and complete normally. [...]</li>
     * <li>Otherwise, record the fact that initialization of the Class object for C is in
     * progress by the current thread, and release LC.<br>
     * Then, initialize each final static field of C with the constant value in
     * its ConstantValue attribute (&sect;4.7.2), in the order the fields appear in the
     * ClassFile structure. [...]</li>
     * <li>Next, execute the class or interface initialization method of C.</li>
     * <li>If the execution of the class or interface initialization method completes
     * normally, then acquire LC, label the Class object for C as fully initialized, notify
     * all waiting threads, release LC, and complete this procedure normally." (&sect; 5.5 - shortened!!!)</li>
     * </ol>
     * </blockquote>
     * If the class is not linked yet, linking is done prior to initialization.
     *
     * @param className the name of the class to initialize
     * @throws ClassNotFoundException if no purported representation for the className is found
     */
    void initialize(String className) throws ClassNotFoundException {
        if (!methodArea.classLinked(className))
            link(className);
        if (methodArea.isInitializing(className))
            return;

        //Set initialization "lock"
        methodArea.setInitializing(className);

        //Initialize constant fields
        DerivedClass dc = classLoader.getClass(className);
        methodArea.initializeConstantFields(dc);

        //Check if <clinit>-method present for class, else finish
        String methodIdentifier = className + CLASS_INIT_METHOD;
        int codeOffset;
        try {
            int methodOffset = methodArea.getMethodOffsetFor(methodIdentifier);
            codeOffset = methodArea.getClassDescTableEntry(methodOffset);
        } catch (NullPointerException e) {
            methodArea.setInitialized(className);
            System.out.println("Class " + className + " initialized.");
            return;
        }

        //Initialize initialization-thread for <clinit> method
        Thread initThread = new Thread();
        int[] args = new int[0];
        int cdOffset = methodArea.getClassOffsetFor(className);
        interpreter.prepareMethod(codeOffset, initThread, args, cdOffset, methodIdentifier);

        //Execute <clinit> method
        while (initThread.framesLeft()) {
            interpreter.executeStep(initThread);
        }
        methodArea.setInitialized(className);
        System.out.println("Class " + className + " initialized (with <clinit> method).");
    }

    /**
     * Load the class of a {@code class} file and create the class in the method area. Afterwards
     * the class is verified and prepared in the method area. Then the class is initialized, which
     * consists of setting its {@code static final} fields to their values and running the
     * {@code <clinit>} method. The method area can be printed after initialization.
     *
     * @param pathName the name of the {@code class} file
     * @param fileName the name of the {@code class} file
     * @param print    {@code true} if file is to be printed
     */
    public void initClassAndPrint(String pathName, String fileName, boolean print) {
        try {
            //1. Load
            classLoader = new ClassLoader(pathName);
            methodArea = new MethodArea();
            load(fileName);

            //2. Link
            verifier = new BytecodeVerifier();
            link(fileName);

            //3. Initialize
            heap = new Heap();
            interpreter = new Interpreter(heap, methodArea, this);
            initialize(fileName);

            if (print)
                System.out.println(methodArea.toString());
        } catch (ClassNotFoundException e) {
            System.out.println("Class " + pathName + fileName + " not found in the file system.");
            System.exit(-1);
        }
    }

    /**
     * Initialize the class of a {@code class} file. Afterwards, the {@code <init>} method of the
     * class is run for the corresponding object, that is created in the heap. After running the method,
     * the. The method area is printed after initialization.
     *
     * @param pathName the name of the {@code class} file
     * @param fileName the name of the {@code class} file
     */
    public void initObjectAndPrint(String pathName, String fileName) {
        initClassAndPrint(pathName, fileName, false);

        //1. create object
        int cdOffset = methodArea.getClassOffsetFor(fileName);
        int fieldCount = methodArea.getFieldCount(cdOffset);
        int methTableOffset = methodArea.getMethodTableOffset(cdOffset);
        int objectOffset = heap.newObject(fieldCount, cdOffset, methTableOffset);

        //2. Get offset for code for <init> method
        int methodOffset;
        try {
            methodOffset = methodArea.getMethodOffsetFor(fileName + OBJECT_INIT_METHOD);
        } catch (NullPointerException ex) {
            throw new NoSuchMethodError("Method " + OBJECT_INIT_METHOD + " not found at location " + pathName + fileName);
        }
        int codeOffset = methodArea.getMethodTableEntry(methTableOffset + methodOffset);

        //3. Invoke method with object reference as target and run
        int[] args = new int[1];
        args[0] = objectOffset;
        mainThread = new Thread();
        invokeMethod(codeOffset, mainThread, args, cdOffset, fileName + OBJECT_INIT_METHOD);
        run();

        //4. Print
        System.out.println(methodArea.toString());
        System.out.println(heap.toString(methodArea));
    }
}
