package tjvm.data;

/**
 * Wrapper class for an {@code int[]} array that automatically increases in size,
 * if more elements are added, than the array can hold. There is a pointer, which
 * shows the actual position of the next entry. It can also be interpreted as size of
 * the array.<p>
 * Because there are no generic classes as of Java 10 for primitive types, {@link DynamicIntArray}
 * and {@link DynamicByteArray} are held in two seperate classes, having the exact same methods. <p>
 * The {@link DynamicIntArray} is  used by the {@link Heap} to store the objects and arrays,
 * as welll as by the {@link MethodArea} to store the static fields in the {@code Static Fields Table}.
 */
class DynamicIntArray {
    /** Initial size of the array. */
    private static final int INITIAL_SIZE = 1 << 8;

    /** Maximum increment iof the array. */
    private static final int MAX_INCREMENT = 1 << 18;

    /** Maximum size of the array. This is the biggest value of {@code int}. */
    private static final int MAX_SIZE = Integer.MAX_VALUE;

    /** The array. */
    private int[] array;

    /** The pointer to the next element that is inserted. Also the length of the array.*/
    private int pointer;

    /**
     * Create a new {@code byte[]} array with the the size of {@link DynamicIntArray#INITIAL_SIZE} that
     * is automatically increased in size, if necessary. The pointer to the first entry is at position {@code 0}.
     */
    DynamicIntArray() {
        array = new int[INITIAL_SIZE];
        pointer = 0;
    }

    /**
     * Add one {@code int} element at the end of the array.
     *
     * @param element the element to add
     * @return offset(/ position) of the element in the array
     */
    int addElement(int element) {
        int begin = pointer;
        if (pointer >= array.length)
            increaseSizeOfArray();
        array[pointer] = element;
        pointer++;
        return begin;
    }

    /**
     * Add multiple {@code int[]} elements at the end of the array.
     *
     * @param elements the elements to add
     * @return offset(/ position) of the beginning of the elements in the array
     */
    int addElements(int[] elements) {
        int begin = pointer;
        while (pointer + elements.length >= array.length)
            increaseSizeOfArray();
        for (int element : elements) {
            array[pointer] = element;
            pointer++;
        }
        return begin;
    }

    /**
     * Get the position of the next element (can be interpreted as size of the array).
     *
     * @return the position of the next element
     */
    int getPointerPosition() {
        return pointer;
    }

    /**
     * Get one {@code int} from the array.
     *
     * @param offset the offset(/position) of the element to get in the array
     * @return the element from specified offset
     * @throws ArrayIndexOutOfBoundsException if offset not in array
     */
    int getElement(int offset) {
        if (offset >= pointer)
            throw new ArrayIndexOutOfBoundsException();
        return array[offset];
    }

    /**
     * Set one {@code int} of the array.
     *
     * @param offset the offset(/position) of the element to set in the array
     * @param value  the new value of the element
     * @throws ArrayIndexOutOfBoundsException if offset not in array
     */
    void setElement(int offset, int value) {
        if (offset >= pointer)
            throw new ArrayIndexOutOfBoundsException();
        array[offset] = value;
    }

    /**
     * Increase the size of the array while keeping the whole content. The pointer stays the same, too.
     */
    private void increaseSizeOfArray() {
        int newSize;
        if (array.length == MAX_SIZE)
            throw new OutOfMemoryError();
        else if (array.length + MAX_INCREMENT >= MAX_SIZE)
            newSize = MAX_SIZE;
        else
            newSize = array.length >= MAX_INCREMENT ? array.length + MAX_INCREMENT : array.length * 2;

        int[] newArray = new int[newSize];
        System.arraycopy(array, 0, newArray, 0, array.length);
        array = newArray;
    }
}
