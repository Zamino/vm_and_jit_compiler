package tjvm.loader;

/**
 * Class that represents a {@code CONSTANT_NameAndType_info} structure in the constant pool. It has two indices
 * to {@code CONSTANT_Utf8_info} structures that hold name and descriptor of a field or method (&sect;4.4.6).
 */
class CPNameAndTypeEntry extends CPEntry {

  /** Index in constant pool to {@code CONSTANT_Utf8_info} structure with name. */
  int nameIndex;

  /** Index in constant pool to {@code CONSTANT_Utf8_info} structure with descriptor. */
  int descriptorIndex;

  /**
   * Create a new {@code CONSTANT_NameAndType_info} structure for the constant pool.
   *
   * @param nameIndex index in constant pool to {@code CONSTANT_Utf8_info} structure with name
   * @param descriptorIndex index in constant pool to {@code CONSTANT_Utf8_info} structure with descriptor
   */
  CPNameAndTypeEntry(int nameIndex, int descriptorIndex) {
    this.nameIndex = nameIndex;
    this.descriptorIndex = descriptorIndex;
  }

  @Override
  public String toString() {
    return "#" + cpIndex + " = NameAndType\t#" + nameIndex + ".#" + descriptorIndex + "\t\t // " + identifier;
  }

  @Override
  public void resolveIdentifier(ConstantPool constantPool) {
    String name = constantPool.getEntryAs(nameIndex, CPUTF8Entry.class).utf8String;
    String descriptor = constantPool.getEntryAs(descriptorIndex, CPUTF8Entry.class).utf8String;
    identifier = name + ":" + descriptor;
  }
}
