class Destination {

    final int home = 0;

    int target;

    Destination() {
        target = home;
    }

    void init (int i) {
        if (i == home) {
            target = 1;
        } else {
            target = i;
        }
    }
}