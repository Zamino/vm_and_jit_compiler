package tjvm.loader;

/**
 * Abstract class that represents an entry of the constant pool. It only holds the information about the tag and
 * the index of the entry in the constant pool (&sect;4.4).
 */
public abstract class CPEntry {

  /** Constant pool tag for a unicode string. */
  public static final byte CONSTANT_Utf8 = 1;
  /** Constant pool tag for an integer value. */
  public static final byte CONSTANT_Integer = 3;
  /** Constant pool tag for an index to class name string. */
  public static final byte CONSTANT_Class = 7;
  /** Constant pool tag for two indices: class ref + name and descriptor desc. */
  public static final byte CONSTANT_Fieldref = 9;
  /** Constant pool tag for two indices: class ref + name and descriptor desc. */
  public static final byte CONSTANT_Methodref = 10;
  /** Constant pool tag for two indices to utf-8 strings: name + encoded descriptor descriptor. */
  public static final byte CONSTANT_NameAndType = 12;

  /** Index of the entry the constant pool. */
  public int cpIndex;

  /** Tag of the constant pool entry. For example the number {@code 1} stands for a {@code CONSTANT_Utf8} tag. */
  public int tag;

  /** Resolved String value of the entry for easy identification. For a method e.g. {@code Class.method:descriptor}*/
  public String identifier;

  /** Value of a {@code CONSTANT_Integer} entry. */
  public int constValue;

  /**
   * Returns the string representation of the constant pool entry. The representation looks similar to
   * <blockquote>{@code javap} - The Java Class File Disassembler</blockquote>
   * using the {@code -verbose} option.
   *
   * @return The string representation of the constant pool entry
   */
  public String toString() {
    String type;
    switch (tag) {
      case CONSTANT_Utf8:
        type = "Utf8";
        break;
      case CONSTANT_Integer:
        type = "Integer";
        break;
      case CONSTANT_Class:
        type = "Class";
        break;
      case CONSTANT_Fieldref:
        type = "Fieldref";
        break;
      case CONSTANT_Methodref:
        type = "Methodref";
        break;
      case CONSTANT_NameAndType:
        type = "NameAndType";
        break;
      default:
        type = "Error";
    }
    return "\t#" + cpIndex + " = " + type + "\t\t" + identifier;
  }

  /**
   * Resolve the identifier for the constant pool entry. For a method this is after the pattern
   * {@code Class.method:descriptor}.
   *
   * @param constantPool the constant pool of the entry for finding the corresponding entries for resolution
   */
  public abstract void resolveIdentifier(ConstantPool constantPool);
}
