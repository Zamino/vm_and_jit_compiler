package tjvm.loader;

import java.util.regex.Pattern;

/**
 * The {@code TextChecker} is a class that can be used to check, if names (&sect;4.2) or descriptors (&sect;4.3) are
 * valid in TinyJava. Depending on the elements they're used for, eg. methods or fields, the checks are different.
 */
public class TextChecker {
    /**
     * Different regular expressions for checking the names and descriptors.
     */
    public static final String
            UNQUALIFIED_NAME = "[a-zA-Z]\\w*",
            BINARY_CLASS = "([a-zA-Z]\\w*/)*[a-zA-Z]\\w*", // Example: java/lang/Thread
            CLASS_DESC = "L[a-zA-Z]\\w*;",
            INT_DESC = "I",
            ARRAY_DESC = "\\[I";


    /**
     * Checks, if a given String contains a valid binary class name. (e.g. java/lang/Thread)
     *
     * @param className the name to check
     * @return true if name is valid
     */
    static boolean isValidBinaryClassName(String className) {
        return Pattern.matches(BINARY_CLASS, className);
    }

    /**
     * Checks, if a given String contains a valid unqualified name. (e.g. field123)
     *
     * @param unqualifiedName the name to check
     * @return true if name is valid
     */
    static boolean isValidUnqualifiedFieldName(String unqualifiedName) {
        return Pattern.matches(UNQUALIFIED_NAME, unqualifiedName);
    }

    /**
     * Checks, if a given String contains a valid unqualified method name. (e.g. method2sx. &lt;init&gt;)
     *
     * @param unqualifiedName the name to check
     * @return true if name is valid
     */
    static boolean isValidUnqualifiedMethodName(String unqualifiedName) {
        return Pattern.matches(UNQUALIFIED_NAME + "|<init>|<clinit>", unqualifiedName);
    }


    /**
     * Checks, if a given String contains a valid field descriptor for Tiny Java (e.g. [I or LThread;).
     *
     * @param fieldDescriptor the descriptor to check
     * @return true if field descriptor is valid in Tiny Java
     */
    static boolean isValidFieldDescriptor(String fieldDescriptor) {
        String fieldPattern = INT_DESC + "|" + ARRAY_DESC + "|" + CLASS_DESC;
        return Pattern.matches(fieldPattern, fieldDescriptor);
    }

    /**
     * Checks, if a given String contains a valid method descriptor for Tiny Java (e.g. (II[I[ILClass1;LClass2;)V).
     * As constructors are different to other methods, they must be checked separately.
     *
     * @param methDescriptor the descriptor to check
     * @param methName       the name of the method
     * @return true if method descriptor is valid in Tiny Java
     */
    static boolean isValidMethDescriptor(String methDescriptor, String methName) {
        String fieldPattern;
        String returnPattern;
        if (methName.startsWith("<")) {
            if (methName.equals("<init>")||methName.equals("<clinit>")) { //if method name starts with "<" ist must be named "<init>" or "<clinit>"
                fieldPattern = ""; //no parameters for class or object initialization in TinyJava
                returnPattern = "V";
            } else {
                return false;
            }
        } else {
            fieldPattern = INT_DESC + "|" + ARRAY_DESC + "|" + CLASS_DESC;
            returnPattern = "([IV])";
        }
        String methodPattern = "\\((" + fieldPattern + ")*\\)" + returnPattern;

        return Pattern.matches(methodPattern, methDescriptor);
    }

}
