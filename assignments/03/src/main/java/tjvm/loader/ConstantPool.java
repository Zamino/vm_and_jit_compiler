package tjvm.loader;

import java.util.Vector;

/**
 * This class represents all information about the constant pool (&sect;4.4) of the {@code class} file after loading.
 * The virtual machine instructions refer to symbolic references in the constant pool table.
 */
public class ConstantPool {

    /**
     * Vector of all entries of the constant pool.
     */
    private Vector<CPEntry> cpEntries;

    /**
     * Create an empty constant pool.
     */
    ConstantPool() {
        cpEntries = new Vector<>();
    }

    /**
     * Get the number of entries of the constant pool.
     *
     * @return the number of elements
     */
    public int size() {
        return cpEntries.size();
    }

    /**
     * Add an entry to the constant pool. Entries to the constant pool should be added in their specific order -
     * otherwise the indexing won't work properly.
     *
     * @param rcpe the entry to add
     * @throws InternalError if entry is {@code null}
     */
    void addEntry(CPEntry rcpe) {
        if (rcpe != null) {
            cpEntries.add(rcpe);
        } else {
            throw new InternalError("Constant pool entry while adding was null.");
        }
    }

    /**
     * Get the entry of a specific index. Note: The indexing of the constant pool is from {@code 1} to {@code size()}.
     * There is no element at index 0.
     *
     * @param index index of the entry to return
     * @return the entry at the specified index
     * @throws InternalError if there's no entry at that position
     */
    public CPEntry getEntry(int index) {
        if (index > 0 && index <= cpEntries.size()) {
            return cpEntries.get(index - 1);
        } else {
            throw new InternalError("Constant pool doesn't have an item at index " + index + ".");
        }
    }

    /**
     * Checks, if an entry of the constant pool is of an certain type.
     *
     * @param index index of the entry to be checked
     * @param tag   the type of the entry to be checked
     * @return {@code true}, if entry is of specified type
     */
    boolean entryIsType(int index, int tag) {
        return getEntry(index).tag == tag;
    }

    /**
     * Get the entry of a specific index, casted as the specified type.
     * Note: The indexing of the constant pool is from {@code 1} to {@code size()}. There is no element at index 0.
     *
     * @param index index of the entry to return
     * @param type  type of the entry to return
     * @param <T>   type of the entry to return
     * @return the entry at the specified index with specified type
     */
    @SuppressWarnings("unchecked")
    <T extends CPEntry> T getEntryAs(int index, Class<T> type) {
        CPEntry entry = getEntry(index);
        if (type.isInstance(entry)) {
            return (T) entry;
        } else {
            throw new ClassCastException();
        }
    }

    /**
     * The Format Checking of the constant pool ensures that the constraints given in the JVM Specification are satisfied
     * by the Constant Pool. These checks depend on the descriptor of structures that are saved in the constant pool.
     * The checks, if all items have the right size and if all tags are valid are already done while deriving the
     * constant pool from the class file. <p>
     * Furthermore, all field and method references in the constant pool are checked. They must have valid names,
     * valid classes and descriptors. <p>
     * Format checking could happen in the entries itself, but is wrapped in this method to make the process more transparent.
     * In addition, there are a few additional information written into the constant pool items, which happens in the
     * different entry-classes.
     *
     * @throws ClassFormatError if constant pool doesn't satisfy the constraints
     */
    void checkFormat() {

        for (int i = 1; i <= size(); i++) {

            CPEntry entry = getEntry(i);
            int tag = entry.tag;

            switch (tag) {
                case CPEntry.CONSTANT_Utf8: //character string - only constraints on variety of characters no byte = 0, no byte between 0xf0 to 0xff
                    CPUTF8Entry utf8entry = (CPUTF8Entry) entry;
                    if (utf8entry.utf8String.length() == 0)
                        throw new ClassFormatError("Constant pool UTF-8 entry at index " + entry.cpIndex + "is empty.");
                    if (utf8entry.utf8String.length() != utf8entry.length)
                        throw new InternalError("Constant pool UTF-8 entry at index " + entry.cpIndex + "has wrong size.");
                    break;

                case CPEntry.CONSTANT_Integer: //integer constant - no explicit constraints given in specification
                    break;

                case CPEntry.CONSTANT_Class: //class reference - must be valid index to utf8 info structure, representing valid binary class name
                    CPClassEntry classEntry = (CPClassEntry) entry;
                    if (entryIsType(classEntry.classNameIndex, CPEntry.CONSTANT_Utf8)) {
                        CPUTF8Entry classNameEntry = getEntryAs(classEntry.classNameIndex, CPUTF8Entry.class);
                        if (!TextChecker.isValidBinaryClassName(classNameEntry.utf8String))
                            throw new ClassFormatError("UTF8 info structure at index" + classNameEntry.cpIndex + "doesn't contain valid binary class name: "+
                                    classNameEntry.utf8String);
                        else
                            classEntry.identifier = classNameEntry.utf8String;
                    } else {
                        throw new ClassFormatError("Constant pool class entry at index " + entry.cpIndex + " doesn't have valid index to Utf8 info structure.");
                    }
                    break;

                case CPEntry.CONSTANT_Fieldref: //field reference - class index must be a class info structure, name and descriptor must be a valid name and descriptor info structure with field descriptor
                    CPFieldOrMethEntry fieldEntry = (CPFieldOrMethEntry) entry;

                    if (entryIsType(fieldEntry.classIndex, CPEntry.CONSTANT_Class) && entryIsType(fieldEntry.nameAndTypeIndex, CPEntry.CONSTANT_NameAndType)) {
                        CPNameAndTypeEntry nameAndTypeEntry = getEntryAs(fieldEntry.nameAndTypeIndex, CPNameAndTypeEntry.class);
                        CPUTF8Entry fieldDescriptorIndex = getEntryAs(nameAndTypeEntry.descriptorIndex, CPUTF8Entry.class);
                        if (!TextChecker.isValidFieldDescriptor(fieldDescriptorIndex.utf8String))
                            throw new ClassFormatError("ValueType descriptor at " + nameAndTypeEntry.cpIndex + "doesn't contain valid field descriptor: "+
                                    fieldDescriptorIndex.utf8String);

                    } else {
                        throw new ClassFormatError("Constant pool field entry at index " + entry.cpIndex + "doesn't have valid index to class or name and type info structure.");
                    }
                    break;


                case CPEntry.CONSTANT_Methodref: //method reference - class index must be a class info structure,
                    CPFieldOrMethEntry methEntry = (CPFieldOrMethEntry) entry;

                    if (entryIsType(methEntry.classIndex, CPEntry.CONSTANT_Class) && entryIsType(methEntry.nameAndTypeIndex, CPEntry.CONSTANT_NameAndType)) {
                        CPNameAndTypeEntry nameAndTypeEntry = getEntryAs(methEntry.nameAndTypeIndex, CPNameAndTypeEntry.class);
                        CPUTF8Entry methDescriptor = getEntryAs(nameAndTypeEntry.descriptorIndex, CPUTF8Entry.class);
                        CPUTF8Entry methName = getEntryAs(nameAndTypeEntry.nameIndex, CPUTF8Entry.class);
                        if (!TextChecker.isValidMethDescriptor(methDescriptor.utf8String, methName.utf8String))
                            throw new ClassFormatError("ValueType descriptor at index" + nameAndTypeEntry.cpIndex + " doesn't contain valid method descriptor: " +
                                    methName.utf8String+":"+methDescriptor.utf8String);
                    } else {
                        throw new ClassFormatError("Constant pool method entry at index " + entry.cpIndex + " doesn't have valid index to class or name and type info structure.");
                    }
                    break;

                case CPEntry.CONSTANT_NameAndType:
                    CPNameAndTypeEntry nameAndTypeEntry = (CPNameAndTypeEntry) entry;
                    if (!entryIsType(nameAndTypeEntry.nameIndex, CPEntry.CONSTANT_Utf8) || !entryIsType(nameAndTypeEntry.descriptorIndex, CPEntry.CONSTANT_Utf8))
                        throw new ClassFormatError("Constant pool name and descriptor entry at index " + entry.cpIndex + " doesn't have valid indices to UTF8 info structures.");
                    break;

                default:
                    throw new ClassFormatError("Error at deriving the constant pool - tag " + tag + " not included in TinyJava.");
            }
            entry.resolveIdentifier(this);
        }
    }

    /**
     * Returns the string representation of the constant pool where every entry is indented.
     * The representation looks similar to
     * <blockquote>{@code javap} - The Java Class File Disassembler</blockquote>
     * using the {@code -verbose} option.
     *
     * @return The string representation of the constant pool
     */
    @Override
    public String toString() {
        StringBuilder res = new StringBuilder();
        res.append("Constant Pool:");
        for (CPEntry entry : cpEntries)
            res.append("\n\t").append(entry.toString());
        return res.toString();
    }
}
