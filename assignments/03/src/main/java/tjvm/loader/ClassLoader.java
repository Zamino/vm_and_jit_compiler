package tjvm.loader;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * The class loader is able to transfer {@code class} files from the file system of the computer into a
 * {@link DerivedClass}, which is easier to handle than the byte-oriented representation. <p>
 * The way this class loader operates, is adapted from "The Java Virtual Machine Specification" (Java SE 10 Edition).
 * All references to the chapters are written with a section symbol e.g. &sect;2.5<p>
 * While the {@code class} file format is defined all through &sect;4.1 to &sect;4.8, the process of loading is described in &sect;5.3.
 * For TinyJava a lot of the mentioned specifications don't apply or apply a little differently. They are adapted for the
 * lesser language size without mentioning every single change. <p>
 * In order to differentiate more between LOADING and CREATION (both &sect;5.3) the creation is done by
 * {@link tjvm.data.MethodArea#create(DerivedClass)} while only the loading is done by the class loader.
 */
public class ClassLoader {

  /**
   * The path the class loader is looking for files to load.
   */
  private String pathName;
  /**
   * Collection to keep track of already loaded classes.
   */
  private HashMap<String, DerivedClass> loadedClasses;


  /**
   * Create a new <code>ClassLoader</code> that is able to find and load classes from a given
   * directory in the file system.
   *
   * @param pathName The path of the folder containing the classes for this <code>ClassLoader</code> instance
   */
  public ClassLoader(String pathName) {
    this.pathName = pathName;
    loadedClasses = new HashMap<>();
  }

  /**
   * Loading a {@code class} file from the file system, which consists of following steps from the JVM Specification
   * in &sect;5.3.1 (Loading Using the Bootstrap Class Loader) and &sect;5.3.5 (Deriving a Class from a {@code class} file).
   * <ol>
   * <li>Check, if the class has been loaded already. If so, no class loading is necessary. But this should have
   * been checked by the virtual machine already, so a {@code LinkageError is thrown}</li>
   * <li>Find the file in the file system. If If no purported representation of the is found,
   * loading throws an instance of {@code ClassNotFoundException}.</li>
   * <li>Derive the class from the file (attempting to parse the purported representation) while detecting
   * following errors:
   * <ul>
   * <li>The {@code class} file has a valid {@code ClassFile} structure (&sect;4.1, &sect;4.8), otherwise a
   * {@code ClassFormatError} is thrown.</li>
   * <li>The {@code class} file actually represents a class with that name, otherwise a
   * {@code NoClassDefFoundError} is thrown.</li>
   * </ul>
   * </li>
   * <li>The class loader records, that is the initiating loader of that class.</li>
   * </ol>
   *
   * @param className the name of the {@code class} file
   * @return the derived class
   * @throws LinkageError           if class has been already loaded
   * @throws ClassNotFoundException if no purported representation for the className is found
   * @throws InternalError          if the stream can't be initialized
   */
  public DerivedClass load(String className) throws ClassNotFoundException {
    String fileName = className + ".class";
    if (loadedClasses.containsKey(className))
      throw new LinkageError("Class with the name " + className + " already loaded.");
    try {
      DataInputStream inStream = new DataInputStream(new FileInputStream(Paths.get(pathName, fileName).toFile()));
      DerivedClass der = new DerivedClass(className);
      deriveClassFile(inStream, der);
      loadedClasses.put(className, der);
      return der;
    } catch (FileNotFoundException e) {
      throw new ClassNotFoundException("File \"" + fileName + "\" not found.");
    } catch (NullPointerException e) {
      throw new InternalError("No file stream has been initialized.");
    }
  }

  /**
   * Tries to derive a class from a {@code DataInputStream}. Deriving a class includes
   * Format Checking (&sect;4.8) of the {@code class} file and the parsing into an internal structure
   * of the virtual machine. The methods calls other methods according to the {@code ClassFile}
   * Structure (&sect;4.1) in order to make the derivation more transparent.
   *
   * @param in  the stream of the {@code class} file to derive
   * @param der the representation of the class to write in
   */
  private void deriveClassFile(DataInputStream in, DerivedClass der) {
    checkMagicNumber(in);
    deriveVersionNumber(in, der);
    deriveConstantPool(in, der);
    deriveAccessFlags(in, der);
    deriveThisSuper(in, der);
    deriveInterfaces(in, der);
    deriveFields(in, der);
    deriveMethods(in, der);
    deriveAttributes(in, der, null, null);
    checkEndOfFile(in);
  }

  /**
   * Each java class-file begins with 4-Bytes that have the value <code>0xCAFEBABE</code>. (&sect;4.1)
   * If they're different, the class is not valid.
   *
   * @param in the current input stream
   * @throws ClassFormatError When magic number is wrong or EOF reached (or other IO exception)
   */
  private void checkMagicNumber(DataInputStream in) {
    try {
      int in_magic_number = in.readInt();
      if (0xCAFEBABE != in_magic_number) {
        throw new ClassFormatError("File doesn't start wtih magic number.");
      }
    } catch (IOException e) {
      throw new ClassFormatError("EOF while reading magic number (or other IO exception).");
    }
  }

  /**
   * Derive the major and minor class file version from the {@code class} file.
   * As there is no different behaviour planned for different versions
   * of TinyJava, it is just read and written into the derived class. Under normal circumstances
   * the machine must check, if the version is supported. (&sect;4.1)
   *
   * @param in  the current input stream
   * @param der the class representation to write in
   * @throws ClassFormatError When EOF is reached (or other IO exception)
   */
  private void deriveVersionNumber(DataInputStream in, DerivedClass der) {
    try {
      der.minorVersion = in.readShort();
      der.majorVersion = in.readShort();
    } catch (IOException e) {
      throw new ClassFormatError("EOF while reading version number (or other IO exception).");
    }
  }

  /**
   * Derive the constant pool from the {@code class} file. There are various constraints
   * for the constant pool, which is a table of variable-sized structures (&sect;4.4). Right
   * before the content of the table is the the length written down within 2 bytes (&sect;4.1).
   * <p>
   * At the end of derivation, the Constant Pool undergoes further Format Checking.
   * This is necessary in order to ensure that the constraints in the JVM Specification are met.
   * (for example: Class entries always have to point to a valid UTF8 entry)
   *
   * @param in  the current input stream
   * @param der the class representation to write in
   * @throws ClassFormatError If constant pool tag not included in TinyJava or EOF is reached (or other IO exception)
   */
  private void deriveConstantPool(DataInputStream in, DerivedClass der) {
    try {
      int cpSize = Short.toUnsignedInt(in.readShort());
      for (int i = 1; i < cpSize; i++) {
        CPEntry cpEntry = null;
        byte tag = in.readByte(); //get tag of constant pool entry
        switch (tag) {
          case CPEntry.CONSTANT_Utf8: //character string
            short length = in.readShort();
            byte[] char_string = new byte[length];
            in.read(char_string, 0, length);
            cpEntry = new CPUTF8Entry(new String(char_string));
            break;
          case CPEntry.CONSTANT_Integer: //integer constant
            cpEntry = new CPIntEntry(in.readInt());
            break;
          case CPEntry.CONSTANT_Class: //class reference, class name: at index in following two bytes
            cpEntry = new CPClassEntry(in.readShort());
            break;
          case CPEntry.CONSTANT_Fieldref: //field reference, with class, name and descriptor (int, int[] or a reference to a TinyJava class)
            cpEntry = new CPFieldOrMethEntry(Short.toUnsignedInt(in.readShort()), Short.toUnsignedInt(in.readShort()));
            break;
          case CPEntry.CONSTANT_Methodref: //method reference, with class, name and descriptor (int, int[] or a reference to a TinyJava class)
            cpEntry = new CPFieldOrMethEntry(Short.toUnsignedInt(in.readShort()), Short.toUnsignedInt(in.readShort()));
            break;
          case CPEntry.CONSTANT_NameAndType:
            cpEntry = new CPNameAndTypeEntry(in.readShort(), in.readShort());
            break;

          default:
            throw new ClassFormatError("Error at deriving the constant pool - tag " + tag + " not included in TinyJava.");
        }

        cpEntry.cpIndex = i;
        cpEntry.tag = tag;
        der.constantPool.addEntry(cpEntry);
      }

    } catch (IOException e) {
      throw new ClassFormatError("EOF while reading constant pool (or other IO exception).");
    }
    der.constantPool.checkFormat();
  }

  /**
   * Derive the access flags from the class file (&sect;4.1). A TinyJava file should only have the <code>ACC_SUPER</code> flag (<code>0x0020</code>).
   * The other flags, for example <code>ACC_PUBLIC</code> or <code>ACC_ENUM</code> are not valid in TinyJava.
   *
   * @param in  the current input stream
   * @param der the class representation to write in
   * @throws ClassFormatError When wrong access flags are set or EOF is reached (or other IO exception)
   */
  private void deriveAccessFlags(DataInputStream in, DerivedClass der) {
    try {
      der.accessFlags = in.readShort();
    } catch (IOException e) {
      throw new ClassFormatError("EOF while reading access flags (or other IO exception).");
    }
  }

  /**
   * Derive the <code>this_class</code> and <code>super_class</code> items from the class file.
   * They must both have a valid index into the constant pool table to a class info structure (&sect;4.1).
   * The super class of every class in TinyJava can only be <code>java/lang/Object</code>.
   *
   * @param in  the current input stream
   * @param der the class representation to write in
   * @throws ClassFormatError     When wrong access flags are set or EOF is reached (or other IO exception)
   * @throws NoClassDefFoundError if the purported representation does not actually represent a class with that name
   */
  private void deriveThisSuper(DataInputStream in, DerivedClass der) {
    try {
      der.thisIndex = Short.toUnsignedInt(in.readShort());
      der.superIndex = Short.toUnsignedInt(in.readShort());
    } catch (IOException e) {
      throw new ClassFormatError("EOF while reading this_class and super_class indices (or other IO exception).");
    }
  }

  /**
   * Derives the <code>interface_count</code> fromm the class file (&sect;4.1). The <code>interface_count</code> should be 0 and
   * therefore the array <code>interfaces[]</code> is empty. This is, because there are no interfaces in TinyJava.
   *
   * @param in  the current input stream
   * @param der the class representation to write in
   * @throws ClassFormatError When wrong access flags are set or EOF is reached (or other IO exception)
   */
  private void deriveInterfaces(DataInputStream in, DerivedClass der) {
    try {
      der.interfaceCount = in.readShort();
      if (der.interfaceCount != 0) {
        throw new ClassFormatError("Interfaces are not implementet in TinyJVM");
      }
    } catch (IOException e) {
      throw new ClassFormatError("EOF while reading interface_count (or other IO exception).");
    }
  }

  /**
   * Derive the <code>field_count</code> and the table of <code>field_info</code> structures from the class file (&sect;4.1).
   * While deriving the fieldList, the following checks are made (&sect;4.5):
   * <ul>
   * <li>Are the fieldList final and/or static? Any other access flags are not valid.</li>
   * <li>Are the <code>name_index</code> and the <code>descriptor_index</code> pointing to a valid
   * entry in the constant pool table?</li>
   * <li>Are the UTF8 values <code>name_index</code> and the <code>descriptor_index</code> well-formed?</li>
   * <li>Each value of the {@code attributes} table must be a {@code attribute_info} structure (which is checked
   * in the method {@code deriveAttributes()}).</li>
   * <li>There can be no final field other than basic int types.</li>
   * <li>There can be no two field with the same name and descriptor.</li>
   * </ul>
   *
   * @param in  the current input stream
   * @param der the class representation to write in
   * @throws ClassFormatError if fieldList are malformed, not valid or EOF is reached (or other IO exception)
   */
  private void deriveFields(DataInputStream in, DerivedClass der) {
    try {
      int fieldsCount = Short.toUnsignedInt(in.readShort());
      for (int i = 0; i < fieldsCount; i++) {

        Field field = new Field();

        field.flags = in.readShort();
        switch (field.flags) {
          case 0x0018:
            field.isFinal = true;
            field.isStatic = true;
            break;
          case 0x0010:
            field.isFinal = true;
            field.isStatic = false;
            break;
          case 0x0008:
            field.isFinal = false;
            field.isStatic = true;
            break;
          case 0x0000:
            field.isFinal = false;
            field.isStatic = false;
            break;
          default:
            throw new ClassFormatError("Flags of fields can only contain ACC_STATIC and ACC_FINAL.");
        }

        CPUTF8Entry nameEntry = der.constantPool.getEntryAs(in.readShort(), CPUTF8Entry.class);
        field.name = nameEntry.utf8String;

        CPUTF8Entry descriptorEntry = der.constantPool.getEntryAs(in.readShort(), CPUTF8Entry.class);
        field.descriptor = descriptorEntry.utf8String;

        switch (field.descriptor) {
          case "I":
            field.valueType = ValueType.INT;
            break;
          case "[I":
            field.valueType = ValueType.INTARR;
            break;
          default:
            field.valueType = ValueType.REF;
            break;
        }

        if (field.isFinal && field.valueType != ValueType.INT)
          throw new ClassFormatError("Final field " + field.name + " is not of base valueType int.");
        deriveAttributes(in, der, null, field);
        field.identifier = der.className + "." + field.name + ":" + field.descriptor;
        if (der.fieldList.containsIdentifier(field.identifier))
          throw new ClassFormatError("There can be no two fields with the same name and descriptor: " + field.identifier);
        else
          der.fieldList.addField(field);
      }
    } catch (IOException e) {
      throw new ClassFormatError("EOF while reading fieldList (or other IO exception).");
    }
  }

  /**
   * Derive the <code>methods_count</code> and the table of <code>method_info</code> structures from the class file.
   * The return type and the type of the parameters are derived from the method descriptor.
   * While deriving the methods, the following checks are made ($4.6):
   * <ul>
   * <li>Is the method static? Any other access flags are not valid.</li>
   * <li>Are the <code>name_index</code> and the <code>descriptor_index</code> pointing to a valid
   * entry in the constant pool table?</li>
   * <li>Are the UTF8 values <code>name_index</code> and the <code>descriptor_index</code> well-formed?</li>
   * <li>Each value of the {@code attributes} table must be a {@code attribute_info} structure (which is checked
   * in the method {@code deriveAttributes()}).</li>
   * <li>There can be no <code>&lt;init&gt;</code> method with return type int or any parameters.</li>
   * <li>The method <code>&lt;clinit&gt;</code> has to have return type void
   * (and static flag set and no parameters if file is v. 51.0 or above - not checked).</li>
   * <li>There can only be 255 arguments (including {@code this}) for a method.</li>
   * </ul>
   *
   * @param in  the current input stream
   * @param der the class representation to write in
   * @throws ClassFormatError if methods are malformed, not valid or EOF is reached (or other IO exception)
   */
  private void deriveMethods(DataInputStream in, DerivedClass der) {
    try {
      int methodsCount = Short.toUnsignedInt(in.readShort());

      for (int i = 0; i < methodsCount; i++) {
        Method method = new Method();

        method.flags = in.readShort();
        switch (method.flags) {
          case 0x0008:
            method.isStatic = true;
            break;
          case 0x0000:
            method.isStatic = false;
            break;
          default:
            throw new ClassFormatError("Flags of methods can only contain ACC_STATIC.");
        }

        method.name = der.constantPool.getEntryAs(in.readShort(), CPUTF8Entry.class).utf8String;
        method.descriptor = der.constantPool.getEntryAs(in.readShort(), CPUTF8Entry.class).utf8String;

        if (!TextChecker.isValidMethDescriptor(method.descriptor, method.name))
          throw new ClassFormatError("Method descriptor " + method.descriptor + " not valid.");

        if (Pattern.matches("^(.*)V$", method.descriptor)) { // void
          method.returnsInt = false;
        } else if (Pattern.matches("^(.*)I$", method.descriptor)) { // int
          method.returnsInt = true;
        } else {
          throw new ClassFormatError("Method descriptor " + method.descriptor + " not valid.");
        }

        Pattern argumentPattern = Pattern.compile("\\((I|(?:\\[I)|(?:L\\w+;))");
        Matcher argumentMatcher = argumentPattern.matcher(method.descriptor);
        Pattern classNamePattern = Pattern.compile("L(\\w+);");
        while (argumentMatcher.find()) {
          switch (argumentMatcher.group(1)) {
            case "I":
              method.parameters.add(ValueType.INT);
              method.parameterNames.add("int");
              break;
            case "[I":
              method.parameters.add(ValueType.INTARR);
              method.parameterNames.add("int[]");
              break;
            default:
              method.parameters.add(ValueType.REF);
              // remove first and last char
              Matcher classNameMatcher = classNamePattern.matcher(argumentMatcher.group(1));
              classNameMatcher.find();
              String methodName = classNameMatcher.group(1);
              method.parameterNames.add(methodName);
              break;
          }
          String cut_first = argumentMatcher.replaceFirst("(");
          argumentMatcher = argumentPattern.matcher(cut_first);
        }

        method.argsSize = method.parameters.size() + (method.isStatic ? 0 : 1);
        if (method.argsSize > 255)
          throw new ClassFormatError("Method " + method.name + " " + method.descriptor + " has too many arguments: " + method.argsSize);

        if (method.name.equals("<init>") && (method.returnsInt || method.argsSize != 1))
          throw new ClassFormatError("Method with signature \"<init>\" not void or with parameters.");

        if (method.name.equals("<clinit>") && method.returnsInt)
          throw new ClassFormatError("Method with signature \"<clinit>\" not void.");
        //In a class file whose version number is 51.0 or above, the class initialization method has its ACC_STATIC flag set and takes no arguments (§4.6).

        //add method, if not already there
        method.identifier = der.className + "." + method.name + ":" + method.descriptor;
        if (der.methodList.containsIdentifier(method.identifier))
          throw new ClassFormatError("There can be no two methods with the same name and descriptor: " + method.identifier);
        else
          der.methodList.addEntry(method);

        //derive attributes for method
        deriveAttributes(in, der, method, null);
        if (!method.hasCodeAttribute)
          throw new ClassFormatError("Method " + method.name + " has not a Code attribute.");
      }

    } catch (IOException e) {
      throw new ClassFormatError("EOF while reading fieldList (or other IO exception).");
    }
  }

  /**
   * Derive the <code>attribute_info</code> structures from the class file. They're use in the {@code ClassFile},
   * {@code field_info}, {@code method_info} and {@code Code_attribute} structures. <p>
   * In this version of the virtual machine, only the following structures are used ($4.7):
   * <ul>
   * <li>{@code ConstantValue}: For {@code field_info} structures. It represents the value of a constant expression.
   * If the field is static, this value is assigned to the field as part of the initialization. The constant pool
   * entry must be a {@code CONSTANT_Integer} (&sect;4.7.2).</li>
   * <li>{@code Code}: For {@code method_info} structures. It contains the virtual machine instructions and
   * auxiliary information for a method. Every {@code method_info} structure must have exactly one {@code Code}
   * attribute (&sect;4.7.3).</li>
   * <li>{@code SourceFile}: The name of the source file the {@code class} file was compiled from (&sect;4.7.10).</li>
   * <li>{@code LineNumberTable}: For {@code Code} attributes. It may be used by debuggers to determine which
   * part of the {@code code} array corresponds to a given line number in the original source file (&sect;4.7.12). </li>
   * </ul>
   *
   * @param in     the current input stream
   * @param der    the class representation to write in
   * @param method the corresponding method table entry for {@code Code} or {@code LineNumberTable} attribute the write in
   *               - may be {@code null} otherwise
   * @param field  the corresponding field for {@code ConstantValue} attribute to write in - may be {@code null} otherwise
   * @throws ClassFormatError attributes are malformed, not valid or EOF is reached (or other IO exception)
   */
  private void deriveAttributes(DataInputStream in, DerivedClass der, Method method, Field field) {
    try {
      int attCount = Short.toUnsignedInt(in.readShort());

      for (int i = 0; i < attCount; i++) {
        //check type of attribute
        int attNameIndex = Short.toUnsignedInt(in.readShort());
        String attribute;
        if (der.constantPool.entryIsType(attNameIndex, CPEntry.CONSTANT_Utf8)) {
          attribute = der.constantPool.getEntryAs(attNameIndex, CPUTF8Entry.class).utf8String;
        } else {
          throw new ClassFormatError("Index to attribute type " + attNameIndex + " not valid.");
        }

        long attLength = Integer.toUnsignedLong(in.readInt());

        switch (attribute) {
          case "ConstantValue": //index to constant pool int entry with value of "final" field and set field.value
            CPIntEntry cpIntEntry = der.constantPool.getEntryAs(in.readShort(), CPIntEntry.class);
            if (field.isStatic) {
              field.value = cpIntEntry.constValue;
            }
            break;

          case "Code": //code attribute of a method, containing the bytecode instructions and additional information
                       // set method.maxStack method.maxLocals method.codeLength
                       // and method.code

            method.maxStack = Short.toUnsignedInt(in.readShort());
            method.maxLocals = Short.toUnsignedInt(in.readShort());
            method.codeLength = in.readInt();
            method.code = new byte[method.codeLength];
            in.read(method.code, 0, method.codeLength);
            method.hasCodeAttribute = true;

            //exception table, not used in TinyJava files
            int exceptTableLength = Short.toUnsignedInt(in.readShort());
            for (int j = 0; j < exceptTableLength; j++) {
              for (int k = 0; k < 4; k++) {
                in.readShort();
              }
            }
            deriveAttributes(in, der, method, null);
            break;

          case "SourceFile": //index to a constant pool utf-8 entry with the name of the source file and  set der.sourceFileName
            CPUTF8Entry sourceFileEntry = der.constantPool.getEntryAs(in.readShort(), CPUTF8Entry.class);
            der.sourceFileName = sourceFileEntry.utf8String;
            break;
          case "LineNumberTable": //corresponding line of code to program counter
            int tableLength = Short.toUnsignedInt(in.readShort());
            method.startPcs = new short[tableLength];
            method.lineNumbers = new short[tableLength];
            for (int j = 0; j < tableLength; j++) {
              //get bytecode programme counters and associated lines in the source file
              method.startPcs[j] = in.readShort();
              method.lineNumbers[j] = in.readShort();
            }
            break;

          default: //all other attributes get ignored
            for (int j = 0; j < attLength; j++) {
              in.readByte();
            }
        }
      }
    } catch (IOException e) {
      throw new ClassFormatError("EOF while reading attributes (or other IO exception).");
    }
  }

  /**
   * While Format Checking (&sect;4.8) the {@code class} file, it must be made sure that the {@code class} file
   * doesn't have any extra bytes at the end.
   *
   * @param in the current input stream
   */
  private void checkEndOfFile(DataInputStream in) {
    try {
      in.readByte();
      throw new ClassFormatError("EOF not reached.");
    } catch (EOFException e) {
      //nothing, because verified :)
    } catch (IOException e) {
      throw new ClassFormatError("Other IO Error while reading EOF.");
    }
  }

  /**
   * Get the derived class representation for a class identifier. This should only be done,
   * if the class is already loaded, which can be checked with {@link tjvm.data.MethodArea#classLoaded(String)}.
   * The class loader doesn't contain a isLoaded method, because it should only be used to load classes,
   * whereas all other operations should be within the defined data areas of the JVM.
   * This method should only be used to link already loaded classes.
   *
   * @param className the name of the class (identifier)
   * @return the representation of the class
   */
  public DerivedClass getClass(String className) {
    return loadedClasses.get(className);
  }
}
