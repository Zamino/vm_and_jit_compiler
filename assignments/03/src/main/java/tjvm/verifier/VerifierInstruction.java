package tjvm.verifier;

import tjvm.execution.Instruction;
import tjvm.loader.DerivedClass;
import tjvm.loader.Method;

import java.util.NavigableMap;
import java.util.Stack;

/**
 * Enum that represents the status of an {@link VerifierInstruction} during the verification process done
 * in the method {@link BytecodeVerifier#verify(DerivedClass)}
 */
enum VerifyStatus {
    /**
     * Status of the {@link VerifierInstruction} after execution of {@link BytecodeVerifier#breakUp(Method)}.
     */
    DIVIDED,
    /**
     * Status of the {@link VerifierInstruction} after execution of
     * {@link BytecodeVerifier#parseBuildAndCheck(NavigableMap, DerivedClass, Method)} .
     */
    PARSED,
    /**
     * Status of the {@link VerifierInstruction} after execution of
     * {@link DataFlowAnalyzer#DataFlowAnalyzer(NavigableMap, DerivedClass, Method)}
     */
    INITIALIZED,
    /**
     * Status of the {@link VerifierInstruction} after execution of {@link DataFlowAnalyzer#run()}.
     */
    VERIFIED
}

/**
 * Class that holds all information about a bytecode instruction for the {@link BytecodeVerifier}.
 * In order to come as close to the algorithm of the "The Bytecode Verifier" given in &sect;4.10.2.2
 * of the "The Java Virtual Machine Specification", some information are saved redundantly in different
 * ways. For example, the two fields {@link #op1} and {@link #op2} are the parsed information of
 * {@link #byteOperands}.
 */
public class VerifierInstruction extends Instruction {

    /**
     * Step of the verification in {@link BytecodeVerifier#verify(DerivedClass)}.
     */
    VerifyStatus verifyStatus;

    /**
     * The stack of values before execution of the instruction, used for verification.
     */
    Stack<VerifierValue> verifierStack;
    /**
     * The local variables of values before execution of the instruction, used for verification.
     */
    VerifierValue[] verifierLocals;
    /**
     * The stack of values after execution of the instruction, used for verification.
     */
    Stack<VerifierValue> mergeStack;
    /**
     * The local variables of values after execution of the instruction, used for verification.
     */
    VerifierValue[] mergeLocals;
    /**
     * Flag that shows, if the instruction must be processed by the {@link DataFlowAnalyzer}.
     */
    boolean changed;

    /**
     * Create a new instruction having the {@link #byteOperands} already parsed into {@link #op1}
     * or {@link #op2}.
     *
     * @param index        the index of the beginning of the instruction in the bytecode array
     * @param type         the type of the instruction
     * @param byteOperands the bytecode array of the operands
     * @param isWide       {@code true}, if the instruction was modified by a {@link InstructionType#WIDE}
     */
    VerifierInstruction(int index, InstructionType type, byte[] byteOperands, boolean isWide) {
        super(index, type, byteOperands, isWide);
        changed = false;
    }

    /**
     * Returns the string representation of the instruction. The representation looks similar to
     * <blockquote>{@code javap} - The Java Class File Disassembler</blockquote>
     * using the {@code -verbose} option.
     *
     * @return The string representation of the instruction
     */
    @Override
    public String toString() {
        StringBuilder res = new StringBuilder();
        String wide = isWide ? "_W" : "";
        res.append(index).append(" : ").append(type.name()).append(wide).append("\t");
        for (byte byteOperand : byteOperands) {
            res.append(Byte.toUnsignedInt(byteOperand)).append(" ");
        }
        return res.toString();
    }
}
