package tjvm.data;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * The {@code heap} is a Run-Time Data Area defined by "The Java Virtual Machine Specification (Java SE 10 Edition)"
 * in chapter &sect;2.5.3. It is shared among all Java Virtual Machine threads and is the data area from which
 * memory for all class instances and array is allocated. <br>
 * There are no specifications on how the heap is supposed to be implemented. The methods of this interface are
 * based on the data structures of the lecture "Mobiler Code" of Prof. Wolfram Amme at the FSU Jena. These data
 * structures follow the principle of addressing data with a base- and offset-address.<br>
 * Each object and array in the heap has a a certain amount of info fields, e.g. the length of the array or the
 * offset of the runtime constant pool.
 */
public class Heap {

    /**
     * Value of the field {@link #STATUS_OFFSET}, if object was initialized by {@link tjvm.verifier.InstructionType#NEW},
     * but not initialized yet.
     */
    public static final int STATUS_ALLOCATED = 1;
    /**
     * Value of the field {@link #STATUS_OFFSET}, if object has constructor already run with
     * {@link tjvm.verifier.InstructionType#INVOKESPECIAL}.
     */
    public static final int STATUS_INITIALIZED = 2;
    /**
     * Value of the field {@link #STATUS_OFFSET} for array that was created by
     * {@link tjvm.verifier.InstructionType#NEWARRAY}.
     */
    public static final int STATUS_ARRAY = 3;
    /**
     * The length of the info block of objects and arrays.
     */
    static final int INFO_LENGTH = 4;
    /**
     * Default value for field {@link #CD_OFFSET} of array.
     */
    private static final int DEFAULT_OFFSET = Integer.MIN_VALUE;
    /**
     * The offset of the entry, that holds the number of fields or length for objects and arrays.
     */
    private static final int LENGTH_OFFSET = -1;
    /**
     * The offset of the entry, that holds the status for objects and arrays.
     */
    private static final int STATUS_OFFSET = -2;
    /**
     * The offset of the entry, that holds the reference (offset) to the
     * {@code Class Descriptor} (CD) for objects (and arrays).
     */
    private static final int CD_OFFSET = -3;
    /**
     * The offset of the entry, that holds the reference (offset) to the
     * {@code Method Table} for objects (and arrays).
     */
    private static final int METHOD_OFFSET = -4;

    /**
     * Data structure that holds all objects and arrays of the virtual machine.
     */
    private DynamicIntArray heap;

    /**
     * Data structure that saves all base addresses. Needed for garbage collection.
     */
    private ArrayList<Integer> baseAdresses;

    /**
     * Create a new array heap. The data structure is created empty.
     */
    public Heap() {
        heap = new DynamicIntArray();
        baseAdresses = new ArrayList<>();
    }

    /**
     * Get the value of an entry specified by base and offset.
     *
     * @param base   the base of the array or object
     * @param offset the offset for the entry
     * @return the value of the entry
     * @throws NullPointerException if base is 0
     */
    private int getEntryAt(int base, int offset) {
        return heap.getElement(base + offset);
    }

    /**
     * Set the value of an entry specified by base and offset.
     *
     * @param base   the base of the array or object
     * @param offset the offset for the entry
     * @param value  the new value
     * @throws NullPointerException if base is 0
     */
    private void setEntryAt(int base, int offset, int value) {
        heap.setElement(base + offset, value);
    }

    /**
     * Get the value of a field of an object. This method is similar to {@link #getEntryAt(int, int)},
     * but checks, if the allocated space of the object is big enough to hold that field.
     *
     * @param base   the base of the object
     * @param offset the offset of the field
     * @return the value of the field
     * @throws InternalError if offset of field does not fit the size of the object.
     */
    public int getField(int base, int offset) {
        int length;
        try {
            length = getEntryAt(base, LENGTH_OFFSET);
        } catch (ArrayIndexOutOfBoundsException e) {
            throw new NullPointerException("Object reference is null.");
        }
        if (length + INFO_LENGTH < -offset)
            throw new InternalError("Offset of field too high for object.");
        else
            return getEntryAt(base, offset);
    }

    /**
     * Set the value of a field of an object. This method is similar to {@link #setEntryAt(int, int, int)},
     * but checks if the allocated space of the object is big enough to hold that field.
     *
     * @param base   the base of the object
     * @param offset the offset of the field
     * @param value  the new value of the field
     * @throws InternalError if offset of field does not fit the size of the object.
     */
    public void putField(int base, int offset, int value) {
        int length;
        try {
            length = getEntryAt(base, LENGTH_OFFSET);
        } catch (ArrayIndexOutOfBoundsException e) {
            throw new NullPointerException("Object reference is null.");
        }
        if (length + INFO_LENGTH < -offset)
            throw new InternalError("Index of field too high for object.");
        else
            setEntryAt(base, offset, value);
    }

    /**
     * Get the value of an element of an array. This method is similar to {@link #getEntryAt(int, int)},
     * but checks, if the allocated space of the array is big enough to hold that element.
     *
     * @param base  the base of the array
     * @param index the index of the field
     * @return the value of the array element
     * @throws ArrayIndexOutOfBoundsException if index illegal
     */
    public int getArrayElement(int base, int index) {
        int length;
        try {
            length = getEntryAt(base, LENGTH_OFFSET);
        } catch (ArrayIndexOutOfBoundsException e) {
            throw new NullPointerException("Array reference is null.");
        }
        if (length <= index || index < 0)
            throw new ArrayIndexOutOfBoundsException("Index " + index + " less than 0 or higher than length " + length + ".");
        else
            return getEntryAt(base, index);
    }

    /**
     * Set the value of an element of an array. This method is similar to {@link #setEntryAt(int, int, int)},
     * but checks, if the allocated space of the array is is big enough to hold that element.
     *
     * @param base  the base of the array
     * @param index the index of the field
     * @param value the new value of the field
     * @throws ArrayIndexOutOfBoundsException if index illegal
     */
    public void setArrayElement(int base, int index, int value) {
        int length;
        try {
            length = getEntryAt(base, LENGTH_OFFSET);
        } catch (ArrayIndexOutOfBoundsException e) {
            throw new NullPointerException("Array reference is null.");
        }
        if (length <= index || index < 0)
            throw new ArrayIndexOutOfBoundsException("Index " + index + " less than 0 or higher than length " + length + ".");
        else
            setEntryAt(base, index, value);
    }

    /**
     * Allocate memory for an object in the heap. This sets the allocated object to the status
     * {@link #STATUS_ALLOCATED} as well as the other information fields {@link #METHOD_OFFSET},
     * {@link #CD_OFFSET} and {@link #LENGTH_OFFSET}.
     *
     * @param fieldCount      the number of fields of the object
     * @param cdOffset        the offset of the class descriptor
     * @param methTableOffset the offset o the methodtable
     * @return the base address of the object
     */
    public int newObject(int fieldCount, int cdOffset, int methTableOffset) {
        int[] object = new int[fieldCount + INFO_LENGTH];
        heap.addElements(object);
        int base = heap.getPointerPosition();
        setEntryAt(base, LENGTH_OFFSET, fieldCount);
        setEntryAt(base, STATUS_OFFSET, STATUS_ALLOCATED);
        setEntryAt(base, CD_OFFSET, cdOffset);
        setEntryAt(base, METHOD_OFFSET, methTableOffset);
        baseAdresses.add(base);
        return base;
    }

    /**
     * Set an object to the {@link #STATUS_INITIALIZED}. This should happen while calling the {@code <init>} method
     * of an object via the instruction {@link tjvm.verifier.InstructionType#INVOKESPECIAL}.
     * This is not necessary for arrays, because they're initialized by default after allocation.
     *
     * @param base tbe base of the object
     */
    public void setInitialized(int base) {
        setEntryAt(base, STATUS_OFFSET, STATUS_INITIALIZED);
    }

    /**
     * Allocate memory for an array in the heap. This sets the allocated array to the status
     * {@link #STATUS_INITIALIZED} as well as the other information fields {@link #CD_OFFSET} with
     * {@link #DEFAULT_OFFSET} and {@link #LENGTH_OFFSET} with the size.
     *
     * @param size the size of the array
     * @return the base address of the array
     */
    public int newArray(int size) {
        int[] object = new int[size + INFO_LENGTH];
        heap.addElements(object);
        int base = heap.getPointerPosition() - size;
        setEntryAt(base, LENGTH_OFFSET, size);
        setEntryAt(base, STATUS_OFFSET, STATUS_ARRAY);
        setEntryAt(base, CD_OFFSET, DEFAULT_OFFSET);
        baseAdresses.add(base);
        return base;
    }

    /**
     * Get the offset to the {@code Method Table} method entries in the {@link MethodArea} for an
     * object specified by its base.
     *
     * @param objectBase the base of the object
     * @return the offset to the method entries in the {@code Method Table}
     */
    public int getMethTableOffset(int objectBase) {
        try {
            return getEntryAt(objectBase, METHOD_OFFSET);
        } catch (ArrayIndexOutOfBoundsException e) {
            throw new NullPointerException("Object reference is null.");
        }
    }

    /**
     * Get the offset to the {@code Class Descriptor} entry in the {@code Class Description Table}
     * in the {@link MethodArea} for an object specified by its base.
     *
     * @param objectBase the base of the object
     * @return the offset to the {@code Class Descriptor} in the {@code Class Description Table}
     */
    public int getClassDescriptorOffset(int objectBase) {
        try {
            return getEntryAt(objectBase, CD_OFFSET);
        } catch (ArrayIndexOutOfBoundsException e) {
            throw new NullPointerException("Object reference is null.");
        }
    }

    /**
     * Get the size of an object or array specified by its base.
     *
     * @param base the base of the object or array
     * @return the offset to the {@code Class Descriptor} in the {@code Class Description Table}
     */
    public int getSize(int base) {
        try {
            return getEntryAt(base, LENGTH_OFFSET);
        } catch (ArrayIndexOutOfBoundsException e) {
            throw new NullPointerException("Array reference is null.");
        }
    }

    /**
     * Get the status of an object or array specified by its base. The status can be
     * {@link #STATUS_ALLOCATED}, {@link #STATUS_INITIALIZED} or {@link #STATUS_ARRAY}.
     *
     * @param base the base of the object or array
     * @return the status
     */
    public int getStatus(int base) {
        try {
            return getEntryAt(base, STATUS_OFFSET);
        } catch (ArrayIndexOutOfBoundsException e) {
            throw new NullPointerException("Array reference is null.");
        }
    }


    /**
     * Returns a string representation of the heap with the contents of every field.
     *
     * @return the string representation of the heap
     */
    @Override
    public String toString() {
        int objCount = 1;
        int arrCount = 1;
        HashMap<Integer, String> descriptions = new HashMap<>();
        for (int base : baseAdresses) {
            if (getStatus(base) == STATUS_ARRAY) {
                descriptions.put(base + METHOD_OFFSET, "(Array[" + arrCount + "]-Header: method table offset)");
                descriptions.put(base + CD_OFFSET, "(Array[" + arrCount + "]-Header: class descriptor offset)");
                descriptions.put(base + STATUS_OFFSET, "(Array[" + arrCount + "]-Header: status offset: ARRAY)");
                descriptions.put(base + LENGTH_OFFSET, "(Array[" + arrCount + "]-Header: length)");
                for (int i = base; i < base + getSize(base); i++) {
                    descriptions.put(i, "(Array[" + arrCount + "]-Element: " + (i - base) + ")");
                }
                arrCount++;
            } else {
                String status = getStatus(base) == STATUS_ALLOCATED ? "ALLOCATED" : "INITIALIZED";
                descriptions.put(base + METHOD_OFFSET, "(Object[" + objCount + "]-Header: method table offset)");
                descriptions.put(base + CD_OFFSET, "(Object[" + objCount + "]-Header: class descriptor offset)");
                descriptions.put(base + STATUS_OFFSET, "(Object[" + objCount + "]-Header: status offset: " + status + ")");
                descriptions.put(base + LENGTH_OFFSET, "(Object[" + objCount + "]-Header: field count)");
                for (int i = base - INFO_LENGTH - 1; i > (base - INFO_LENGTH - 1 - getSize(base)); i--) {
                    descriptions.put(i, "(Object[" + objCount + "]-Field: " + ((i - base + INFO_LENGTH) * (-1)) + ")");
                }
                objCount++;
            }
        }

        StringBuilder builder = new StringBuilder();
        builder.append("\n--------- HEAP ---------\n");
        for (int i = 0; i < heap.getPointerPosition(); i++) {
            String desc = descriptions.getOrDefault(i, "");
            builder.append("#").append(i).append(": ").append(heap.getElement(i)).
                    append("\t\t ").append(desc).append("\n");
        }
        return builder.toString();
    }

    /**
     * Returns a string representation of the heap with a decompilation of the class
     * and fields names using the method area.
     *
     * @param methodArea the method area to get the class and field descriptors from
     * @return the string representation of the heap
     */
    public String toString(MethodArea methodArea) {
        int objCount = 1;
        int arrCount = 1;
        HashMap<Integer, String> descriptions = new HashMap<>();
        for (int base : baseAdresses) {
            if (getStatus(base) == STATUS_ARRAY) {
                descriptions.put(base + METHOD_OFFSET, "(Array[" + arrCount + "]-Header: method table offset)");
                descriptions.put(base + CD_OFFSET, "(Array[" + arrCount + "]-Header: class descriptor offset)");
                descriptions.put(base + STATUS_OFFSET, "(Array[" + arrCount + "]-Header: status offset: ARRAY)");
                descriptions.put(base + LENGTH_OFFSET, "(Array[" + arrCount + "]-Header: length)");
                for (int i = base; i < base + getSize(base); i++) {
                    descriptions.put(i, "(Array[" + arrCount + "]-Element: " + (i - base) + ")");
                }
                arrCount++;
            } else {
                String status = getStatus(base) == STATUS_ALLOCATED ? "ALLOCATED" : "INITIALIZED";
                int cdOffset = getClassDescriptorOffset(base);
                String className = "";
                for (Map.Entry<String, Integer> entry : methodArea.offsetTableClass.entrySet()) {
                    if (entry.getValue() == cdOffset)
                        className = entry.getKey();
                }
                descriptions.put(base + METHOD_OFFSET, "(Object[" + objCount + "]-Header (" + className + "): method table offset)");
                descriptions.put(base + CD_OFFSET, "(Object[" + objCount + "]-Header(" + className + "): class descriptor offset)");
                descriptions.put(base + STATUS_OFFSET, "(Object[" + objCount + "]-Header (" + className + "): status offset: " + status + ")");
                descriptions.put(base + LENGTH_OFFSET, "(Object[" + objCount + "]-Header (" + className + "): field count)");
                for (Map.Entry<String, Integer> entry : methodArea.offsetTableField.entrySet()) {
                    if (entry.getKey().startsWith(className)) {
                        int pos = entry.getValue() + base;
                        descriptions.put(pos, "(Object[" + objCount + "]-Field: " + ((pos - base + INFO_LENGTH + 1) * (-1)) + " - " + entry.getKey() + ")");
                    }
                }
                objCount++;
            }
        }

        StringBuilder builder = new StringBuilder();
        builder.append("\n--------- HEAP ---------\n");
        for (int i = 0; i < heap.getPointerPosition(); i++) {
            String desc = descriptions.getOrDefault(i, "");
            builder.append("#").append(i).append(": ").append(heap.getElement(i)).
                    append("\t\t ").append(desc).append("\n");
        }
        return builder.toString();
    }


}
