plugins {
    id("application")
    id("java")
}

application {
    mainClass = "tjvm.Main"
}

tasks.withType<Javadoc> {
    (options as StandardJavadocDocletOptions).addBooleanOption("html5", true)
    (options as CoreJavadocOptions).memberLevel = JavadocMemberLevel.PRIVATE
}


repositories {
    mavenCentral()
}

dependencies {
    implementation("commons-cli:commons-cli:1.4")
}
