In diesem Ordner befinden sich 3 Unterordner, die in ihrer Struktur erhalten bleiben sollten.

In dem Ordner "bin" befinden sich die ausführbaren Dateien "TinyJVM.bat" für Windows-Systeme
und "TinyJVM" für Linux-Systeme. Diese werden aufgerufen, um die TinyJava Virtual Machine
zu benutzen. Unten in diesem Readme-File sind die Optionen aufgelistet, von denen immer
genau eine zur Ausführung benutzt werden muss.
Anmerkung: In Windows kann "TinyJVM.bat" zur PATH-Variablen hinzugefügt werden, sodass in
Windows PowerShell oder der Eingabeaufforderung die Anwendung unkomplizierter genutzt
werden kann. (Anleitung z.B. hier: https://www.tutonaut.de/anleitung-pfad-path-variable-in-windows-einstellen/)
Das gleiche funktioniert selbstverständlich auch in Linux: https://wiki.ubuntuusers.de/Umgebungsvariable/

In dem Ordner "lib" sind die beiden jar-Dateien, die genutzt werden vorhanden, diese müssen dort
verbleiben und auch relativ zu dem Ordner "bin" auf der gleichen Stufe sein.

In dem Ordner "test" sind viele in Java implementierte Klassen und die kompilierten Klassendateien
vorhanden. Diese können zum testen der TinyJava Virtual Machine genutzt werden. Ausschließlich
die Klasse "Main" hat die Methode "static void tinyMain()" implementiert, sodass nur bei dieser
Klasse der Aufruf mit der Ausführen-Option "TinyJVM -r Main.class" zur korrekten Ausführung führt.
Die anderen Optionen können auch an den anderen Klassendateien ausprobiert werden.


usage: TinyJVM <option> <classfile>
 -?,--help                      Print this usage message.
 -ci,--classinit <classfile>    Initialize the class from the classfile
                                and print the method area.
 -d,--decompile <classfile>     Print the decompiled classfile (like javap
                                -v).
 -li,--link <classfile>         Link the class from the classfile and
                                print the method area.
 -lo,--load <classfile>         Load the class from the classfile and
                                print the method area.
 -oi,--objectinit <classfile>   Initialize an instance of the class from
                                the classfile and print the method area
                                and heap.
 -r,--run <classfile>           Run the static void tinyMain() method of
                                the class file.