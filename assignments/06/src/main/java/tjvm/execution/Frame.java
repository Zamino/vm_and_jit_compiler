package tjvm.execution;

/**
 * Class to store information per method invocation. The data areas defined by the
 * Java Virtual Machine Specification are:
 * <ul>
 * <li>Local Variables ({@link #locals})</li>
 * <li>Operand Stack ({@link #stack})</li>
 * <li>Reference to run-time constant pool ({@link #rcpRef})</li>
 * <li>Saved program counter for restoration ({@link #savePc})</li>
 * </ul>
 * <blockquote>
 * "A frame is used to store data and partial results, as well as to perform dynamic
 * linking, return values for methods [...].<p>
 * A new frame is created each time a method is invoked. A frame is destroyed when
 * its method invocation completes[...]. Frames are allocated from the Java Virtual Machine
 * stack (&sect;2.5.2) of the thread creating the frame. Each frame has its own array of
 * local variables (&sect;2.6.1), its own operand stack (&sect;2.6.2), and a reference to
 * the runtime constant pool (§2.5.5) of the class of the current method." (&sect;2.6 JVMS)
 * </blockquote>
 */
class Frame {

    /**
     * Array of local variables used by the Java Virtual Machine {@link Instruction}s. The values
     * stored in the local variables are the representation of local variables in Java methodes, e.g.: <p>
     * {@code int localVar = 1;} where {@code localVar} would be a local variable.
     * <blockquote>
     * "Each frame (&sect;2.6) contains an array of variables known as its local variables. The
     * length of the local variable array of a frame is determined at compile-time and
     * supplied in the binary representation of a class or interface along with the code for
     * the method associated with the frame (&sect;4.7.3).
     * A single local variable can hold a value of type boolean, byte, char, short, int,
     * float, reference, or returnAddress. A pair of local variables can hold a value
     * of type long or double.
     * Local variables are addressed by indexing. The index of the first local variable is
     * zero. An integer is considered to be an index into the local variable array if and only
     * if that integer is between zero and one less than the size of the local variable array."
     * (&sect;2.6.1 JVMS)
     * </blockquote>
     * As there are only int values in TinyJava, this is an int[] array
     */
    int[] locals;

    /**
     * Operand stack of values used by the Java Virtual Machine {@link Instruction}s. The values
     * stored on the operand stack are used to store values for processing within methods.
     */
    OperandStack stack;

    /**
     * Reference to the {@link tjvm.data.RuntimeCP} in the {@link tjvm.data.MethodArea}
     * that holds the symbolic information for methods, fields and classes as well as
     * constant int values.
     * <blockquote>
     * "Each frame (&sect;2.6) contains a reference to the run-time constant pool (&sect;2.6.1) for
     * the type of the current method to support dynamic linking of the method code.
     * The class file code for a method refers to methods to be invoked and variables
     * to be accessed via symbolic references. Dynamic linking translates these symbolic
     * method references into concrete method references, loading classes as necessary to
     * resolve as-yet-undefined symbols, and translates variable accesses into appropriate
     * offsets in storage structures associated with the run-time location of these variables.
     * This late binding of the methods and variables makes changes in other classes that
     * a method uses less likely to break this code." (&sect;2.6.3 JVMS)
     * </blockquote>
     */
    int rcpRef;

    /**
     * Field to save the current program counter from {@link Thread#pc}, when another method is invoked
     * and this frame ceases to be the current frame.
     * The program counter is a pointer to the Bytecode in the {@code Bytecode Table} in the
     * {@link tjvm.data.MethodArea} of the method currently being executed.
     * Usually the frame on top of the {@link FrameStack} would be responsible to reinstate
     * the program counter of the frame below it, but as most of the logic to run {@link Instruction}s
     * is in the {@link Interpreter}, saving the program counter of a frame in the frame itself
     * looks better.
     * <blockquote>
     * "A method invocation completes normally if that invocation does not cause an
     * exception (&sect;2.10) to be thrown, either directly from the Java Virtual Machine or as
     * a result of executing an explicit throw statement. If the invocation of the current
     * method completes normally, then a value may be returned to the invoking method.
     * This occurs when the invoked method executes one of the return instructions
     * (&sect;2.11.8), the choice of which must be appropriate for the type of the value being
     * returned (if any).
     * The current frame (&sect;2.6) is used in this case to restore the state of the invoker,
     * including its local variables and operand stack, with the program counter of the
     * invoker appropriately incremented to skip past the method invocation instruction.
     * Execution then continues normally in the invoking method's frame with the
     * returned value (if any) pushed onto the operand stack of that frame." (&sect;2.6.4 JVMS)
     * </blockquote>
     */
    int savePc;

    /**
     * Field to save the start of Bytecode in the {@code Bytecode Table} in the {@link tjvm.data.MethodArea}
     * of the method represented by that frame. This is only used for better error-messages to show the index
     * of the {@link Instruction} within the method and not the whole {@code Bytecode Table}.
     */
    int startPc;

    /**
     * Identifier of the method the frame represents. Used for debugging.
     */
    String methodIdentifier;

    /**
     * Create a new frame, which happens each time a method is invoked.
     *
     * @param maxStack         maximum height of operand stack
     * @param maxLocals        maximum size of local variable array
     * @param rcpRef           reference (index) to runtime constant pool
     * @param startPc          start-index of instructions in {@code Bytecode Table}
     * @param methodIdentifier identifier of the method for debugging and final-check
     */
    Frame(int maxStack, int maxLocals, int rcpRef, int startPc, String methodIdentifier) {
        stack = new OperandStack(maxStack);

        locals = new int[maxLocals];
        this.rcpRef = rcpRef;

        savePc = startPc;
        this.startPc = startPc;

        this.methodIdentifier = methodIdentifier;
    }
}
