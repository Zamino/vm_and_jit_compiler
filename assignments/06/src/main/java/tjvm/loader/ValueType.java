package tjvm.loader;

/**
 * Enumeration for value types in Tiny Java. They can only be of the types {@code int}, {@code int[]} or
 * a class programmed in TinyJava.
 */
public enum ValueType {
    /** Value type {@code int}. */
    INT,
    /** Value type {@code int[]}. */
    INTARR,
    /** Value type of a class. */
    REF
}
