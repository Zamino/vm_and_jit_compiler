package tjvm.loader;

/**
 * Class that represents a {@code CONSTANT_Integer_info} structure in the constant pool. It only contains an
 * {@code int} value, which is used by instructions or for final fields (&sect;4.4.4).
 */
class CPIntEntry extends CPEntry {

  /**
   * Create a new {@code CONSTANT_Integer_info} structure for the constant pool.
   *
   * @param constValue {@code int} value of the entry
   */
  CPIntEntry(int constValue) {
    this.constValue = constValue;
  }

  @Override
  public String toString() {
    return "#" + cpIndex + " = Integer\t\t" + constValue;
  }

  @Override
  public void resolveIdentifier(ConstantPool constantPool) {
    identifier = Integer.toString(constValue);
  }
}
