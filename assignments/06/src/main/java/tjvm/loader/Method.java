package tjvm.loader;

import tjvm.verifier.BytecodeVerifier;
import tjvm.verifier.VerifierInstruction;

import java.util.SortedMap;
import java.util.Vector;

/**
 * Class that holds all information about a method (&sect;4.6) of the {@code class} file after loading.
 * The methods are saved in the {@link MethodList}.
 */
public class Method {

    /**
     * The identifier which is used for easy identification of the method in the pattern of {@code Class.method:descriptor}
     */
    public String identifier;
    /**
     * Shows, if the method is static.
     */
    public boolean isStatic;
    /**
     * The {@code code} contains virtual machine instructions and their arguments.
     */
    public byte[] code;
    /**
     * The maximum number of values in the local variables while execution of this method.
     */
    public int maxLocals;
    /**
     * The name of the method.
     */
    public String name;
    /**
     * {@code true}, if code attribute has been read already.
     */
    boolean hasCodeAttribute;
    /**
     * The descriptor of the method.
     */
    String descriptor;
    /**
     * Return type of the method. If {@code true}, method returns int, if {@code false}, method has no return value.
     */
    public boolean returnsInt;
    /**
     * Representation of the {@code access_flags}.
     */
    short flags;
    /**
     * The number of arguments for the method (parameters + this for initialization or methods ofobjects).
     */
    public int argsSize;
    /**
     * The types of the parameters of the method. This can be {@link ValueType#INT},
     * {@link ValueType#INTARR} or {@link ValueType#REF}}.
     */
    public Vector<ValueType> parameters;
    /**
     * The String representation of the parameters of the method. This can be {@code int}, {@code int[]} or a class name.
     */
    public Vector<String> parameterNames;
    /**
     * The maximum number of values on the stack while execution of this method.
     */
    public int maxStack;
    /**
     * The number of bytes the {@code code} array is made of.
     */
    int codeLength;
    /**
     * The positions of the program counter from the {@code LineNumberTable}
     * attribute where the line in the original source file changes.
     */
    short[] startPcs;
    /**
     * The corresponding original source file line numbers for the position of the program
     * counter of the {@code LineNumberTable} attribute.
     */
    short[] lineNumbers;

    /**
     * Create an empty method.
     */
    Method() {
        parameters = new Vector<>();
        parameterNames = new Vector<>();
    }

    /**
     * Returns the string representation of a method where every line is indented.
     * The representation looks similar to
     * <blockquote>{@code javap} - The Java Class File Disassembler</blockquote>
     * using the {@code -verbose} option.
     *
     * @return The string representation of the method
     */
    @Override
    public String toString() {
        //first line
        StringBuilder res = new StringBuilder();
        res.append("\t");
        res.append(isStatic ? "static " : "");
        res.append(returnsInt ? "int " : "void ");
        res.append(name).append("(");
        for (int i = 0; i < parameters.size(); i++) {
            res.append(parameterNames.get(i));
            res.append(i == parameters.size() - 1 ? "" : ", ");
        }
        res.append(");\n");

        //second line
        res.append("\tdescriptor: ").append(descriptor).append("\n");

        //third line
        res.append("\tflags: ").append(Integer.toBinaryString(flags)).append("\n");

        //Code
        res.append("\tCode:\n");
        res.append("\t\tstack=").append(maxStack);
        res.append(", locals=").append(maxLocals);
        res.append(", args_size=").append(argsSize).append("\n");

        SortedMap<Integer, VerifierInstruction> instructions = new BytecodeVerifier().breakUp(this);
        for (VerifierInstruction inst : instructions.values()) {
            res.append("\t\t\t").append(inst.toString()).append("\n");
        }

        return res.toString();
    }
}