#include <cstdlib>
#include <cstdio>
#include <cassert>

int STACK_SIZE = 16;
int VARIABLE_SIZE = 16;
int LABEL_SIZE = 16;

typedef enum {
  // arithmetic
  ADD,
  SUB,
  DIV,
  MUL,
  MOD,
  // stack
  PUSH, // <value>
  POP,
  // variable
  LOAD, // <variable>
  STORE, // <variable>
  // flow
  LABEL, // <label>
  JMP, // <label>
  JMPC, // <label>
  // function
  RETURN,
  // print / debug
  Print,
  PrintVar, // <pos>
  PrintAll
} instruction_t;

int interpret(int* program) {
  instruction_t* pc = (instruction_t*) program;
  int stack[STACK_SIZE];
  int* stack_ptr = stack + STACK_SIZE; // point after end
  int variables[VARIABLE_SIZE];
  instruction_t* labels[LABEL_SIZE];

  int tmp_label;
  for (;;) {
    switch (*pc++) {
      case ADD:
        stack_ptr[1] += stack_ptr [0];
        stack_ptr++;
        break;
      case SUB:
        stack_ptr[1] -= stack_ptr [0];
        stack_ptr++;
        break;
      case DIV:
        stack_ptr[1] /= stack_ptr [0];
        stack_ptr++;
        break;
      case MUL:
        stack_ptr[1] *= stack_ptr [0];
        stack_ptr++;
        break;
      case MOD:
        stack_ptr[1] %= stack_ptr [0];
        stack_ptr++;
        break;
      case PUSH: // <value>
        stack_ptr--;
        stack_ptr[0] = *pc++;
        break;
      case POP:
        stack_ptr++;
        break;
      // variable
      case LOAD: // <variable>
        stack_ptr--;
        stack_ptr[0] = variables[*pc++];
        break;
      case STORE: // <variable>
        variables[*pc++] = stack_ptr[0];
        break;
      // flow
      case LABEL: // <label>
        tmp_label = *pc++;
        labels[tmp_label] = pc;
        break;
      case JMP: // <label>
        tmp_label = *pc++;
        pc = labels[tmp_label];
        break;
      case JMPC: // <label>
        tmp_label = *pc++;
        if (stack_ptr[0]) {
          pc = labels[tmp_label];
        }
        break;
      // function
      case RETURN:
        return stack_ptr[0];
        break;
      // print / debug
      case Print:
        printf("STACK TOP: %d\n", stack_ptr[0]);
        break;
      case PrintVar: // <pos>
        printf("Variable: %d\n", variables[*pc++]);
        break;
      case PrintAll:
        printf("STACK:\n");
        for(int i=1; i<=STACK_SIZE; i++) {
          printf("pos %d: %d\n",i, stack[STACK_SIZE - i]);
        }
        printf("Variables:\n");
        for(int i=0; i<VARIABLE_SIZE; i++) {
          printf("var %d: %d\n",i, variables[i]);
        }
        break;
    }
  }
}

void test_arithmetic_operations() {
  int program_5_plus_7[] = {
    PUSH, 5,
    PUSH, 7,
    ADD,
    RETURN
  };
  assert(("5+7=12", 12 == interpret(program_5_plus_7)));

  int program_5_sub_7[] = {
    PUSH, 5,
    PUSH, 7,
    SUB,
    RETURN
  };
  assert(("5-7=-2", -2 == interpret(program_5_sub_7)));

  int program_5_mul_7[] = {
    PUSH, 5,
    PUSH, 7,
    MUL,
    RETURN
  };
  assert(("5*7=35", 35 == interpret(program_5_mul_7)));

  int program_15_div_5[] = {
    PUSH, 15,
    PUSH, 5,
    DIV,
    RETURN
  };
  assert(("15*5=3", 3 == interpret(program_15_div_5)));

  int program_15_mod_4[] = {
    PUSH, 15,
    PUSH, 4,
    MOD,
    RETURN
  };
  assert(("15%4=3", 3 == interpret(program_15_mod_4)));
}

void test_variables() {
  int program_1_variable[] = {
    PUSH, 5,
    STORE, 0,
    PUSH, 6,
    LOAD, 0,
    RETURN
  };
  assert(("v0=5", 5 == interpret(program_1_variable)));

  int program_2_variable[] = {
    PUSH, 5,
    STORE, 0,
    POP,
    PUSH, 6,
    STORE, 1,
    POP,
    LOAD, 0,
    LOAD, 1,
    ADD,
    STORE, 0,
    POP,
    LOAD, 0,
    RETURN
  };
  assert(("v0=5;v1=6;v0=v0+v1;return v0", 11 == interpret(program_2_variable)));
}

void test_jumps() {
  int program_count_loops[] = {
    PUSH, 0,
    STORE, 0, // sum
    STORE, 1, // count loop
    LABEL, 0, // loop
    POP, // remove cmp value
    // add 1 to count loop
    LOAD, 1,
    PUSH, 1,
    ADD,
    STORE, 1,
    POP,
    //
    LOAD, 0, // sum
    PUSH, 10,
    ADD,
    STORE, 0,
    PUSH, 100, // cmp value
    SUB,
    JMPC, 0, // jump loop
    LOAD, 1, // count loop
    // Print,
    // PrintVar, 0,
    // PrintAll,
    RETURN
  };

  assert(("count loops", 10 == interpret(program_count_loops)));
}


int main () {

  test_arithmetic_operations();
  test_variables();
  test_jumps();
}

