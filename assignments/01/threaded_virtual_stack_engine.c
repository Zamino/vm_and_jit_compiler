#include <cstdlib>
#include <cstdio>
#include <cassert>
#include <stdio.h>

int STACK_SIZE = 16;
int VARIABLE_SIZE = 16;
int LABEL_SIZE = 16;

void interpret() {

  void* program_5_plus_7[] = {
    &&l_PUSH, (void *) 5,
    &&l_PUSH, (void *) 7,
    &&l_ADD,
    &&l_RETURN
  };

  void* program_5_sub_7[] = {
    &&l_PUSH, (void *) 5,
    &&l_PUSH, (void *) 7,
    &&l_SUB,
    &&l_RETURN
  };

  void* program_5_mul_7[] = {
    &&l_PUSH, (void *) 5,
    &&l_PUSH, (void *) 7,
    &&l_MUL,
    &&l_RETURN
  };

  void* program_15_div_5[] = {
    &&l_PUSH, (void *) 15,
    &&l_PUSH, (void *) 5,
    &&l_DIV,
    &&l_RETURN
  };

  void* program_15_mod_4[] = {
    &&l_PUSH, (void *) 15,
    &&l_PUSH, (void *) 4,
    &&l_MOD,
    &&l_RETURN
  };

  void* program_1_variable[] = {
    &&l_PUSH, (void *) 5,
    &&l_STORE, (void *) 0,
    &&l_PUSH, (void *) 6,
    &&l_LOAD, (void *) 0,
    &&l_RETURN
  };

  void* program_2_variable[] = {
    &&l_PUSH, (void *) 5,
    &&l_STORE, (void *) 0,
    &&l_POP,
    &&l_PUSH, (void *) 6,
    &&l_STORE, (void *) 1,
    &&l_POP,
    &&l_LOAD, (void *) 0,
    &&l_LOAD, (void *) 1,
    &&l_ADD,
    &&l_STORE, (void *) 0,
    &&l_POP,
    &&l_LOAD, (void *) 0,
    &&l_RETURN
  };

  void* program_count_loops[] = {
    &&l_PUSH, (void *) 0,
    &&l_STORE, (void *) 0, // sum
    &&l_STORE, (void *) 1, // count loop
    &&l_LABEL, (void *) 0, // loop
    &&l_POP, // remove cmp value
    // add 1 to count loop
    &&l_LOAD, (void *) 1,
    &&l_PUSH, (void *) 1,
    &&l_ADD,
    &&l_STORE, (void *) 1,
    &&l_POP,
    //
    &&l_LOAD, (void *) 0, // sum
    &&l_PUSH, (void *) 10,
    &&l_ADD,
    &&l_STORE, (void *) 0,
    &&l_PUSH, (void *) 100, // cmp value
    &&l_SUB,
    &&l_JMPC, (void *) 0, // jump loop
    &&l_LOAD, (void *) 1, // count loop
    // Print,
    // PrintVar, 0,
    // PrintAll,
    &&l_RETURN
  };

  void** program = program_count_loops;
  long retrun_value = 10;

  void** pc = program;
  long stack[STACK_SIZE];
  long* stack_ptr = stack + STACK_SIZE; // point after end
  long variables[VARIABLE_SIZE];
  void** labels[LABEL_SIZE];

  int tmp_label;

  goto **(pc++); // start program

l_ADD:
  stack_ptr[1] += stack_ptr [0];
  stack_ptr++;
  goto **(pc++);
l_SUB:
  stack_ptr[1] -= stack_ptr [0];
  stack_ptr++;
  goto **(pc++);
l_DIV:
  stack_ptr[1] /= stack_ptr [0];
  stack_ptr++;
  goto **(pc++);
l_MUL:
  stack_ptr[1] *= stack_ptr [0];
  stack_ptr++;
  goto **(pc++);
l_MOD:
  stack_ptr[1] %= stack_ptr [0];
  stack_ptr++;
  goto **(pc++);
l_PUSH: // <value>
  stack_ptr--;
  stack_ptr[0] = (long) *(pc++);
  goto **(pc++);
l_POP:
  stack_ptr++;
  goto **(pc++);
// variable
l_LOAD: // <variable>
  stack_ptr--;
  stack_ptr[0] = variables[(long) *(pc++)];
  goto **(pc++);
l_STORE: // <variable>
  variables[(long) *(pc++)] = stack_ptr[0];
  goto **(pc++);
// flow
l_LABEL: // <label>
  tmp_label = (long) *(pc++);
  labels[tmp_label] = pc;
  goto **(pc++);
l_JMP: // <label>
  tmp_label = (long) *(pc++);
  pc = labels[tmp_label];
  goto **(pc++);
l_JMPC: // <label>
  tmp_label = (long) *(pc++);
  if (stack_ptr[0]) {
    pc = labels[tmp_label];
  }
  goto **(pc++);
// function
l_RETURN:
  goto l_END;
// print / debug
l_Print:
  printf("STACK TOP: %ld\n", stack_ptr[0]);
  goto **(pc++);
l_PrintVar: // <pos>
  printf("Variable: %ld\n", variables[(long) *(pc++)]);
  goto **(pc++);
PrintAll:
  printf("STACK:\n");
  for(int i=1; i<=STACK_SIZE; i++) {
    printf("pos %d: %ld\n",i, stack[STACK_SIZE - i]);
  }
  printf("Variables:\n");
  for(int i=0; i<VARIABLE_SIZE; i++) {
    printf("var %d: %ld\n",i, variables[i]);
  }
  goto **(pc++);

l_END:
  printf("Return value: %ld\n", stack_ptr[0]);
  assert(retrun_value == stack_ptr[0]);

  return;
}

int main () {

  printf("Start Program\n");
  interpret();
  printf("End Program\n");
}

