package tjvm.converter;

enum RegisterType {
  TEMPORAL,
  LITERAL, // constant pool
  CONSTANT
}

public class Register {

  static int TEMP_COUNT = 0;

  RegisterType type;
  int value;
  String name;
  Integer constValue;

  Register(RegisterType type, int value, String name) {
    this.type  = type;
    this.value = value;
    this.name  = name;
    if (type == RegisterType.CONSTANT)
      constValue = value;
  }

  Register(RegisterType type, int value) {
    this.type  = type;
    this.value = value;
    if (type == RegisterType.CONSTANT)
      constValue = value;
  }

  Register(RegisterType type) {
    int value = -1;
    if (type == RegisterType.TEMPORAL)
      value = TEMP_COUNT++;

    this.type  = type;
    this.value = value;
  }

  /**
   * Returns the string representation of the instruction.
   *
   * @return The string representation of the instruction
   */
  public String toString() {
    StringBuilder res = new StringBuilder();
    switch (type) {
      case TEMPORAL: res.append("T(").append(value).append(")"); break;
      case LITERAL:  res.append("L(").append(value).append(")[").append(name).append("]"); break;
      case CONSTANT:              res.append(value); break;
    }
    return res.toString();
  }
}

