package tjvm.converter;

import tjvm.verifier.VerifierInstruction;

import java.util.NavigableMap;
import java.util.TreeMap;
import java.util.Optional;
import java.util.Stack;

public class BasicBlock implements Comparable<BasicBlock> {

  Integer start_index;
  int end_index;
  NavigableMap<Integer, VerifierInstruction> stack_instructions;
  NavigableMap<Integer, RegisterInstruction> reg_instructions;
  Stack<Register> end_register_stack;
  Register[] end_virtual_registers;
  NavigableMap<Integer, BasicBlock> predecessors;
  Optional<BasicBlock> jump;
  Optional<BasicBlock> no_jump;

  BasicBlock(int start_index, int end_index, NavigableMap<Integer, VerifierInstruction> stack_instructions) {
    this.start_index = start_index;
    this.end_index = end_index;
    this.stack_instructions = stack_instructions;
    this.reg_instructions = new TreeMap();
    predecessors = new TreeMap<Integer, BasicBlock>();
    jump = Optional.empty();
    no_jump = Optional.empty();
  }

  @Override public int compareTo(BasicBlock a) {
    return this.start_index.compareTo(a.start_index);
  }

  public String toString() {
    StringBuilder res = new StringBuilder();
    res.append(start_index + ":\n");
    for (VerifierInstruction inst : stack_instructions.values()) {
      res.append("  " + inst + "\n");
    }
    return res.toString();
  }

  public String toStringRegister() {
    StringBuilder res = new StringBuilder();
    res.append(start_index + ":\n");
    for (RegisterInstruction inst : reg_instructions.values()) {
      res.append("  " + inst + "\n");
    }
    res.append("register stack:\n");
    for (Register reg : end_register_stack) {
      res.append("  " + reg + "\n");
    }
    return res.toString();
  }
}
