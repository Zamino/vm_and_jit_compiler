package tjvm.converter;

import tjvm.verifier.VerifierInstruction;
import tjvm.verifier.InstructionType;

import tjvm.data.RuntimeCPEntry;
import tjvm.loader.DerivedClass;
import tjvm.loader.Method;
import tjvm.loader.TextChecker;

import java.util.EmptyStackException;
import java.util.NoSuchElementException;
import java.util.NavigableMap;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.Objects;
import java.util.Stack;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.Optional;

import java.io.File;
import java.io.PrintWriter;
import java.io.IOException;

public class RegisterCodeConverter {

  /**
   * Original stack instructions of the method that is analyzed.
   */
  private NavigableMap<Integer, VerifierInstruction> orig_instructions;

  /**
   * Converted regisert instructions of the method that is analyzed.
   */
  private NavigableMap<Integer, RegisterInstruction> conv_instructions;

  private NavigableMap<Integer, BasicBlock> basic_blocks;

  /**
   * The class the method is from. Necessary for constant pool entries etc.
   */
  private DerivedClass dc;

  /**
   * The method table entry of the method to be analyzed.
   */
  private Method method;

  /**
   * Initialize the register code converter.
   * @param instructions the parsed instructions to be converted
   * @param dc           the derived information from the {@code class} file
   * @param method       the specific derived information of the method
   * @throws VerifyError if more parameters that local variables
   */
  public RegisterCodeConverter(NavigableMap<Integer, VerifierInstruction> stack_instructions, DerivedClass dc, Method method) {
    this.orig_instructions = stack_instructions;
    this.dc = dc;
    this.method = method;
  }

  public void convert() {

    splitBasicBlocks();

    try {
      generateDotStackGraph();
    } catch (IOException e) {
      System.out.println("Could not create graph files.");
    }

    Register.TEMP_COUNT = 0; // reset
    Register[] current_virtual_registers = new Register[method.maxLocals];
    for (int i=0; i < method.argsSize; i++) {
      current_virtual_registers[i] = new Register(RegisterType.TEMPORAL);
    }
    TreeSet<BasicBlock> todo_blocks = new TreeSet<BasicBlock>();
    todo_blocks.add(basic_blocks.firstEntry().getValue());
    while (!todo_blocks.isEmpty()) {
      BasicBlock current_block = todo_blocks.pollFirst();
      int block_reg_inst_index = 0;

      Stack<Register> block_reg_stack = new Stack<>();

      // add register of predecessors
      for (BasicBlock pre_block : current_block.predecessors.values()) {
        if (pre_block.end_register_stack != null) {
          Stack<Register> pre_register_stack = pre_block.end_register_stack;
          current_virtual_registers = pre_block.end_virtual_registers.clone();

          if (current_block.predecessors.size() > 1) {
            for (int i=0; i < pre_register_stack.size(); i++) {
              RegisterInstruction phi_inst = new RegisterInstruction(
                  block_reg_inst_index++,
                  RegisterInstructionType.PHI
                  );
              phi_inst.registers = new Register[2];
              phi_inst.registers[0] = new Register(RegisterType.TEMPORAL);
              phi_inst.registers[1] = new Register(RegisterType.CONSTANT, i);
              current_block.reg_instructions.put(phi_inst.index, phi_inst);
              block_reg_stack.add(phi_inst.registers[0]);
            }
          } else {
            block_reg_stack = (Stack)pre_register_stack.clone();
          }
          break;
        }
      }

      for (VerifierInstruction inst : current_block.stack_instructions.values()) {
        RegisterInstruction reg_inst;
        RegisterInstructionType reg_inst_type;
        Register reg_1 = null;
        Register reg_2 = null;
        Register reg_3 = null;
        Register reg_4 = null;
        int local_var;
        int const_value;
        int parameter_count;
        int register_count;
        Boolean has_return;
        String descriptor;

        switch(inst.type) { // {{{
          // jump-instructions
          case IFEQ:
          case IFNE:
          case IFGT:
          case IFGE:
          case IFLT:
          case IFLE:
          case IFNONNULL:
          case IFNULL:
            reg_1 = block_reg_stack.pop();
            switch(inst.type) {
              case IFEQ: reg_inst_type = RegisterInstructionType.IFEQ; break;
              case IFNE: reg_inst_type = RegisterInstructionType.IFNE; break;
              case IFGT: reg_inst_type = RegisterInstructionType.IFGT; break;
              case IFGE: reg_inst_type = RegisterInstructionType.IFGE; break;
              case IFLT: reg_inst_type = RegisterInstructionType.IFLT; break;
              case IFLE: reg_inst_type = RegisterInstructionType.IFLE; break;
              case IFNONNULL: reg_inst_type = RegisterInstructionType.IFNONNULL; break;
              case IFNULL:    reg_inst_type = RegisterInstructionType.IFNULL;    break;
              default: reg_inst_type = null;
            }
            reg_inst = new RegisterInstruction(block_reg_inst_index++, reg_inst_type);
            reg_inst.registers = new Register[2];
            reg_inst.registers[0] = reg_1;
            reg_inst.registers[1] = new Register(RegisterType.CONSTANT, inst.op1);
            current_block.reg_instructions.put(reg_inst.index, reg_inst);
            break;
          case IF_ICMPEQ:
          case IF_ICMPNE:
          case IF_ICMPGT:
          case IF_ICMPGE:
          case IF_ICMPLT:
          case IF_ICMPLE:
          case IF_ACMPEQ:
          case IF_ACMPNE:
            reg_1 = block_reg_stack.pop();
            reg_2 = block_reg_stack.pop();
            switch(inst.type) {
              case IF_ICMPEQ: reg_inst_type = RegisterInstructionType.IF_ICMPEQ; break;
              case IF_ICMPNE: reg_inst_type = RegisterInstructionType.IF_ICMPNE; break;
              case IF_ICMPGT: reg_inst_type = RegisterInstructionType.IF_ICMPGT; break;
              case IF_ICMPGE: reg_inst_type = RegisterInstructionType.IF_ICMPGE; break;
              case IF_ICMPLT: reg_inst_type = RegisterInstructionType.IF_ICMPLT; break;
              case IF_ICMPLE: reg_inst_type = RegisterInstructionType.IF_ICMPLE; break;
              case IF_ACMPEQ: reg_inst_type = RegisterInstructionType.IF_ICMPEQ; break;
              case IF_ACMPNE: reg_inst_type = RegisterInstructionType.IF_ICMPNE; break;
              default: reg_inst_type = null;
            }
            reg_inst = new RegisterInstruction(block_reg_inst_index++, reg_inst_type);
            reg_inst.registers = new Register[3];
            reg_inst.registers[0] = reg_1;
            reg_inst.registers[1] = reg_2;
            reg_inst.registers[2] = new Register(RegisterType.CONSTANT, inst.op1);
            current_block.reg_instructions.put(reg_inst.index, reg_inst);
            break;
          case GOTO:
          case GOTO_W:
            switch(inst.type) {
              case GOTO:   reg_inst_type = RegisterInstructionType.GOTO;   break;
              case GOTO_W: reg_inst_type = RegisterInstructionType.GOTO_W; break;
              default: reg_inst_type = null;
            }
            reg_inst = new RegisterInstruction(block_reg_inst_index++, reg_inst_type);
            reg_inst.registers = new Register[1];
            reg_inst.registers[0] = new Register(RegisterType.CONSTANT, inst.op1);
            current_block.reg_instructions.put(reg_inst.index, reg_inst);
            break;

            //local variable instruction
          case ALOAD_0:
          case ALOAD_1:
          case ALOAD_2:
          case ALOAD_3:
          case ALOAD:
             local_var = inst.type == InstructionType.ALOAD ?
                 inst.op1
               : inst.type.opcode - InstructionType.ALOAD_0.opcode;
            reg_inst = new RegisterInstruction(block_reg_inst_index++, RegisterInstructionType.MOVE);
            reg_1 = new Register(RegisterType.TEMPORAL);
            reg_2 = current_virtual_registers[local_var];
            reg_inst.registers = new Register[2];
            reg_inst.registers[0] = reg_1;
            reg_inst.registers[1] = reg_2;
            block_reg_stack.push(reg_1);
            current_block.reg_instructions.put(reg_inst.index, reg_inst);
            break;

          case ILOAD_0:
          case ILOAD_1:
          case ILOAD_2:
          case ILOAD_3:
          case ILOAD:
             local_var = inst.type == InstructionType.ILOAD ?
                 inst.op1
               : inst.type.opcode - InstructionType.ILOAD_0.opcode;
            reg_inst = new RegisterInstruction(block_reg_inst_index++, RegisterInstructionType.MOVE);
            reg_1 = new Register(RegisterType.TEMPORAL);
            reg_2 = current_virtual_registers[local_var];
            reg_inst.registers = new Register[2];
            reg_inst.registers[0] = reg_1;
            reg_inst.registers[1] = reg_2;
            block_reg_stack.push(reg_1);
            current_block.reg_instructions.put(reg_inst.index, reg_inst);
            break;

          case ASTORE_0:
          case ASTORE_1:
          case ASTORE_2:
          case ASTORE_3:
          case ASTORE:
             local_var = inst.type == InstructionType.ASTORE ?
                 inst.op1
               : inst.type.opcode - InstructionType.ASTORE_0.opcode;
            current_virtual_registers[local_var] = block_reg_stack.pop();
            break;
          case ISTORE_0:
          case ISTORE_1:
          case ISTORE_2:
          case ISTORE_3:
          case ISTORE:
             local_var = inst.type == InstructionType.ISTORE ?
                 inst.op1
               : inst.type.opcode - InstructionType.ISTORE_0.opcode;
            current_virtual_registers[local_var] = block_reg_stack.pop();
            break;

            // constant-pool instructions
          case LDC:
          case LDC_W:
            reg_inst = new RegisterInstruction(block_reg_inst_index++, RegisterInstructionType.MOVE);
            reg_1 = new Register(RegisterType.TEMPORAL);
            {
              RuntimeCPEntry cpEntry = dc.runtimeCP.getEntry(inst.op1);
              reg_2 = new Register(RegisterType.LITERAL, inst.op1, cpEntry.toString());
            }
            reg_inst.registers = new Register[2];
            reg_inst.registers[0] = reg_1;
            reg_inst.registers[1] = reg_2;
            block_reg_stack.push(reg_1);
            current_block.reg_instructions.put(reg_inst.index, reg_inst);
            break;

          case NEW:
            reg_inst = new RegisterInstruction(block_reg_inst_index++, RegisterInstructionType.NEW);
            reg_1 = new Register(RegisterType.TEMPORAL);
            {
              RuntimeCPEntry cpEntry = dc.runtimeCP.getEntry(inst.op1);
              reg_2 = new Register(RegisterType.LITERAL, inst.op1, cpEntry.toString());
            }
            reg_inst.registers = new Register[2];
            reg_inst.registers[0] = reg_1;
            reg_inst.registers[1] = reg_2;
            block_reg_stack.push(reg_1);
            current_block.reg_instructions.put(reg_inst.index, reg_inst);
            break;

          case PUTFIELD:
            reg_inst = new RegisterInstruction(block_reg_inst_index++, RegisterInstructionType.PUTFIELD);
            {
              RuntimeCPEntry cpEntry = dc.runtimeCP.getEntry(inst.op1);
              reg_1 = new Register(RegisterType.LITERAL, inst.op1, cpEntry.toString());
            }
            reg_3 = block_reg_stack.pop(); // value
            reg_2 = block_reg_stack.pop(); // object ref
            reg_inst.registers = new Register[3];
            reg_inst.registers[0] = reg_1;
            reg_inst.registers[1] = reg_2;
            reg_inst.registers[2] = reg_3;
            current_block.reg_instructions.put(reg_inst.index, reg_inst);
            break;

          case PUTSTATIC:
            reg_inst = new RegisterInstruction(block_reg_inst_index++, RegisterInstructionType.MOVE);
            {
              RuntimeCPEntry cpEntry = dc.runtimeCP.getEntry(inst.op1);
              reg_1 = new Register(RegisterType.LITERAL, inst.op1, cpEntry.toString());
            }
            reg_2 = block_reg_stack.pop();
            reg_inst.registers = new Register[2];
            reg_inst.registers[0] = reg_1;
            reg_inst.registers[1] = reg_2;
            current_block.reg_instructions.put(reg_inst.index, reg_inst);
            break;

          case GETFIELD:
            reg_inst = new RegisterInstruction(block_reg_inst_index++, RegisterInstructionType.GETFIELD);
            reg_1 = new Register(RegisterType.TEMPORAL); // store value
            {
              RuntimeCPEntry cpEntry = dc.runtimeCP.getEntry(inst.op1);
              reg_2 = new Register(RegisterType.LITERAL, inst.op1, cpEntry.toString());
            }
            reg_3 = block_reg_stack.pop(); // object ref
            reg_inst.registers = new Register[3];
            reg_inst.registers[0] = reg_1;
            reg_inst.registers[1] = reg_2;
            reg_inst.registers[2] = reg_3;
            block_reg_stack.push(reg_1);
            current_block.reg_instructions.put(reg_inst.index, reg_inst);
            break;

          case GETSTATIC:
            reg_inst = new RegisterInstruction(block_reg_inst_index++, RegisterInstructionType.MOVE);
            reg_1 = new Register(RegisterType.TEMPORAL);
            {
              RuntimeCPEntry cpEntry = dc.runtimeCP.getEntry(inst.op1);
              reg_2 = new Register(RegisterType.LITERAL, inst.op1, cpEntry.toString());
            }
            reg_inst.registers = new Register[2];
            reg_inst.registers[0] = reg_1;
            reg_inst.registers[1] = reg_2;
            block_reg_stack.push(reg_1);
            current_block.reg_instructions.put(reg_inst.index, reg_inst);
            break;

          case INVOKESTATIC:
            reg_inst = new RegisterInstruction(block_reg_inst_index++, RegisterInstructionType.CALL);
            {
              RuntimeCPEntry cpEntry = dc.runtimeCP.getEntry(inst.op1);
              reg_1 = new Register(RegisterType.LITERAL, inst.op1, cpEntry.toString());
              descriptor = cpEntry.getDescriptorSubstring();
            }

            {
              String parameters = descriptor.substring(descriptor.indexOf("(") + 1, descriptor.indexOf(")"));
              parameter_count = getParameterCount(parameters);
            }

            {
              String returnString = descriptor.substring(descriptor.indexOf(")") + 1);
              has_return = returnString.equals("I");
            }

            register_count = 1 + parameter_count + (has_return ? 1 : 0);
            reg_inst.registers = new Register[register_count];
            reg_inst.registers[register_count - parameter_count - 1] = reg_1;
            for (int i = register_count - 1; i > register_count - parameter_count -1 ; i--) {
              reg_inst.registers[i] = block_reg_stack.pop();
            }
            if (has_return) {
              reg_2 = new Register(RegisterType.TEMPORAL);
              reg_inst.registers[0] = reg_2;
              block_reg_stack.push(reg_2);
            }
            current_block.reg_instructions.put(reg_inst.index, reg_inst);
            break;

          case INVOKESPECIAL:
          case INVOKEVIRTUAL:
            reg_inst = new RegisterInstruction(block_reg_inst_index++, RegisterInstructionType.DISPATCH);


            {
              RuntimeCPEntry cpEntry = dc.runtimeCP.getEntry(inst.op1);
              reg_1 = new Register(RegisterType.LITERAL, inst.op1, cpEntry.toString());
              descriptor = cpEntry.getDescriptorSubstring();
            }

            {
              String parameters = descriptor.substring(descriptor.indexOf("(") + 1, descriptor.indexOf(")"));
              parameter_count = getParameterCount(parameters);
            }

            // for (int i = 0; i < stack.size(); i++)
            //   if (stack.get(i).sameType(val1) && stack.get(i).id == val1.id)
            //     stack.set(i, new VerifierValue(VerifierType.OBJECT, val1.objType));

            {
              String returnString = descriptor.substring(descriptor.indexOf(")") + 1);
              has_return = returnString.equals("I");
            }

            register_count = 2 + parameter_count + (has_return ? 1 : 0);
            reg_inst.registers = new Register[register_count];
            reg_inst.registers[register_count - parameter_count - 2] = reg_1;
            for (int i = register_count - 1; i > register_count - parameter_count -1 ; i--) {
              reg_inst.registers[i] = block_reg_stack.pop();
            }
            reg_2 = block_reg_stack.pop();
            reg_inst.registers[register_count - parameter_count - 1] = reg_2;
            if (has_return) {
              reg_2 = new Register(RegisterType.TEMPORAL);
              reg_inst.registers[0] = reg_2;
              block_reg_stack.push(reg_2);
            }
            current_block.reg_instructions.put(reg_inst.index, reg_inst);
            break;

            //various arguments instructions
          case NEWARRAY:
            reg_inst = new RegisterInstruction(block_reg_inst_index++, RegisterInstructionType.NEWARRAY);
            reg_1 = new Register(RegisterType.TEMPORAL);
            reg_2 = new Register(RegisterType.CONSTANT, inst.op1);
            reg_3 = block_reg_stack.pop();
            reg_inst.registers = new Register[3];
            reg_inst.registers[0] = reg_1;
            reg_inst.registers[1] = reg_2;
            reg_inst.registers[2] = reg_3;
            block_reg_stack.push(reg_1);
            current_block.reg_instructions.put(reg_inst.index, reg_inst);
            break;

          case BIPUSH:
          case SIPUSH:
            reg_inst = new RegisterInstruction(block_reg_inst_index++, RegisterInstructionType.MOVE);
            reg_1 = new Register(RegisterType.TEMPORAL);
            reg_2 = new Register(RegisterType.CONSTANT, inst.op1);
            reg_inst.registers = new Register[2];
            reg_inst.registers[0] = reg_1;
            reg_inst.registers[1] = reg_2;
            block_reg_stack.push(reg_1);
            current_block.reg_instructions.put(reg_inst.index, reg_inst);
            break;

          case IINC:
            reg_inst = new RegisterInstruction(block_reg_inst_index++, RegisterInstructionType.IADD);
            reg_1 = new Register(RegisterType.TEMPORAL);
            reg_2 = current_virtual_registers[inst.op1];
            reg_3 = new Register(RegisterType.CONSTANT, inst.op2);
            reg_inst.registers = new Register[3];
            reg_inst.registers[0] = reg_1;
            reg_inst.registers[1] = reg_2;
            reg_inst.registers[2] = reg_3;
            current_block.reg_instructions.put(reg_inst.index, reg_inst);
            current_virtual_registers[inst.op1] = reg_1;
            break;

            // no argument instructions
          case NOP:
            reg_inst = new RegisterInstruction(block_reg_inst_index++, RegisterInstructionType.NOP);
            current_block.reg_instructions.put(reg_inst.index, reg_inst);
            break;
          case POP:
            block_reg_stack.pop();
            break;
          case POP2:
            block_reg_stack.pop();
            block_reg_stack.pop();
            break;
          case SWAP:
            reg_1 = block_reg_stack.pop();
            reg_2 = block_reg_stack.pop();
            block_reg_stack.push(reg_1);
            block_reg_stack.push(reg_2);
            break;
          case DUP:
            reg_1 = block_reg_stack.pop();
            block_reg_stack.push(reg_1);
            block_reg_stack.push(reg_1);
            break;
          case DUP_X1:
            reg_1 = block_reg_stack.pop();
            reg_2 = block_reg_stack.pop();
            block_reg_stack.push(reg_1);
            block_reg_stack.push(reg_2);
            block_reg_stack.push(reg_1);
            break;
          case DUP_X2:
            reg_1 = block_reg_stack.pop();
            reg_2 = block_reg_stack.pop();
            reg_3 = block_reg_stack.pop();
            block_reg_stack.push(reg_1);
            block_reg_stack.push(reg_3);
            block_reg_stack.push(reg_2);
            block_reg_stack.push(reg_1);
            break;
          case DUP2:
            reg_1 = block_reg_stack.pop();
            reg_2 = block_reg_stack.pop();
            block_reg_stack.push(reg_2);
            block_reg_stack.push(reg_1);
            block_reg_stack.push(reg_2);
            block_reg_stack.push(reg_1);
            break;
          case DUP2_X1:
            reg_1 = block_reg_stack.pop();
            reg_2 = block_reg_stack.pop();
            reg_3 = block_reg_stack.pop();
            block_reg_stack.push(reg_2);
            block_reg_stack.push(reg_1);
            block_reg_stack.push(reg_3);
            block_reg_stack.push(reg_2);
            block_reg_stack.push(reg_1);
            break;
          case DUP2_X2:
            reg_1 = block_reg_stack.pop();
            reg_2 = block_reg_stack.pop();
            reg_3 = block_reg_stack.pop();
            reg_4 = block_reg_stack.pop();
            block_reg_stack.push(reg_2);
            block_reg_stack.push(reg_1);
            block_reg_stack.push(reg_4);
            block_reg_stack.push(reg_3);
            block_reg_stack.push(reg_2);
            block_reg_stack.push(reg_1);
            break;
          case IADD:
          case ISUB:
          case IMUL:
          case IDIV:
          case IAND:
          case IOR:
          case IXOR:
          case IREM:
          case ISHL:
          case ISHR:
          case IUSHR:
            switch(inst.type) {
              case IADD:  reg_inst_type = RegisterInstructionType.IADD;  break;
              case ISUB:  reg_inst_type = RegisterInstructionType.ISUB;  break;
              case IMUL:  reg_inst_type = RegisterInstructionType.IMUL;  break;
              case IDIV:  reg_inst_type = RegisterInstructionType.IDIV;  break;
              case IAND:  reg_inst_type = RegisterInstructionType.IAND;  break;
              case IOR:   reg_inst_type = RegisterInstructionType.IOR;   break;
              case IXOR:  reg_inst_type = RegisterInstructionType.IXOR;  break;
              case IREM:  reg_inst_type = RegisterInstructionType.IREM;  break;
              case ISHL:  reg_inst_type = RegisterInstructionType.ISHL;  break;
              case ISHR:  reg_inst_type = RegisterInstructionType.ISHR;  break;
              case IUSHR: reg_inst_type = RegisterInstructionType.IUSHR; break;
              default: reg_inst_type = null;
            }
            reg_inst = new RegisterInstruction(block_reg_inst_index++, reg_inst_type);
            reg_1 = new Register(RegisterType.TEMPORAL);
            reg_3 = block_reg_stack.pop();
            reg_2 = block_reg_stack.pop();
            reg_inst.registers = new Register[3];
            reg_inst.registers[0] = reg_1;
            reg_inst.registers[1] = reg_2;
            reg_inst.registers[2] = reg_3;
            block_reg_stack.push(reg_1);
            current_block.reg_instructions.put(reg_inst.index, reg_inst);
            break;

          case INEG:
            reg_inst = new RegisterInstruction(block_reg_inst_index++, RegisterInstructionType.INEG);
            reg_1 = new Register(RegisterType.TEMPORAL);
            reg_2 = block_reg_stack.pop();
            reg_inst.registers = new Register[2];
            reg_inst.registers[0] = reg_1;
            reg_inst.registers[1] = reg_2;
            block_reg_stack.push(reg_1);
            current_block.reg_instructions.put(reg_inst.index, reg_inst);
            break;

          case ICONST_M1:
          case ICONST_0:
          case ICONST_1:
          case ICONST_2:
          case ICONST_3:
          case ICONST_4:
          case ICONST_5:
            reg_inst = new RegisterInstruction(block_reg_inst_index++, RegisterInstructionType.MOVE);
            reg_1 = new Register(RegisterType.TEMPORAL);
            const_value = inst.type.opcode - InstructionType.ICONST_0.opcode;
            reg_2 = new Register(RegisterType.CONSTANT, const_value);
            reg_inst.registers = new Register[2];
            reg_inst.registers[0] = reg_1;
            reg_inst.registers[1] = reg_2;
            block_reg_stack.push(reg_1);
            current_block.reg_instructions.put(reg_inst.index, reg_inst);
            break;

          case IALOAD:
            reg_inst = new RegisterInstruction(block_reg_inst_index++, RegisterInstructionType.GETELEMENT);
            reg_4 = block_reg_stack.pop(); // index
            reg_3 = block_reg_stack.pop(); // ref
            reg_2 = new Register(RegisterType.CONSTANT, 10); // int type
            reg_1 = new Register(RegisterType.TEMPORAL); // register
            reg_inst.registers = new Register[4];
            reg_inst.registers[0] = reg_1;
            reg_inst.registers[1] = reg_2;
            reg_inst.registers[2] = reg_3;
            reg_inst.registers[3] = reg_4;
            block_reg_stack.push(reg_1);
            current_block.reg_instructions.put(reg_inst.index, reg_inst);
            break;

          case IASTORE:
            reg_inst = new RegisterInstruction(block_reg_inst_index++, RegisterInstructionType.SETELEMENT);
            reg_4 = block_reg_stack.pop(); // value
            reg_3 = block_reg_stack.pop(); // index
            reg_2 = new Register(RegisterType.CONSTANT, 10); // int type
            reg_1 = block_reg_stack.pop(); // ref
            reg_inst.registers = new Register[4];
            reg_inst.registers[0] = reg_1;
            reg_inst.registers[1] = reg_2;
            reg_inst.registers[2] = reg_3;
            reg_inst.registers[3] = reg_4;
            current_block.reg_instructions.put(reg_inst.index, reg_inst);
            break;

          case ARRAYLENGTH:
            reg_inst = new RegisterInstruction(block_reg_inst_index++, RegisterInstructionType.LENGTH);
            reg_1 = new Register(RegisterType.TEMPORAL);
            reg_2 = block_reg_stack.pop();
            reg_inst.registers = new Register[2];
            reg_inst.registers[0] = reg_1;
            reg_inst.registers[1] = reg_2;
            block_reg_stack.push(reg_1);
            current_block.reg_instructions.put(reg_inst.index, reg_inst);
            break;

          case RETURN:
            // block_reg_stack = new Stack<>();
            reg_inst = new RegisterInstruction(block_reg_inst_index++, RegisterInstructionType.RETURN);
            reg_inst.registers = new Register[0];
            current_block.reg_instructions.put(reg_inst.index, reg_inst);
            break;

          case IRETURN:
            reg_inst = new RegisterInstruction(block_reg_inst_index++, RegisterInstructionType.IRETURN);
            reg_1 = block_reg_stack.pop();
            reg_inst.registers = new Register[1];
            reg_inst.registers[0] = reg_1;
            current_block.reg_instructions.put(reg_inst.index, reg_inst);
            break;
        } // }}}
      }

      // set register stack for successors;
      current_block.end_register_stack = block_reg_stack;
      current_block.end_virtual_registers = current_virtual_registers;

      if (current_block.no_jump.isPresent()) {
        if (current_block.no_jump.get().reg_instructions.size() == 0)
          todo_blocks.add(current_block.no_jump.get());
      }
      if (current_block.jump.isPresent()) {
        if (current_block.jump.get().reg_instructions.size() == 0)
          todo_blocks.add(current_block.jump.get());
      }
    }

    // parse standard phi inst -------------------------------------------------
    for (BasicBlock block : basic_blocks.values()) {
      for (RegisterInstruction reg_inst : block.reg_instructions.values()) {
        if (reg_inst.type == RegisterInstructionType.PHI) {
          if (reg_inst.registers[1].type != RegisterType.CONSTANT)
            continue; // parsed phi inst
          Register dest = reg_inst.registers[0];
          int index = reg_inst.registers[1].value;

          NavigableMap<Integer, Register> pre_stack_registers = new TreeMap<Integer, Register>();
          for (BasicBlock pre_block : block.predecessors.values()) {
            Register pre_stack_register = pre_block.end_register_stack.get(index);
            if (pre_stack_register.value == dest.value)
              continue;
            pre_stack_registers.put(pre_stack_register.value, pre_stack_register);
          }
          if (pre_stack_registers.size() > 1) {
            reg_inst.registers = new Register[1 + pre_stack_registers.size()];
            reg_inst.registers[0] = dest;
            int j = 1;
            for (Register pre_virt_register : pre_stack_registers.values()) {
              reg_inst.registers[j++] = pre_virt_register;
            }
          } else {
            // convert to move; will get optimized later
            reg_inst.type = RegisterInstructionType.MOVE;
            reg_inst.registers[1] = pre_stack_registers.firstEntry().getValue();
          }
        }
      }
    }

    // add phi inst for virtual register ---------------------------------------
    for (BasicBlock block : basic_blocks.values()) {
      for (int i=0; i < block.end_virtual_registers.length; i++) {
        NavigableMap<Integer, Register> pre_virt_registers = new TreeMap<Integer, Register>();
        for (BasicBlock pre_block : block.predecessors.values()) {
          Register pre_virt_register = pre_block.end_virtual_registers[i];
          pre_virt_registers.put(pre_virt_register.value, pre_virt_register);
        }
        if (pre_virt_registers.size() > 1) {
          RegisterInstruction phi_inst = new RegisterInstruction(
              block.reg_instructions.firstKey() - 1,
              RegisterInstructionType.PHI
              );
          phi_inst.registers = new Register[1 + pre_virt_registers.size()];
          phi_inst.registers[0] = new Register(RegisterType.TEMPORAL);
          int j = 1;
          for (Register pre_virt_register : pre_virt_registers.values()) {
            phi_inst.registers[j++] = pre_virt_register;
          }

          Register old_virt_register = block.end_virtual_registers[i];
          Stack<BasicBlock> todo = new Stack<>();
          Stack<BasicBlock> done = new Stack<>();
          todo.push(block);
          while (!todo.empty()) {
            BasicBlock t_block = todo.pop();
            for (RegisterInstruction reg_inst : t_block.reg_instructions.values()) {
              for (int k = reg_inst.sets_register() ? 1 : 0; k < reg_inst.registers.length; k++) {
                if (reg_inst.registers[k].type == RegisterType.TEMPORAL
                    && pre_virt_registers.containsKey(reg_inst.registers[k].value)) {
                  reg_inst.registers[k] = phi_inst.registers[0];
                }
              }
            }
            // For debugging:
            // for (int k = 0; k < t_block.end_register_stack.size(); k++) {
            //   if (t_block.end_register_stack.get(k).type == RegisterType.TEMPORAL
            //       && pre_virt_registers.containsKey(t_block.end_register_stack.get(k).value)) {
            //     t_block.end_register_stack.set(k, phi_inst.registers[0]);
            //   }
            // }
            done.push(t_block);
            if (t_block.end_virtual_registers[i] == old_virt_register) {
              t_block.end_virtual_registers[i] = phi_inst.registers[0];
              if (t_block.no_jump.isPresent() && done.search(t_block.no_jump.get()) == -1)
                todo.push(t_block.no_jump.get());
              if (t_block.jump.isPresent() && done.search(t_block.jump.get()) == -1)
                todo.push(t_block.jump.get());
            }
          }
          block.reg_instructions.put(phi_inst.index, phi_inst);
        }
      }
    }

    try {
      generateDotRegGraph();
    } catch (IOException e) {
      System.out.println("Could not create graph files.");
    }
  }

  public void optimise () {

    copy_propagation();
    constant_folding();

    // print optimized graph
    try {
      generateDotOptRegGraph();
    } catch (IOException e) {
      System.out.println("Could not create graph files.");
    }
  }

  void copy_propagation() {
    NavigableMap<Integer, Register> register_mapping = new TreeMap<>();
    for(BasicBlock block : basic_blocks.values()) {
      Stack<RegisterInstruction> moves_to_delte = new Stack<>();
      for (RegisterInstruction inst : block.reg_instructions.values()) {
        if (inst.type == RegisterInstructionType.MOVE && inst.registers[0].type == RegisterType.TEMPORAL) {
          register_mapping.put(inst.registers[0].value, inst.registers[1]);
          moves_to_delte.push(inst);
        }
      }
      for (RegisterInstruction move_inst : moves_to_delte) {
        block.reg_instructions.remove(move_inst.index);
      }
    }

    for(BasicBlock block : basic_blocks.values()) {
      for (RegisterInstruction inst : block.reg_instructions.values()) {
        for (int i = 0; i < inst.registers.length; i++) {
          Register orig_register = inst.registers[i];
          while (orig_register.type == RegisterType.TEMPORAL
              && register_mapping.containsKey(orig_register.value)) {
            orig_register = register_mapping.get(orig_register.value);
          }
          inst.registers[i] = orig_register;
        }
      }
    }
  }

  void constant_folding() {

    // propagate const values
    Boolean changened = true;
    while (changened) {
      changened = false;
      for(BasicBlock block : basic_blocks.values()) {
        if (_block_constant_folding(block)) {
          changened = true;
        }
      }
    }

    // upcast temp register with constant values
    for(BasicBlock block : basic_blocks.values()) {
      for (RegisterInstruction inst : block.reg_instructions.values()) {
        for (Register reg : inst.registers) {
          if (reg.type == RegisterType.TEMPORAL && reg.constValue != null) {
            reg.type = RegisterType.CONSTANT;
            reg.value = reg.constValue;
          }
        }
      }
    }

    // remove instruction that store to contant values
    for(BasicBlock block : basic_blocks.values()) {
      Stack<RegisterInstruction> inst_to_delete = new Stack<>();
      for (RegisterInstruction inst : block.reg_instructions.values()) {
        switch(inst.type) { // {{{
          // jump-instructions
          case MOVE: case PHI:
          case IADD: case ISUB: case IMUL: case IDIV: case IAND: case IOR:
          case IXOR: case IREM: case ISHL: case ISHR: case IUSHR:
          case INEG:
            if (inst.registers[0].type == RegisterType.CONSTANT)
              inst_to_delete.push(inst);
          default:
        } // }}}
      }
      for (RegisterInstruction inst : inst_to_delete) {
        block.reg_instructions.remove(inst.index);
      }
    }
  }

  Boolean _block_constant_folding (BasicBlock block) {
    Boolean changened = false;

    for (RegisterInstruction inst : block.reg_instructions.values()) {

      Boolean has_only_const_values = true;
      for(int i = 1; i < inst.registers.length; i++) {
        if (inst.registers[i].constValue == null)
          has_only_const_values = false;
      }
      if (inst.registers.length < 2
          || inst.registers[0].constValue != null
          || !has_only_const_values)
        continue;

      switch(inst.type) { // {{{
        // jump-instructions
        case MOVE:
          if (inst.registers[0].type != RegisterType.TEMPORAL)
            break;

          inst.registers[0].constValue = inst.registers[1].constValue;
          changened = true;
          break;

        case PHI:
          {
            Boolean has_same_value = true;
            int value = inst.registers[1].constValue;
            for(int i = 2; i < inst.registers.length; i++) {
              if (inst.registers[i].constValue != value)
                has_same_value = false;
            }

            if (has_same_value) {
              inst.registers[0].constValue = value;
              changened = true;
            }
          }
          break;

        case IADD:
        case ISUB:
        case IMUL:
        case IDIV:
        case IAND:
        case IOR:
        case IXOR:
        case IREM:
        case ISHL:
        case ISHR:
        case IUSHR:

          switch(inst.type) {
            case IADD:  inst.registers[0].constValue = inst.registers[1].constValue +   inst.registers[2].constValue; break;
            case ISUB:  inst.registers[0].constValue = inst.registers[1].constValue -   inst.registers[2].constValue; break;
            case IMUL:  inst.registers[0].constValue = inst.registers[1].constValue *   inst.registers[2].constValue; break;
            case IDIV:  inst.registers[0].constValue = inst.registers[1].constValue /   inst.registers[2].constValue; break;
            case IAND:  inst.registers[0].constValue = inst.registers[1].constValue &   inst.registers[2].constValue; break;
            case IOR:   inst.registers[0].constValue = inst.registers[1].constValue |   inst.registers[2].constValue; break;
            case IXOR:  inst.registers[0].constValue = inst.registers[1].constValue ^   inst.registers[2].constValue; break;
            case IREM:  inst.registers[0].constValue = inst.registers[1].constValue %   inst.registers[2].constValue; break;
            case ISHL:  inst.registers[0].constValue = inst.registers[1].constValue <<  inst.registers[2].constValue; break;
            case ISHR:  inst.registers[0].constValue = inst.registers[1].constValue >>  inst.registers[2].constValue; break;
            case IUSHR: inst.registers[0].constValue = inst.registers[1].constValue >>> inst.registers[2].constValue; break;
          }
          changened = true;
          break;

        case INEG:

          inst.registers[0].constValue = -1 * inst.registers[1].constValue;
          changened = true;
          break;

        default:
      } // }}}
    }

    return changened;
  }

  void splitBasicBlocks () {

    TreeSet<Integer> basic_block_indices = new TreeSet<Integer>();
    basic_block_indices.add(0);
    Integer next_index;
    for(VerifierInstruction inst : orig_instructions.values()) {
      switch(inst.type) {
        case IFEQ:
        case IFNE:
        case IFGT:
        case IFGE:
        case IFLT:
        case IFLE:
        case IF_ICMPEQ:
        case IF_ICMPNE:
        case IF_ICMPGT:
        case IF_ICMPGE:
        case IF_ICMPLT:
        case IF_ICMPLE:
        case IF_ACMPEQ:
        case IF_ACMPNE:
        case IFNONNULL:
        case IFNULL:
        case GOTO:
        case GOTO_W:
          next_index = orig_instructions.higherKey(inst.index);
          if (next_index != null) {
            basic_block_indices.add(next_index);
          }
          int jump_index = inst.index + inst.op1;
          basic_block_indices.add(jump_index);
          break;
        case RETURN:
        case IRETURN:
          next_index = orig_instructions.higherKey(inst.index);
          if (next_index != null) {
            basic_block_indices.add(next_index);
          }
          break;
        default:
      }
    }

    // create basic blocks
    basic_blocks = new TreeMap<Integer, BasicBlock>();

    VerifierInstruction g_last_inst = orig_instructions.lastEntry().getValue();
    int g_end_index = g_last_inst.index + (g_last_inst.isWide ? 2 : 1)
                + g_last_inst.byteOperands.length;
    for (int start_index : basic_block_indices) {
      Integer end_index = basic_block_indices.higher(start_index);
      if (end_index == null) {
        end_index = g_end_index;
      }
      BasicBlock new_block = new BasicBlock(start_index, end_index, orig_instructions.subMap(start_index, true, end_index, false));
      basic_blocks.put(start_index, new_block);
    }

    // create edges of control flow graph
    BasicBlock other_basic_block;
    for (BasicBlock basic_block : basic_blocks.values()) {
      VerifierInstruction last_inst = orig_instructions.lowerEntry(basic_block.end_index).getValue();
      switch(last_inst.type) {
        case IFEQ:
        case IFNE:
        case IFGT:
        case IFGE:
        case IFLT:
        case IFLE:
        case IF_ICMPEQ:
        case IF_ICMPNE:
        case IF_ICMPGT:
        case IF_ICMPGE:
        case IF_ICMPLT:
        case IF_ICMPLE:
        case IF_ACMPEQ:
        case IF_ACMPNE:
        case IFNONNULL:
        case IFNULL:
          other_basic_block = basic_blocks.get(basic_block.end_index);
          basic_block.no_jump = Optional.of(other_basic_block);
          other_basic_block.predecessors.put(basic_block.start_index, basic_block);
        case GOTO:
        case GOTO_W:
          int jump_index = last_inst.index + last_inst.op1;
          other_basic_block = basic_blocks.get(jump_index);
          basic_block.jump = Optional.of(other_basic_block);
          other_basic_block.predecessors.put(basic_block.start_index, basic_block);
          break;
        case RETURN:
        case IRETURN:
          break;
        default:
          other_basic_block = basic_blocks.get(basic_block.end_index);
          basic_block.no_jump = Optional.of(other_basic_block);
          other_basic_block.predecessors.put(basic_block.start_index, basic_block);
          break;
      }
    }

  }

  public void generateDotStackGraph () throws IOException {

    new File("./stack_graphs").mkdirs();
    String path = "./stack_graphs/" + method.identifier + ".gv";
    File file = new File(path);
    if (!file.exists()) {
      file.createNewFile();
    } else {
      file.delete();
      file.createNewFile();
    }

    PrintWriter writer = new PrintWriter(path);
    writer.println("digraph \"" + method.identifier + "\" {");
    writer.println("label = \"" + method.identifier.replace("<init>", "&lt;init&gt;")  + "\";");
    writer.println("labelloc = \"t\";");

    for (BasicBlock block : basic_blocks.values()) {
      writer.println(block.start_index + " [shape=rectangle, label=<" + block.toString().replaceAll("\n", "<BR/>").replace("<init>", "&lt;init&gt;") + ">];");
      if (block.no_jump.isPresent()) {
        writer.println(block.start_index + " -> " + block.no_jump.get().start_index + " [label=\"no jump\"];");
      }
      if (block.jump.isPresent()) {
        writer.println(block.start_index + " -> " + block.jump.get().start_index + " [label=\"jump\"];");
      }
    }

    writer.println("}");

    writer.close();
  }

  public void generateDotRegGraph () throws IOException {

    new File("./reg_graphs").mkdirs();
    String path = "./reg_graphs/" + method.identifier + ".gv";
    File file = new File(path);
    if (!file.exists()) {
      file.createNewFile();
    } else {
      file.delete();
      file.createNewFile();
    }

    PrintWriter writer = new PrintWriter(path);
    writer.println("digraph \"" + method.identifier + "\" {");
    writer.println("label = \"" + method.identifier.replace("<init>", "&lt;init&gt;")  + "\";");
    writer.println("labelloc = \"t\";");

    for (BasicBlock block : basic_blocks.values()) {
      writer.println(block.start_index + " [shape=rectangle, label=<" + block.toStringRegister().replaceAll("\n", "<BR/>").replace("<init>", "&lt;init&gt;") + ">];");
      if (block.no_jump.isPresent()) {
        writer.println(block.start_index + " -> " + block.no_jump.get().start_index + " [label=\"no jump\"];");
      }
      if (block.jump.isPresent()) {
        writer.println(block.start_index + " -> " + block.jump.get().start_index + " [label=\"jump\"];");
      }
    }

    writer.println("}");

    writer.close();
  }

  public void generateDotOptRegGraph () throws IOException {

    new File("./opt_reg_graphs").mkdirs();
    String path = "./opt_reg_graphs/" + method.identifier + ".gv";
    File file = new File(path);
    if (!file.exists()) {
      file.createNewFile();
    } else {
      file.delete();
      file.createNewFile();
    }

    PrintWriter writer = new PrintWriter(path);
    writer.println("digraph \"" + method.identifier + "\" {");
    writer.println("label = \"" + method.identifier.replace("<init>", "&lt;init&gt;")  + "\";");
    writer.println("labelloc = \"t\";");

    for (BasicBlock block : basic_blocks.values()) {
      writer.println(block.start_index + " [shape=rectangle, label=<" + block.toStringRegister().replaceAll("\n", "<BR/>").replace("<init>", "&lt;init&gt;") + ">];");
      if (block.no_jump.isPresent()) {
        writer.println(block.start_index + " -> " + block.no_jump.get().start_index + " [label=\"no jump\"];");
      }
      if (block.jump.isPresent()) {
        writer.println(block.start_index + " -> " + block.jump.get().start_index + " [label=\"jump\"];");
      }
    }

    writer.println("}");

    writer.close();
  }

  /**
   * Parse a descriptor and return count of parameters. This can be used for a method or
   * field descriptor. This must be only be consecutive parts of {@code I}, {@code [I} and {@code LClassName;}.
   * Other parts must not be contained in the String, because no checking of the descriptor is done.
   *
   * @param descriptor the descriptor to be parsed
   * @return parameter count, it is 0, if the descriptor is empty or not valid
   */
  private int getParameterCount(String descriptor) {
    //derive parameters from method descriptor
    String paraPattern = TextChecker.INT_DESC + "|" + TextChecker.ARRAY_DESC + "|" + TextChecker.CLASS_DESC;
    Matcher matcher = Pattern.compile(paraPattern).matcher(descriptor);
    int count = 0;
    while (matcher.find()) { count++; }
    return count;
  }
}

