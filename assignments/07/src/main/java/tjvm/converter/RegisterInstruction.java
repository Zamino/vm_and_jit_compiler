package tjvm.converter;

import tjvm.execution.Instruction;
import tjvm.loader.DerivedClass;
import tjvm.loader.Method;

import java.util.NavigableMap;
import java.util.Stack;

enum RegisterInstructionType {
  /** PHI operation */
  PHI,

  /** No operation. */
  NOP,
  //  ACONST_NULL,  //no null reference in TinyJava

  /** MOVE copy value to registers */
  MOVE,

  /** SET ELEMENT of array object on index to register value */
  SETELEMENT,
  /** GET ELEMENT of array object on index to register */
  GETELEMENT,
  /** LENTH of array of array object */
  LENGTH,

  /** Add two integer registers. */
  IADD,
  //  LADD, FADD, DADD,

  /** Subtract one integer register from another. */
  ISUB,
  //  LSUB, FSUB, DSUB,

  /** Multiply two integer registers. */
  IMUL,
  //  LMUL, FMUL, DMUL,

  /** Divide one integer register by another. */
  IDIV,
  //  LDIV, FDIV, DDIV,

  /** Integer remainder. */
  IREM,
  //  LREM, FREM, DREM,

  /** Negate integer register. */
  INEG,
  //  LNEG, FNEG, DNEG,

  /** Integer left shift of value1 by positions defined in low 5 bits of value2. */
  ISHL,
  //  LSHL,

  /** Integer right shift of value1 by positions defined in low 5 bits of value2 (arithmetic, keeping the sign bit and distinguishing b/w mantissa and exponent). */
  ISHR,
  //  LSHR,

  /** Integer right shift of value1 by positions defined in low 5 bits of value2 (logical, regardless of sign bit/mantissa/exponent). */
  IUSHR,
  //  LUSHR,

  /** Bitwise AND on two integer registers. */
  IAND,
  //  LAND,

  /** Bitwise OR on two integer registers. */
  IOR,
  //  LOR,

  /** Bitwise XOR on two integer registers. */
  IXOR,
  //  LXOR,

  /** Increment integer in loc var table at index in next byte by constant in byte after index byte. */
  // IINC(132, 2), // MOVE + ADD + MOVE

  //  I2L, I2F, I2D,  //conversions of integers
  //  L2I, L2F, L2D,  //conversions of long values
  //  F2I, F2L, F2D,  //converisons of float values
  //  D2I, D2L, D2F,  //conversions of double values
  //  I2B, I2C, I2S,  //more conversions of integers
  //  LCMP,    //comparing of two long values
  //  FCMPL, FCMPG,  //comparing of two float values
  //  DCMPL, DCMPG,  //comparing of two double values

  /** If register is 0, jump to instruction indicated by constant. */
  IFEQ,
  /** If register is not 0, jump to instruction indicated by constant. */
  IFNE,
  /** If register is less than 0, jump to instruction indicated by constant. */
  IFLT,
  /** If register is greater or equal to 0, jump to instruction indicated by constant. */
  IFGE,
  /** If register is greater than 0, jump to instruction indicated by constant. */
  IFGT,
  /** If register is less or equal to 0, jump to instruction indicated by constant. */
  IFLE,

  /** If the given two register are equal, jump to instruction indicated by constant. */
  IF_ICMPEQ,
  /** If the given two register are not equal, jump to instruction indicated by constant. */
  IF_ICMPNE,
  /** If first register is less than second register, jump to instruction indicated by constant. */
  IF_ICMPLT,
  /** If first register is greater or equal to second register, jump to instruction indicated by constant. */
  IF_ICMPGE,
  /** If first register is greater than second register, jump to instruction indicated by constant. */
  IF_ICMPGT,
  /** If first register is less or equal to second register, jump to instruction indicated by constant. */
  IF_ICMPLE,

  // IF_ACMPEQ, // IF_ICMPEQ
  // IF_ACMPNE, // IF_ICMPNE

  /** Go to instruction indicated by constant*/
  GOTO,

  //  JSR, RET,      mainly used for finally clauses
  //  TABLESWITCH, LOOKUPSWITCH,  used for switch statement, not available in TinyJava

  /** Return register, an {@code int} value from method to the invoker. */
  IRETURN,
  //  LRETURN, FRETURN, DRETURN, ARETURN  //only integer or void allowed as return types
  /** Return {@code void} from method. */
  RETURN,

  /** Push static field value into register, constant pool index. */
  // GETSTATIC, MOVE OF LITERAL TO REGISTER
  /** Store register value into static field at constant pool index. */
  // PUTSTATIC, MOVE OF REGISTER TO LITERAL
  /** Push field value in cp indicated by literal register into register. */
  GETFIELD,
  /** Store register value into cp field indicated by literal register. */
  PUTFIELD,

  /** Invoke virtual method at cp index specified by next two bytes. */
  DISPATCH,    // INVOKEVIRTUA,
  // DISPATCH, // INVOKESPECIAL
  /** Invoke static method at cp index specified by next two bytes. */
  CALL,        // INVOKESTATIC,

  //  INVOKEINTERFACE,  //no interfaces
  //  INVOKEDYNAMIC,    //no dynamic methods

  /** Create new object of a class at constant pool index specified by literal register. */
  NEW,
  /** Create new array of primitive type specified by by constant. */
  NEWARRAY,

  //  ANEWARRAY,    //no arrays of objects in TinyJava

  /** Push array length into register. */
  ARRAYLENGTH,

  //  ATHROW,      //no exceptions in TinyJava
  //  CHECKCAST,    //no casts in TinyJava

  //  INSTANCEOF,      //no class inheritance in TinyJava
  //  MONITORENTER, MONITOREXIT,  //no threads in TinyJava

  //  MULTIANEWARRAY,    //only one-dimensional arrays in TinyJava

  // /** Double the operand length of the instruction right after this instruction. */
  // WIDE(196, -1),

  /** If null reference in register, jump to instruction indicated by constant. */
  IFNULL,
  /** If no null reference in register, jump to instruction indicated by constant. */
  IFNONNULL,

  /** Go to instruction specified by constant. */
  GOTO_W;

  //  JSR_W,    //no subroutines (other than methods) in TinyJava
}

public class RegisterInstruction {

  int index;
  RegisterInstructionType type;
  Register[] registers;

  /**
   * Create a new regisert instruction
   *
   * @param index        the index of the beginning of the instruction in the bytecode array
   * @param type         the type of the instruction
   */

  RegisterInstruction(int index, RegisterInstructionType type) {
    this.index = index;
    this.type  = type;
  }

  public Boolean sets_register() {
    switch(this.type) {
      case PHI: case MOVE: case GETELEMENT: case LENGTH: case IADD: case ISUB:
      case IMUL: case IDIV: case IREM: case INEG: case ISHL: case ISHR:
      case IUSHR: case IAND: case IOR: case IXOR: case NEW: case NEWARRAY:
      case ARRAYLENGTH: case GETFIELD:
        return true;
      case DISPATCH: case CALL:
        if (this.registers[0].type == RegisterType.TEMPORAL)
          return true;
      default: return false;
    }
  }

  /**
   * Returns the string representation of the instruction.
   *
   * @return The string representation of the instruction
   */
  public String toString() {
    StringBuilder res = new StringBuilder();
    res.append(index).append(" : ").append(type.name()).append(" ");
    if (registers != null) {
      for (Register register : registers) {
        res.append(register).append(" ");
      }
    }
    return res.toString();
  }
}
