package tjvm.verifier;

/**
 * Enum that represents the type of a value on the {@link VerifierInstruction#verifierStack} or in the
 * {@link VerifierInstruction#verifierLocals} which is used during the data-flow analysis of the {@link DataFlowAnalyzer}
 * during Bytecode verification of the {@link BytecodeVerifier}.
 */
enum VerifierType {
    /**
     * Primitive type {@code int}.
     */
    INT(false),
    /**
     * Array type {@code int[]}.
     */
    INTARR(true),
    /**
     * Reference type, that is already initialized.
     */
    OBJECT(true),
    /**
     * Reference type, that is not initialized.
     */
    UNINIT(true),
    /**
     * Illegal type. Used for local variables that are not initialized at the beginning of data-flow analysis.
     */
    ILLEGAL(false),
    /**
     * Unusable type. Used if two different primitive types in the local variable array are merged during data-flow analysis.
     */
    UNUSABLE(false);

    /**
     * {@code true} if the value represents a reference to an (uninitialized) object or array.
     */
    private final boolean isReference;

    /**
     * Define a new type.
     *
     * @param isReference {@code true} if the value represents a reference to an (uninitialized) object or array
     */
    VerifierType(boolean isReference) {
        this.isReference = isReference;
    }

    /**
     * Get, if the value represents a reference to an (uninitialized) object or array
     *
     * @return {@code true} if the value represents a reference to an (uninitialized) object or array
     */
    boolean isReference() {
        return isReference;
    }
}

