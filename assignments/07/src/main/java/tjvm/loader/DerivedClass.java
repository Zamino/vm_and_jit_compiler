package tjvm.loader;

import tjvm.data.RuntimeCP;

/**
 * Class that represents all information about a {@code class}-file after loading. It contains
 * the following components:
 * <ul>
 * <li>The {@link ConstantPool} that holds the symbolic information for the virtual machine instructions.</li>
 * <li>The {@link FieldList} that holds information about all fields of the class.</li>
 * <li>The {@link MethodList} that holds information about all methods of the class.</li>
 * <li>Further information for example {@code access_flags}.</li>
 * </ul>
 * This class is used by the {@link ClassLoader} to write all information gathered while parsing and checking
 * the {@code class} file. The derived class is then used by the {@link tjvm.data.MethodArea} to create the
 * class in the method area, as well as by the {@link tjvm.verifier.BytecodeVerifier} to verify the code during linking, because there
 * are more information needed than are held in the method area.
 */
public class DerivedClass {
    /** The name of the class representation. */
    public String className;

    /** The major version of the {@code class} file the class is derived from. */
    int majorVersion;
    /** The minor version of the {@code class} file the class is derived from. */
    int minorVersion;

    /** The constant pool table of the class. */
    public ConstantPool constantPool;

    /** The runtime constant pool of the class. */
    public RuntimeCP runtimeCP;

    /** Representation of the {@code access_flags}. */
    short accessFlags;
    /** Index in the constant pool to the name of the super class. In TinyJava this is {@code java/lang/Object}.*/
    int superIndex;
    /** Index in the constant pool to the name of this class. */
    int thisIndex;

    /** The number of interfaces the class implements. In TinyJava this is {@code 0}. */
    int interfaceCount;
    /** All methods of the class. */
    public MethodList methodList;
    /** All fields of the class. */
    public FieldList fieldList;

    /** The name of the source file, from which the {@code class} file was compiled. */
    String sourceFileName;

    /**
     * Create a new instance of a derived class. The internal structures, the {@link ConstantPool}, the {@link FieldList}
     * and the {@link MethodList} are set up empty as well.
     *
     * @param className the name of the class
     */
    DerivedClass(String className) {
        this.className = className;
        constantPool = new ConstantPool();
        methodList = new MethodList();
        fieldList = new FieldList();
    }

    /**
     * Returns the string representation of the derived class. The representation looks similar to
     * <blockquote>{@code javap} - The Java Class File Disassembler</blockquote>
     * using the {@code -verbose} option.
     *
     * @return The string representation of the derived class
     */
    @Override
    public String toString() {
        String indent = "\t";
        String comment = "\t\t\t // ";
        String thisClass = constantPool.getEntryAs(constantPool.getEntryAs(thisIndex, CPClassEntry.class).classNameIndex, CPUTF8Entry.class).utf8String;
        String superClass = constantPool.getEntryAs(constantPool.getEntryAs(superIndex, CPClassEntry.class).classNameIndex, CPUTF8Entry.class).utf8String;
        return "class " + className + "\n" +
                indent + "minor version: " + minorVersion + "\n" +
                indent + "major version: " + majorVersion + "\n" +
                indent + "flags: " + Integer.toBinaryString(accessFlags) + "\n" +
                indent + "this_class: #" + thisIndex + "\t" + comment + thisClass + "\n" +
                indent + "super_class: #" + superIndex + comment + superClass + "\n" +
                indent + "interfaces: " + interfaceCount + ", fields: " + fieldList.size() + ", methods: " + methodList.size() + "\n" +
                constantPool.toString() + "\n" +
                "{\n" +
                fieldList.toString() +
                methodList.toString() +
                "}\n" +
                "Source File: \"" + sourceFileName + "\"";
    }

}
