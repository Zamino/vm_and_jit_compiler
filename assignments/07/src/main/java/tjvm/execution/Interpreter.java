package tjvm.execution;

import tjvm.data.Heap;
import tjvm.data.MethodArea;
import tjvm.data.RuntimeCPEntry;
import tjvm.loader.CPEntry;
import tjvm.loader.DerivedClass;
import tjvm.loader.TextChecker;
import tjvm.verifier.InstructionType;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Class that uses the {@link Heap} and {@link MethodArea} to run uncompiled Bytecode
 * from {@code class} files. The information which code to run and information about
 * threads are always given the interpreter as an argument of type {@link Thread}.<br>
 * Interpreting code is only possible, if the corresponding class is already loaded
 * and created as well as linked.<br>
 * To interpret the Bytecode the Interpreter first translates the next {@code byte}
 * into an {@link Instruction}. Afterwards, if the instruction has arguments within
 * the {@code byte[]} code array, these arguments, called operands, are read and, if
 * necessary, translated. Afterwards, the effect of the instruction is applied to
 * the data structures. The basic principle and algorithm of the interpreter is are
 * described in the Java Virtual Machine Specification in chapter &sect;2.11 as follows:
 * <blockquote>
 * Java Virtual Machine instruction consists of a one-byte opcode specifying
 * the operation to be performed, followed by zero or more operands supplying
 * arguments or data that are used by the operation. Many instructions have no
 * operands and consist only of an opcode.
 * Ignoring exceptions, the inner loop of a Java Virtual Machine interpreter is
 * effectively <br>
 * <code>
 * do&#123; <br>
 * &#160;&#160;&#160;atomically calculate pc and fetch opcode at pc; <br>
 * &#160;&#160;&#160;if (operands) fetch operands;<br>
 * &#160;&#160;&#160;execute the action for the opcode;<br>
 * &#125; while (there is more to do);</code><br>
 * </blockquote>
 */
class Interpreter {
    /**
     * Reference to run-time data area that stores objects and arrays.
     */
    private Heap heap;
    /**
     * Reference to run-time data area that stores per-class information like bytecode or static fields.
     */
    private MethodArea methodArea;
    /**
     * Reference to virtual machine to invoke methods as well as load, create and link classes.
     */
    private TinyJVM jvm;

    /**
     * Initialize the interpreter.
     *
     * @param heap       the heap for the program
     * @param methodArea the method area for the program
     * @param jvm        the virtual machine to invoke methods as well as load, create and link classes
     */
    Interpreter(Heap heap, MethodArea methodArea, TinyJVM jvm) {
        this.heap = heap;
        this.methodArea = methodArea;
        this.jvm = jvm;
    }

    /**
     * Prepare a method by initializing a {@link Frame} und pushing it onto the {@link FrameStack}.
     * This is done, when the interpreter is supposed to run the method. This consists of the
     * following steps
     * <ol>
     * <li>Look up the {@code max_stack}, {@code max_locals} and the reference to the runtime constant pool
     * from the {@link MethodArea}.</li>
     * <li>Initialize a new {@link Frame} und put the arguments of the method in the local variables.</li>
     * <li>Save the current program counter in the current frame.</li>
     * <li>Push the new frame on top of the frame stack.</li>
     * <li>Set program counter of the {@link Thread} to the beginning of the bytecode of the new frame.</li>
     * </ol>
     * Preparing a method does not automatically interpret the code. This is done with {@link #executeStep(Thread)}.
     *
     * @param codeOffset       the offset of the code in the {@code Bytecode Table} of the {@link MethodArea}
     * @param thread           the thread, where the method was invoked
     * @param args             the arguments for the method
     * @param cdOffset         the offset to the class descriptor of the class of the invoked method
     * @param methodIdentifier the identifier of the method for debugging
     */
    void prepareMethod(int codeOffset, Thread thread, int[] args, int cdOffset, String methodIdentifier) {
        //1. Get information for method
        int maxLocals = methodArea.getMaxLocals(codeOffset);
        int maxStack = methodArea.getMaxStack(codeOffset);
        int rcpRef = methodArea.getRCPIndex(cdOffset);

        //2. Initialize frame and put arguments in local variables
        Frame frame = new Frame(maxStack, maxLocals, rcpRef, codeOffset, methodIdentifier);
        System.arraycopy(args, 0, frame.locals, 0, args.length);

        //3. save pc to current frame, push new frame and set program counter register to first instruction
        if (thread.framesLeft()) {
            thread.currentFrame().savePc = thread.pc;
        }
        thread.frameStack.push(frame);
        thread.pc = thread.currentFrame().savePc;
    }


    /**
     * Execute one single instruction within a thread. This consists of two steps:
     * <ol>
     * <li>Parsing the instruction from the {@code byte} code array into an {@link Instruction}
     * with {@link #getInst(Thread)}</li>
     * <li>Applying the effect of that instruction to all data areas involved ({@link Heap}, {@link MethodArea}
     * and {@link Thread}) with {@link #executeInst(Thread, Instruction)}.</li>
     * </ol>
     *
     * @param thread the thread for which the next instruction is to be run
     */
    void executeStep(Thread thread) {
        //1. parse instruction
        Instruction inst = getInst(thread);

        //2. apply instruction
        executeInst(thread, inst);
    }

    /**
     * Get the next instruction for the thread including the operands. This uses
     * the {@link Thread#pc} of the thread to determine the position in the {@code Bytecode Table}
     * in the {@link MethodArea}. The program counter of the thread is set to the position
     * right after the parsed instruction, which makes a change only necessary, if there
     * is a jump.
     *
     * @param thread the thread to parse next instruction for
     * @return the parsed instruction
     */
    private Instruction getInst(Thread thread) {
        //1. parse first byte
        boolean isWide = false;
        int index = thread.pc;
        InstructionType type = InstructionType.getType(methodArea.getBytecode(thread.pc));
        thread.pc++;

        //2. parse second byte if WIDE-instruction
        if (type == InstructionType.WIDE) {
            isWide = true;
            type = InstructionType.getType(methodArea.getBytecode(thread.pc));
            thread.pc++;
        }

        //3. read byte operands
        int opByteCount = type.byteCount * (isWide ? 2 : 1);
        byte[] opBytes = new byte[opByteCount];
        for (int i = 0; i < opByteCount; i++) {
            opBytes[i] = methodArea.getBytecode(thread.pc);
            thread.pc++;
        }

        //4. create new instruction and convert byte operands into readable operands
        Instruction inst = new Instruction(index, type, opBytes, isWide);
        inst.parseOperands();
        return inst;
    }

    /**
     * Execute an in advance parsed instruction, which might change all involved data areas. This might affect the
     * values on the {@link Heap}, in the {@link MethodArea} and the current frame of the {@link Thread}.<br>
     * If a jump is done, the {@link Thread#pc} is changed to the target address of the jump.<br>
     * Execution of an instruction can cause resolution of a {@link RuntimeCPEntry}, that can represent
     * a class, a field or a method. This can lead to loading or linking of other classes. It can also
     * cause another method to be invoked which ultimately can lead to pushing a new {@link Frame} on the
     * {@link FrameStack} or a method to end, which pops a {@link Frame} from the {@link FrameStack}.
     *
     * @param thread the thread the instruction is parsed from
     * @param inst   the instruction that has to be applied/executed
     */
    private void executeInst(Thread thread, Instruction inst) {
        Frame frame = thread.currentFrame();
        OperandStack stack = frame.stack;
        int[] locals = frame.locals;
        String methIdent = thread.currentFrame().methodIdentifier;

        int val1;
        int val2;
        int val3;
        int val4;
        int objectRef;

        int localVar;
        int offset;

        int argsSize;
        int[] args;
        int cdOffset;
        int methTableOffset;

        RuntimeCPEntry cpEntry;

        boolean isFinal;
        boolean isInit;
        boolean isSameClass;

        try {
            switch (inst.type) {
                // jump-instructions
                case IFEQ:
                case IFNULL:
                    if (stack.pop() == 0)
                        thread.pc = inst.index + inst.op1;
                    break;
                case IFNE:
                case IFNONNULL:
                    if (stack.pop() != 0)
                        thread.pc = inst.index + inst.op1;
                    break;
                case IFGT:
                    if (stack.pop() > 0)
                        thread.pc = inst.index + inst.op1;
                    break;
                case IFGE:
                    if (stack.pop() >= 0)
                        thread.pc = inst.index + inst.op1;
                    break;
                case IFLT:
                    if (stack.pop() < 0)
                        thread.pc = inst.index + inst.op1;
                    break;
                case IFLE:
                    if (stack.pop() <= 0)
                        thread.pc = inst.index + inst.op1;
                    break;
                case IF_ICMPEQ:
                case IF_ACMPEQ:
                    if (stack.pop() == stack.pop())
                        thread.pc = inst.index + inst.op1;
                    break;
                case IF_ICMPNE:
                case IF_ACMPNE:
                    if (stack.pop() != stack.pop())
                        thread.pc = inst.index + inst.op1;
                    break;
                case IF_ICMPGT:
                    if (stack.pop() < stack.pop())
                        thread.pc = inst.index + inst.op1;
                    break;
                case IF_ICMPGE:
                    if (stack.pop() <= stack.pop())
                        thread.pc = inst.index + inst.op1;
                    break;
                case IF_ICMPLT:
                    if (stack.pop() > stack.pop())
                        thread.pc = inst.index + inst.op1;
                    break;
                case IF_ICMPLE:
                    if (stack.pop() >= stack.pop())
                        thread.pc = inst.index + inst.op1;
                    break;

                case GOTO:
                case GOTO_W:
                    thread.pc = inst.index + inst.op1;
                    break;

                //local variable instruction
                case ALOAD_0:
                case ALOAD_1:
                case ALOAD_2:
                case ALOAD_3:
                case ALOAD:
                    localVar = inst.type == InstructionType.ALOAD ? inst.op1 : inst.type.opcode - InstructionType.ALOAD_0.opcode;
                    stack.push(locals[localVar]);
                    break;

                case ILOAD_0:
                case ILOAD_1:
                case ILOAD_2:
                case ILOAD_3:
                case ILOAD:
                    localVar = inst.type == InstructionType.ILOAD ? inst.op1 : inst.type.opcode - InstructionType.ILOAD_0.opcode;
                    stack.push(locals[localVar]);
                    break;

                case ASTORE_0:
                case ASTORE_1:
                case ASTORE_2:
                case ASTORE_3:
                case ASTORE:
                    localVar = inst.type == InstructionType.ASTORE ? inst.op1 : inst.type.opcode - InstructionType.ASTORE_0.opcode;
                    locals[localVar] = stack.pop();
                    break;

                case ISTORE_0:
                case ISTORE_1:
                case ISTORE_2:
                case ISTORE_3:
                case ISTORE:
                    localVar = inst.type == InstructionType.ISTORE ? inst.op1 : inst.type.opcode - InstructionType.ISTORE_0.opcode;
                    locals[localVar] = stack.pop();
                    break;

                // constant-pool instructions
                case LDC:
                case LDC_W:
                    cpEntry = methodArea.getCPEntryAt(frame.rcpRef, inst.op1);
                    stack.push(cpEntry.constValue);
                    break;

                case NEW:
                    cpEntry = methodArea.getCPEntryAt(frame.rcpRef, inst.op1);
                    if (!cpEntry.resolved)
                        resolve(cpEntry);
                    initializeIfNecessary(cpEntry);
                    cdOffset = cpEntry.offset;
                    methTableOffset = methodArea.getMethodTableOffset(cdOffset);
                    int fieldCount = methodArea.getFieldCount(cdOffset);
                    objectRef = heap.newObject(fieldCount, cdOffset, methTableOffset);
                    stack.push(objectRef);
                    break;

                case PUTFIELD:
                    val1 = stack.pop();
                    objectRef = stack.pop();
                    cpEntry = methodArea.getCPEntryAt(frame.rcpRef, inst.op1);
                    if (!cpEntry.resolved)
                        resolve(cpEntry);
                    if (cpEntry.offset > 0)
                        throw new IncompatibleClassChangeError("Instruction " + inst.type.name() + "at index " + (inst.index - frame.startPc)
                                + " of method " + methIdent + " tries to access the static field " + cpEntry.identifier);
                    isFinal = methodArea.isFinalField(cpEntry.identifier);
                    isInit = methIdent.contains("<init>");
                    isSameClass = methIdent.substring(0, methIdent.indexOf(".")).contains(cpEntry.getClassSubstring());
                    if (isFinal && (!isInit || !isSameClass))
                        throw new IllegalAccessError("Instruction " + inst.type.name() + "at index " + (inst.index - frame.startPc)
                                + " of method " + methIdent + " tries to write in final field " + cpEntry.identifier);
                    heap.putField(objectRef, cpEntry.offset, val1);
                    break;
                case PUTSTATIC:
                    val1 = stack.pop();
                    cpEntry = methodArea.getCPEntryAt(frame.rcpRef, inst.op1);
                    if (!cpEntry.resolved)
                        resolve(cpEntry);
                    initializeIfNecessary(cpEntry);
                    if (cpEntry.offset < 0)
                        throw new IncompatibleClassChangeError("Instruction " + inst.type.name() + "at index " + (inst.index - frame.startPc)
                                + " of method " + methIdent + " tries to access the non-static field " + cpEntry.identifier);
                    isFinal = methodArea.isFinalField(cpEntry.identifier);
                    isInit = methIdent.contains("<clinit>");
                    isSameClass = methIdent.substring(0, methIdent.indexOf(".")).contains(cpEntry.getClassSubstring());
                    if (isFinal && (!isInit || !isSameClass))
                        throw new IllegalAccessError("Instruction " + inst.type.name() + "at index " + (inst.index - frame.startPc)
                                + " of method " + methIdent + " tries to write in final field " + cpEntry.identifier);
                    methodArea.setClassDescTableEntry(cpEntry.offset, val1);
                    break;

                case GETFIELD:
                    objectRef = stack.pop();
                    cpEntry = methodArea.getCPEntryAt(frame.rcpRef, inst.op1);
                    if (!cpEntry.resolved)
                        resolve(cpEntry);
                    if (cpEntry.offset > 0)
                        throw new IncompatibleClassChangeError("Instruction " + inst.type.name() + "at index " + (inst.index - frame.startPc)
                                + " of method " + methIdent + " tries to access the static field " + cpEntry.identifier);
                    val1 = heap.getField(objectRef, cpEntry.offset);
                    stack.push(val1);
                    break;
                case GETSTATIC:
                    cpEntry = methodArea.getCPEntryAt(frame.rcpRef, inst.op1);
                    if (!cpEntry.resolved)
                        resolve(cpEntry);
                    initializeIfNecessary(cpEntry);
                    if (cpEntry.offset < 0)
                        throw new IncompatibleClassChangeError("Instruction " + inst.type.name() + "at index " + (inst.index - frame.startPc)
                                + " of method " + methIdent + " tries to access the non-static field " + cpEntry.identifier);
                    val1 = methodArea.getClassDescTableEntry(cpEntry.offset);
                    stack.push(val1);
                    break;

                case INVOKESTATIC:
                    cpEntry = methodArea.getCPEntryAt(frame.rcpRef, inst.op1);

                    //1. Check, if print-instruction
                    if (cpEntry.identifier.equals("System.print:(I)V")) {
                        System.out.println(stack.pop());
                        break;
                    }

                    //2. Resolve and initialize if necessary
                    if (!cpEntry.resolved)
                        resolve(cpEntry);
                    initializeIfNecessary(cpEntry);

                    //3. Get offset for Code
                    if (cpEntry.offset < 0)
                        throw new IncompatibleClassChangeError("Instruction " + inst.type.name() + "at index " + (inst.index - frame.startPc)
                                + " of method " + methIdent + " tries to call the non-static method " + cpEntry.identifier);
                    offset = methodArea.getClassDescTableEntry(cpEntry.offset);

                    //4. get class descriptor offset and runtime constant pool index for class
                    cdOffset = methodArea.getClassOffsetFor(cpEntry.getClassSubstring());

                    //5. Get arguments
                    argsSize = methodArea.getArgsSize(offset);
                    args = new int[argsSize];
                    for (int i = argsSize - 1; i >= 0; i--)
                        args[i] = stack.pop();


                    jvm.invokeMethod(offset, thread, args, cdOffset, cpEntry.identifier);
                    break;

                case INVOKESPECIAL:
                case INVOKEVIRTUAL:
                    cpEntry = methodArea.getCPEntryAt(frame.rcpRef, inst.op1);

                    //1. Get arguments
                    argsSize = getParCount(cpEntry) + 1; //+1 = objectRef
                    args = new int[argsSize];
                    for (int i = argsSize - 1; i >= 0; i--)
                        args[i] = stack.pop();

                    //2. Check, if INVOKESPECIAL of Object --> no further method-call!
                    if (inst.type == InstructionType.INVOKESPECIAL && cpEntry.identifier.equals("java/lang/Object.<init>:()V")) {
                        heap.setInitialized(args[0]);
                        break;
                    }

                    //3. Resolve
                    if (!cpEntry.resolved)
                        resolve(cpEntry);

                    //4. Get offset for Code
                    if (cpEntry.offset >= 0)
                        throw new IncompatibleClassChangeError("Instruction " + inst.type.name() + "at index " + (inst.index - frame.startPc)
                                + " of method " + methIdent + " tries to call the static method " + cpEntry.identifier);
                    methTableOffset = heap.getMethTableOffset(args[0]);
                    offset = methodArea.getMethodTableEntry(cpEntry.offset + methTableOffset);

                    //5. get class descriptor offset
                    cdOffset = heap.getClassDescriptorOffset(args[0]);

                    jvm.invokeMethod(offset, thread, args, cdOffset, cpEntry.identifier);
                    break;

                //various arguments instructions
                case NEWARRAY:
                    int count = stack.pop();
                    if (count < 0) {
                        throw new NegativeArraySizeException("Instruction " + inst.type.name() + " at index " + (inst.index - frame.startPc) +
                                " of method " + methIdent + " tries to declare negative array size");
                    }
                    if (inst.op1 == 10) { //always true
                        objectRef = heap.newArray(count);
                        stack.push(objectRef);
                    }
                    break;
                case BIPUSH:
                case SIPUSH:
                    stack.push(inst.op1);
                    break;
                case IINC:
                    locals[inst.op1] = locals[inst.op1] + inst.op2;
                    break;

                // no argument instructions
                case NOP:
                    break;
                case POP:
                    stack.pop();
                    break;
                case POP2:
                    stack.pop();
                    stack.pop();
                    break;
                case SWAP:
                    val1 = stack.pop();
                    val2 = stack.pop();
                    stack.push(val1);
                    stack.push(val2);
                    break;
                case DUP:
                    val1 = stack.pop();
                    stack.push(val1);
                    stack.push(val1);
                    break;
                case DUP_X1:
                    val1 = stack.pop();
                    val2 = stack.pop();
                    stack.push(val1);
                    stack.push(val2);
                    stack.push(val1);
                    break;
                case DUP_X2:
                    val1 = stack.pop();
                    val2 = stack.pop();
                    val3 = stack.pop();
                    stack.push(val1);
                    stack.push(val3);
                    stack.push(val2);
                    stack.push(val1);
                    break;
                case DUP2:
                    val1 = stack.pop();
                    val2 = stack.pop();
                    stack.push(val2);
                    stack.push(val1);
                    stack.push(val2);
                    stack.push(val1);
                    break;
                case DUP2_X1:
                    val1 = stack.pop();
                    val2 = stack.pop();
                    val3 = stack.pop();
                    stack.push(val2);
                    stack.push(val1);
                    stack.push(val3);
                    stack.push(val2);
                    stack.push(val1);
                    break;
                case DUP2_X2:
                    val1 = stack.pop();
                    val2 = stack.pop();
                    val3 = stack.pop();
                    val4 = stack.pop();
                    stack.push(val2);
                    stack.push(val1);
                    stack.push(val4);
                    stack.push(val3);
                    stack.push(val2);
                    stack.push(val1);
                    break;
                case IADD:
                    stack.push(stack.pop() + stack.pop());
                    break;
                case ISUB:
                    val2 = stack.pop();
                    val1 = stack.pop();
                    stack.push(val1 - val2);
                    break;
                case IMUL:
                    stack.push(stack.pop() * stack.pop());
                    break;
                case IDIV:
                    val2 = stack.pop();
                    val1 = stack.pop();
                    stack.push(val1 / val2);
                    break;
                case IAND:
                    stack.push(stack.pop() & stack.pop());
                    break;
                case IOR:
                    stack.push(stack.pop() | stack.pop());
                    break;
                case IXOR:
                    stack.push(stack.pop() ^ stack.pop());
                    break;
                case IREM:
                    val2 = stack.pop();
                    val1 = stack.pop();
                    stack.push(val1 % val2);
                    break;
                case ISHL:
                    val2 = stack.pop();
                    val1 = stack.pop();
                    stack.push(val1 << (val2 & 0b11111));
                    break;
                case ISHR:
                    val2 = stack.pop();
                    val1 = stack.pop();
                    stack.push(val1 >> (val2 & 0b11111));
                    break;
                case IUSHR:
                    val2 = stack.pop();
                    val1 = stack.pop();
                    stack.push(val1 >>> (val2 & 0b11111));
                    break;
                case INEG:
                    stack.push(-stack.pop());
                    break;
                case ICONST_M1:
                case ICONST_0:
                case ICONST_1:
                case ICONST_2:
                case ICONST_3:
                case ICONST_4:
                case ICONST_5:
                    stack.push(inst.type.opcode - InstructionType.ICONST_0.opcode);
                    break;

                case IALOAD:
                    offset = stack.pop();
                    objectRef = stack.pop();
                    val1 = heap.getArrayElement(objectRef, offset);
                    stack.push(val1);
                    break;
                case IASTORE:
                    val1 = stack.pop();
                    offset = stack.pop();
                    objectRef = stack.pop();
                    heap.setArrayElement(objectRef, offset, val1);
                    break;
                case ARRAYLENGTH:
                    objectRef = stack.pop();
                    val1 = heap.getSize(objectRef);
                    stack.push(val1);
                    break;
                case RETURN:
                    thread.frameStack.pop();
                    if (thread.framesLeft())
                        thread.pc = thread.currentFrame().savePc;
                    break;
                case IRETURN:
                    val1 = stack.pop();
                    thread.frameStack.pop();
                    if (thread.framesLeft()) {
                        thread.currentFrame().stack.push(val1);
                        thread.pc = thread.currentFrame().savePc;
                    }
                    break;
                case WIDE:
                    throw new InternalError("Instruction " + inst.type.name() + " at index " + (inst.index - frame.startPc) +
                            " of method " + methIdent + " can not be a standalone method. Method not verified properly.");
                default:
                    throw new InternalError("Instruction " + inst.type.name() + " at index " + (inst.index - frame.startPc) +
                            " of method " + methIdent + " is unknown. Method not verified properly.");
            }

        } catch (Exception | Error e) {
            e.printStackTrace();
            throw new InternalError("Instruction " + inst.type.name() + " at index " + (inst.index - frame.startPc) +
                    " of method " + methIdent + " threw exception.");
        }

    }

    /**
     * Resolve a constant pool entry. The actual way of resolution is determined by
     * the {@link RuntimeCPEntry#tag}, which is different for classes, fields and
     * methods. After resolution {@link RuntimeCPEntry#resolved} is set to {@code true}.
     * As the identifier in {@link RuntimeCPEntry} is already resolved, it is not necessary
     * to look at other entries in the {@link tjvm.data.RuntimeCP}.
     * <blockquote>
     * "The Java Virtual Machine instructions [...] <code>getfield, getstatic,
     * invokespecial, invokestatic, invokevirtual, ldc, ldc_w, new, putfield,</code>
     * and {@code putstatic} make symbolic references to the run-time constant pool.
     * Execution of any of these instructions requires resolution of its symbolic reference.<br>
     * Resolution is the process of dynamically determining concrete values from
     * symbolic references in the run-time constant pool.<br>
     * Resolution can be attempted on a symbolic reference that has already been
     * resolved. An attempt to resolve a symbolic reference that has already successfully
     * been resolved always succeeds trivially and always results in the same entity
     * produced by the initial resolution of that reference.<br>
     * If an error occurs during resolution of a symbolic reference, then an instance of
     * IncompatibleClassChangeError (or a subclass) must be thrown at a point in the
     * program that (directly or indirectly) uses the symbolic reference." (&sect;5.4.3)<br>
     * </blockquote>
     *
     * @param entry the constant pool entry to resolve
     */
    private void resolve(RuntimeCPEntry entry) {
        switch (entry.tag) {
            case CPEntry.CONSTANT_Class:
                resolveClass(entry);
                break;
            case CPEntry.CONSTANT_Fieldref:
                resolveField(entry);
                break;
            case CPEntry.CONSTANT_Methodref:
                resolveMethod(entry);
                break;
            default:
                throw new IncompatibleClassChangeError("Constant pool entry tag unknown.");
        }
        entry.resolved = true;
    }

    /**
     * Resolve a runtime constant pool class entry. The class is loaded, if necessary.
     * <blockquote>
     * "To resolve an unresolved symbolic reference from D to a class or interface C denoted
     * by N, the following steps are performed:<br>
     * 1. The defining class loader of D is used to create a class or interface denoted by
     * N. This class or interface is C. The details of the process are given in &sect;5.3.<br>
     * Any exception that can be thrown as a result of failure of class or interface
     * creation can thus be thrown as a result of failure of class and interface
     * resolution." (&sect;5.4.3.1)
     * </blockquote>
     *
     * @param entry the constant pool class entry to resolve
     * @throws NoClassDefFoundError if class lookup fails
     */
    private void resolveClass(RuntimeCPEntry entry) {
        try {
            loadIfNecessary(entry);
        } catch (ClassNotFoundException ex) {
            NoClassDefFoundError err = new NoClassDefFoundError("Error resolving class " + entry.identifier + " .");
            err.initCause(ex);
            throw err;
        }
        entry.offset = methodArea.getClassOffsetFor(entry.identifier);
    }

    /**
     * Resolve a runtime constant pool fieldref entry. The class of the field is
     * linked, if necessary, because in {@link MethodArea#prepare(DerivedClass)}
     * the offsets of the static fields are determined.
     * <blockquote>
     * "To resolve an unresolved symbolic reference from D to a field in a class or interface
     * C, the symbolic reference to C given by the field reference must first be resolved
     * (&sect;5.4.3.1). Therefore, any exception that can be thrown as a result of failure of
     * resolution of a class or interface reference can be thrown as a result of failure of
     * field resolution. If the reference to C can be successfully resolved, an exception
     * relating to the failure of resolution of the field reference itself can be thrown.<br>
     * When resolving a field reference, field resolution first attempts to look up the
     * referenced field in C and its superclasses: <br>
     * 1. If C declares a field with the name and descriptor specified by the field
     * reference, field lookup succeeds. The declared field is the result of the field
     * lookup. [...]<br>
     * 4. Otherwise, field lookup fails.<br>
     * Then:<br>
     * If field lookup fails, field resolution throws a NoSuchFieldError." (&sect;5.4.3.2)
     * </blockquote>
     *
     * @param entry the constant pool fieldref entry to resolve
     * @throws NoClassDefFoundError if class lookup fails
     * @throws NoSuchFieldError     if field lookup fails
     */
    private void resolveField(RuntimeCPEntry entry) {
        try {
            linkIfNecessary(entry);
        } catch (ClassNotFoundException ex) {
            NoClassDefFoundError error = new NoClassDefFoundError("Error resolving field " + entry.identifier + " .");
            error.initCause(ex);
            throw error;
        }
        try {
            entry.offset = methodArea.getFieldOffsetFor(entry.identifier);
        } catch (NullPointerException ex) {
            NoSuchFieldError err = new NoSuchFieldError("Field " + entry.identifier + " can not be resolved.");
            err.initCause(ex);
            throw err;

        }
    }

    /**
     * Resolve a runtime constant pool methodref entry. The class of the method is
     * linked, if necessary, because in {@link MethodArea#prepare(DerivedClass)}
     * the offsets of the static methods are determined.
     * <blockquote>
     * "To resolve an unresolved symbolic reference from D to a method in a class C, the
     * symbolic reference to C given by the method reference is first resolved (&sect;5.4.3.1).
     * Therefore, any exception that can be thrown as a result of failure of resolution of
     * a class reference can be thrown as a result of failure of method resolution. If the
     * reference to C can be successfully resolved, exceptions relating to the resolution of
     * the method reference itself can be thrown.<br>
     * When resolving a method reference:<br>
     * [...] method resolution attempts to locate the referenced method in C
     * and its superclasses:<br>
     * [...] if C declares a method with the name and descriptor specified by
     * the method reference, method lookup succeeds.[...]<br>
     * The result of method resolution is determined by whether method lookup succeeds
     * or fails:<br>
     * If method lookup fails, method resolution throws a NoSuchMethodError."(&sect;5.4.3.3)
     * </blockquote>
     *
     * @param entry the constant pool methodref entry to resolve
     * @throws NoClassDefFoundError if class lookup fails
     * @throws NoSuchMethodError    if method lookup fails
     */
    private void resolveMethod(RuntimeCPEntry entry) {
        try {
            linkIfNecessary(entry);
        } catch (ClassNotFoundException ex) {
            NoClassDefFoundError err = new NoClassDefFoundError("Error resolving method " + entry.identifier + " .");
            err.initCause(ex);
            throw err;
        }
        try {
            entry.offset = methodArea.getMethodOffsetFor(entry.identifier);
        } catch (NullPointerException ex) {
            NoSuchMethodError err = new NoSuchMethodError("Method " + entry.identifier + " can not be resolved.");
            err.initCause(ex);
            throw err;
        }
    }

    /**
     * Loads a class if necessary. The identifier of the class is gotten from
     * the constant pool entry. If the identifier of the entry is for example
     * {@code ClassA.fieldB:I} only the part {@code ClassA} is used for
     * the procedure. If the the class is already loaded, nothing happens.
     *
     * @param entry the cp entry with the class to load
     * @throws ClassNotFoundException if no purported representation for the className is found
     */
    private void loadIfNecessary(RuntimeCPEntry entry) throws ClassNotFoundException {
        String classIdentifier = entry.getClassSubstring();
        if (!methodArea.classLoaded(classIdentifier)) {
            jvm.load(classIdentifier);
        }
    }

    /**
     * Link a class if necessary. The identifier of the class is gotten from
     * the constant pool entry. If the identifier of the entry is for example
     * {@code ClassA.fieldB:I} only the part {@code ClassA} is used for
     * the procedure. If the the class is already linked, nothing happens.
     *
     * @param entry the cp entry with the class to link
     * @throws ClassNotFoundException if no purported representation for the className is found
     */
    private void linkIfNecessary(RuntimeCPEntry entry) throws ClassNotFoundException {
        String classIdentifier = entry.getClassSubstring();
        if (!methodArea.classLinked(classIdentifier)) {
            jvm.link(classIdentifier);
        }
    }

    /**
     * Initialize a class if necessary. The identifier of the class is gotten from
     * the constant pool entry. If the identifier of the entry is for example
     * {@code ClassA.fieldB:I} only the part {@code ClassA} is used for
     * the procedure. If the the class is already initialized, nothing happens.
     *
     * @param entry the cp entry with the class to initialize
     * @throws ClassNotFoundException if no purported representation for the className is found
     */
    private void initializeIfNecessary(RuntimeCPEntry entry) throws ClassNotFoundException {
        String classIdentifier = entry.getClassSubstring();
        if (!methodArea.isInitializing(classIdentifier)) {
            jvm.initialize(classIdentifier);
        }
    }

    /**
     * Get the number of method parameters for a constant pool entry that refers
     * to a method. This method parses the identifier of the method to find out,
     * how many parameters are necessary. This method is needed, because the {@code objectref}
     * is on the stack under the arguments, which makes it necessary for the instructions
     * {@link InstructionType#INVOKEVIRTUAL} and theoretically {@link InstructionType#INVOKESPECIAL}
     * to know where {@code objectref} is on the stack to find the bytecode for the method.
     *
     * @param entry the runtime constant pool entry for a method
     * @return the number of parameters of the method
     */
    private int getParCount(RuntimeCPEntry entry) {
        String descriptor = entry.getDescriptorSubstring();
        String parameters = descriptor.substring(descriptor.indexOf("(") + 1, descriptor.indexOf(")"));
        String paraPattern = TextChecker.INT_DESC + "|" + TextChecker.ARRAY_DESC + "|" + TextChecker.CLASS_DESC;
        Matcher matcher = Pattern.compile(paraPattern).matcher(parameters);
        int parCount = 0;
        while (matcher.find()) {
            parCount++;
        }
        return parCount;
    }
}
