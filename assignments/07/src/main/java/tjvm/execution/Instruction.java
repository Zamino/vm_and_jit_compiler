package tjvm.execution;

import tjvm.data.ByteConverter;
import tjvm.verifier.InstructionType;

/**
 * Class that represents an instruction for the Java Virtual Machine. All instructions are
 * described in "The Java Virtual Machine Specification (Java SE 10 Edition)" in chapter
 * &sect;6.5. For easier handling, this class holds the (parsed and unparsed) operands
 * as well as the index in the {@code Bytecode} array and an information, if it is
 * modified by a WIDE instruction.
 */
public class Instruction {

    /**
     * The index of the beginning of the instruction in the bytecode array
     */
    public int index;

    /**
     * The type of the instruction.
     */
    public InstructionType type;

    /**
     * The first operand of the instruction.
     */
    public int op1;

    /**
     * The second operand of the instruction.
     */
    public int op2;

    /**
     * The bytecode array of the operands.
     */
    public byte[] byteOperands;

    /**
     * {@code true}, if the instruction was modified by a {@link InstructionType#WIDE}.
     */
    public boolean isWide;

    /**
     * Create a new instruction and having the {@link #byteOperands} already parsed into {@link #op1}
     * or {@link #op2}.
     *
     * @param index        the index of the beginning of the instruction in the bytecode array
     * @param type         the type of the instruction
     * @param byteOperands the bytecode array of the operands
     * @param isWide       {@code true}, if the instruction was modified by a {@link InstructionType#WIDE}
     */
    public Instruction(int index, InstructionType type, byte[] byteOperands, boolean isWide) {
        this.index = index;
        this.type = type;
        this.byteOperands = byteOperands;
        this.isWide = isWide;
    }

    /**
     * Parse the array of {@link #byteOperands} depending on the {@link #type} into {@link #op1} and {@link #op2}.
     * How the parsing works, is written down in chapter &sect;6.5. of the Java Virtual Machine Specification.
     */
    public void parseOperands() {
        switch (type) {
            //jump instructions
            case IFEQ:
            case IFNE:
            case IFGT:
            case IFGE:
            case IFLT:
            case IFLE:
            case IF_ICMPEQ:
            case IF_ICMPNE:
            case IF_ICMPGT:
            case IF_ICMPGE:
            case IF_ICMPLT:
            case IF_ICMPLE:
            case IF_ACMPEQ:
            case IF_ACMPNE:
            case IFNONNULL:
            case IFNULL:
            case GOTO:
            case GOTO_W:
                op1 = ByteConverter.toSignedInt(byteOperands);
                break;

            //local variable instruction
            case ILOAD:
            case ISTORE:
            case ALOAD:
            case ASTORE:
                op1 = ByteConverter.toUnsignedInt(byteOperands);
                break;

            // constant-pool instructions
            case LDC:
            case LDC_W:
            case NEW:
            case PUTFIELD:
            case GETFIELD:
            case GETSTATIC:
            case PUTSTATIC:
            case INVOKESTATIC:
            case INVOKESPECIAL:
            case INVOKEVIRTUAL:
                op1 = ByteConverter.toUnsignedInt(byteOperands);
                break;

            //various arguments instructions
            case NEWARRAY:
            case BIPUSH:
            case SIPUSH:
                op1 = ByteConverter.toSignedInt(byteOperands);
                break;
            case IINC:
                int opSize = isWide ? 2 : 1;
                byte[] byteOp1 = new byte[opSize];
                System.arraycopy(byteOperands, 0, byteOp1, 0, opSize);
                op1 = ByteConverter.toUnsignedInt(byteOp1);
                byte[] byteOp2 = new byte[opSize];
                System.arraycopy(byteOperands, opSize, byteOp2, 0, opSize);
                op2 = ByteConverter.toSignedInt(byteOp2);
                break;
        }
    }

}
