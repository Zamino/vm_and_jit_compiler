Das Projekt "TinyJVM" wurde mit dem Build-Tool Gradle erstellt.
Um die Erstellung des Projektes auszuführen, muss das Java Development Kit installatiert sein (JDK)
und (bei Windows) die Variable von Java zum Pfad hinzugefügt werden (https://www.java.com/de/download/help/path.xml)

Anschließend kann in diesem Verzeichnis "gradlew" bzw. "gradlew.bat" ausgeführt werden. Ist Gradle
noch nicht installiert, beginnt die Installation automatisch, anschließend kann durch erneutes
Ausführung die Dokumentation zur Benutzung aufgerufen werden.

Die wichtigsten Befehle für eine Benutzung von Gradle in Bezug auf das Projekt sind:
gradlew build       (hierdurch wird das gesamte Projekt kompiliert und der Ordner build erstellt)
gradlew javadoc     (hierdurch wird die Dokumentation des Projekts in dem ordner "build/docs/javadoc" erstellt)
gradlew tasks       (Übersicht der möglichen Aktionen)
gradlew -help       (Übersicht der möglichen

Wichtige Ordner:
src/main/java       (alle Java-Klassen des Projekts)
src/dist            (alle zusätzlichen Dateien, die mit dem Programm ausgeliefert werden - z.B. Testklassen für TinyJVM und Readme)

Nach Erstellung des Projektes befindet sich in dem Ordner "build/distributions" das Programm, welches
als zip- und als tar-Archiv verpackt ist. In diesen Archiven ist ebenfalls eine readme-Datei,
die die Benutzung des Programms beschreibt.

Die Tiny JVM kann nach Erstellung ebenfalls mit dem folgenden Befehl gestartet werden:
gradlew run                                     (für Ausführen der TinyJVM ohne Argumente)
gradlew run --args='-r folderX/ClassA.class'    (für Ausführen der TinyJVM mit Argumenten, hier als Beispiel)

Eine Anleitung für die Benutzung eines bereits existierenden Gradle Builds befindet sich unter:
https://guides.gradle.org/using-an-existing-gradle-build/